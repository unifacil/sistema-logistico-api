package com.unifacil.entidad;

import java.util.Date;


public class Unidad {
	
	
	private Long id;	
	
	private String nombrerazon;	
	
	private String abreviatura;	
	
	private boolean estado;	
	
	private Date fechaRegistro;	
	
	private int usuarioRegistro;	
	
	private Date fechaModificacion;	
	
	private int usuarioModificacion;
	
	private Ubicacion ubicacion;

	public Unidad(){
		
	}
	
	public Long getId() 
	{
		return id;
	}
	public void setId(Long id) 
	{
		this.id = id;
	}

	public String getNombreRazon() 
	{
		return nombrerazon;
	}
	public void setNombreRazon(String nombrerazon) 
	{
		this.nombrerazon = nombrerazon;
	}		
	
	public String getAbreviatura() 
	{
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) 
	{
		this.abreviatura = abreviatura;
	}
	
	public boolean getEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getNombrerazon() {
		return nombrerazon;
	}
	public void setNombrerazon(String nombrerazon) {
		this.nombrerazon = nombrerazon;
	}
	public int getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(int usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}
	public int getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(int usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	public Ubicacion getUbicacion() {
		if(ubicacion == null)
			ubicacion = new Ubicacion();
		return ubicacion;
	}
	public void setUbicacion(Ubicacion ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	
}
