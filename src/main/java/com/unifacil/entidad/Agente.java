package com.unifacil.entidad;

import java.util.Date;
import java.util.List;

public class Agente {

	private Long id;

	private String nombres;

	private String apellidos;

	private String dni;

	private String licencia;

	private String carnet;

	private String fechaInit;
	private Date fechaInitDate;

	private String fechaCese;
	private Date fechaCeseDate;

	private String celular;

	private String direccion;

	private boolean estado;

	private Usuario usuarioRegistro;

	private Date fechaRegistro;

	private Usuario usuarioModificacion;

	private Date fechaModificacion;

	public List<Ubicacion> listaUbicaciones;

	public List<Unidad> listaUnidades;

	public List<Sede> listaSedes;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getLicencia() {
		return licencia;
	}

	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getFechaInit() {
		return fechaInit;
	}

	public void setFechaInit(String fechaInit) {
		this.fechaInit = fechaInit;
	}

	public String getFechaCese() {
		return fechaCese;
	}

	public void setFechaCese(String fechaCese) {
		this.fechaCese = fechaCese;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Usuario getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(Usuario usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Usuario getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(Usuario usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Date getFechaInitDate() {
		return fechaInitDate;
	}

	public void setFechaInitDate(Date fechaInitDate) {
		this.fechaInitDate = fechaInitDate;
	}

	public Date getFechaCeseDate() {
		return fechaCeseDate;
	}

	public void setFechaCeseDate(Date fechaCeseDate) {
		this.fechaCeseDate = fechaCeseDate;
	}


}
