package com.unifacil.entidad;

import java.util.Date;

public class InventarioDetalle {

	private Long id;
	
	private Inventario inventario;
	
	private int cantidad;
	
	private String fechaCompra;
	
	private boolean estado;
	
	private int usuarioRegistrador;
	
	private String fechaRegistro;
	
	private int usuarioModificador;
	
	private String fechaModificacion;
	
	public InventarioDetalle(){
		inventario = new Inventario();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getUsuarioRegistrador() {
		return usuarioRegistrador;
	}

	public void setUsuarioRegistrador(int usuarioRegistrador) {
		this.usuarioRegistrador = usuarioRegistrador;
	}

	public int getUsuarioModificador() {
		return usuarioModificador;
	}

	public void setUsuarioModificador(int usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}

	public String getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
	
}
