package com.unifacil.entidad;

public class Subcategoria {

	private Long id;
	
	private String nombre;
	
	private Categoria categoria;
	
	public Subcategoria(){
		categoria = new Categoria();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	
}
