package com.unifacil.entidad;

public class Reporte {
	private Inventario inventario; //REPORTE 1
	
	private InventarioDetalle inventarioDetalle;//REPORTE 2
	
	private Asignacion asignacion;//REPORTE 3
	
	public Reporte(){
		inventario = new Inventario();
		inventarioDetalle = new InventarioDetalle();
		asignacion = new Asignacion();
	}

	public InventarioDetalle getInventarioDetalle() {
		return inventarioDetalle;
	}

	public void setInventarioDetalle(InventarioDetalle inventarioDetalle) {
		this.inventarioDetalle = inventarioDetalle;
	}

	public Asignacion getAsignacion() {
		return asignacion;
	}

	public void setAsignacion(Asignacion asignacion) {
		this.asignacion = asignacion;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Reporte [inventario=");
		builder.append(inventario);
		builder.append(", inventarioDetalle=");
		builder.append(inventarioDetalle);
		builder.append(", asignacion=");
		builder.append(asignacion);
		builder.append("]");
		return builder.toString();
	}
	
}
