package com.unifacil.entidad;

import java.util.List;

public class Asignacion {
	
private Long id;
	
	private Agente agente;
	
	private Unidad unidad;
	
	private Sede sede;
	
	private Inventario inventario;
	
	private int cantidad;
	
	private String fechaAsignacion;
	
	private boolean estado;
	
	private int idUsuarioRegistro;
	
	private int idUsuarioModificador;
	
	private String fechaRegistro;
	
	private String fechaModificacion;
	
	//Atributo para el servicio registrar
	
	private List<Agente> listaAgentes;
	
	private List<Unidad> listaUnidades;
	
	private List<Sede> listaSedes;
	
	public Asignacion(){
		agente = new Agente();
		unidad = new Unidad();
		sede = new Sede();
		inventario = new Inventario();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getIdUsuarioRegistro() {
		return idUsuarioRegistro;
	}

	public void setIdUsuarioRegistro(int idUsuarioRegistro) {
		this.idUsuarioRegistro = idUsuarioRegistro;
	}

	public int getIdUsuarioModificador() {
		return idUsuarioModificador;
	}

	public void setIdUsuarioModificador(int idUsuarioModificador) {
		this.idUsuarioModificador = idUsuarioModificador;
	}

	public Sede getSede() {
		return sede;
	}

	public void setSede(Sede sede) {
		this.sede = sede;
	}

	public Inventario getInventario() {
		return inventario;
	}

	public void setInventario(Inventario inventario) {
		this.inventario = inventario;
	}

	public void setAgente(Agente agente) {
		this.agente = agente;
	}

	public void setUnidad(Unidad unidad) {
		this.unidad = unidad;
	}

	public Agente getAgente() {
		return agente;
	}

	public Unidad getUnidad() {
		return unidad;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public List<Agente> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<Agente> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<Unidad> getListaUnidades() {
		return listaUnidades;
	}

	public void setListaUnidades(List<Unidad> listaUnidades) {
		this.listaUnidades = listaUnidades;
	}

	public List<Sede> getListaSedes() {
		return listaSedes;
	}

	public void setListaSedes(List<Sede> listaSedes) {
		this.listaSedes = listaSedes;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Asignacion [id=");
		builder.append(id);
		builder.append(", agente=");
		builder.append(agente);
		builder.append(", unidad=");
		builder.append(unidad);
		builder.append(", sede=");
		builder.append(sede);
		builder.append(", inventario=");
		builder.append(inventario);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", fechaAsignacion=");
		builder.append(fechaAsignacion);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", idUsuarioRegistro=");
		builder.append(idUsuarioRegistro);
		builder.append(", idUsuarioModificador=");
		builder.append(idUsuarioModificador);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append(", listaAgentes=");
		builder.append(listaAgentes);
		builder.append(", listaUnidades=");
		builder.append(listaUnidades);
		builder.append(", listaSedes=");
		builder.append(listaSedes);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
