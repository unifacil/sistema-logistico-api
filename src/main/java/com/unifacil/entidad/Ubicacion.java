package com.unifacil.entidad;

import java.sql.Date;


public class Ubicacion {
	

	private Long id;	
	
	private String nombres;	
	
	private String direccion;	
	
	private String responsable;	
	
	private boolean estado;	
	
	private Date fechaCreacion;	
	
	private Integer usuarioCreador;	
	
	private Date fechaModificacion;	
	
	private Integer usuarioModificador;
	
	private Integer usuario_perfil;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getResponsable() {
		return responsable;
	}
	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public boolean getEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public Integer getUsuarioCreador() {
		return usuarioCreador;
	}
	public void setUsuarioCreador(Integer usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public Integer getUsuarioModificador() {
		return usuarioModificador;
	}
	public void setUsuarioModificador(Integer usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}
	public Integer getUsuario_perfil() {
		return usuario_perfil;
	}
	public void setUsuario_perfil(Integer usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}

}
