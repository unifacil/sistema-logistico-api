package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.UsuarioRequest;

public interface UsuarioServicio {
	
	UsuarioBean findyUsernameAndPassword(String username, String password);
	
	List<UsuarioBean> buscarUsuario(UsuarioRequest usuarioRequest);
		
	boolean registrarUsuario(UsuarioBean usuarioBean);
	
	boolean actualizarUsuario(UsuarioBean usuarioBean);
	
	boolean eliminarUsuario(UsuarioRequest usuarioRequest);
	
	String recuperarPassword(UsuarioRequest usuarioRequest);
	
	boolean cambiarPassword(UsuarioRequest usuarioRequest);
}
