package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.CategoriaBean;

public interface CategoriaServicio {

	List<CategoriaBean> listarCategoria();

}
