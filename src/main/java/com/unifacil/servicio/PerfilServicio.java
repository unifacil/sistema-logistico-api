package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.PerfilBean;

public interface PerfilServicio {
	List<PerfilBean> listarPerfiles();
}
