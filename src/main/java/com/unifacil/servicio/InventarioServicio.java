package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.InventarioBean;
import com.unifacil.bean.InventarioDetalleBean;
import com.unifacil.bean.request.InventarioRequest;

public interface InventarioServicio {
	boolean registrarInventario(InventarioDetalleBean inventarioDetalleBean);
	List<InventarioBean> buscarInventario(InventarioRequest request);
}
