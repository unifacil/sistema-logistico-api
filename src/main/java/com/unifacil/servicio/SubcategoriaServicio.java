package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.SubcategoriaBean;
import com.unifacil.bean.SubcategoriaRequest;

public interface SubcategoriaServicio {

	List<SubcategoriaBean> listarSubcategoria(SubcategoriaRequest request);
	
}
