package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.ReporteBean;
import com.unifacil.bean.request.ReporteRequest;

public interface ReporteServicio {

	List<ReporteBean> obtenerReporte(ReporteRequest request);
	
}
