package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.AsignacionBean;
import com.unifacil.bean.request.AsignacionRequest;

public interface AsignacionServicio {

	boolean registrarAsignacion(AsignacionBean bean);
	List<AsignacionBean> buscarAsignacion(AsignacionRequest request);
	
}
