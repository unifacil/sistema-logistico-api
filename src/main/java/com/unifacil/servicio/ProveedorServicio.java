package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.ProveedorBean;
import com.unifacil.bean.request.ProveedorRequest;

public interface ProveedorServicio {

	List<ProveedorBean> buscarProveedor(ProveedorRequest request);

	boolean registrarProveedor(ProveedorRequest request);

	boolean modificarProveedor(ProveedorRequest request);

	boolean eliminarProveedor(ProveedorRequest request);
}
