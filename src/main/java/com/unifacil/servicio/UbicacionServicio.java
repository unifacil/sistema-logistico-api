package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.request.UbicacionRequest;
import com.unifacil.entidad.Ubicacion;


public interface UbicacionServicio {

	List<UbicacionBean> findAll(UbicacionRequest ubicacionRequest);
	List<UbicacionBean> buscarUbicacion(UbicacionRequest ubicacionRequest);
	int registrarUbicacion(UbicacionBean ubicacionBean);
	int actualizarUbicacion(UbicacionBean ubicacionBean);
	int eliminarUbicacion(UbicacionBean ubicacionBean);
}
