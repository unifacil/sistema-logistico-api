package com.unifacil.servicio;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.unifacil.bean.AgenteBean;
import com.unifacil.bean.request.AgenteRequest;

public interface AgenteServicio {

	AgenteBean findById(Long id);
	List<AgenteBean> findAll(AgenteRequest agenteRequest);
	List<AgenteBean> buscarAgente(AgenteRequest agenteRequest);
	boolean registrarAgente(AgenteRequest agenteRequest);	
	boolean modificarAgente(AgenteRequest agenteRequest);	
	boolean eliminarAgente(AgenteRequest agenteRequest);
	boolean cargarLicencia(List<MultipartFile> lista);
}
