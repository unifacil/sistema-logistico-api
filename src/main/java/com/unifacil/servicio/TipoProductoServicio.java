package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.TipoProductoBean;
import com.unifacil.bean.request.TipoProductoRequest;

public interface TipoProductoServicio {

	List<TipoProductoBean> listarTipoProducto(TipoProductoRequest request);
	
	
}
