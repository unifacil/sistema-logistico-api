package com.unifacil.servicio;

import java.util.List;
import com.unifacil.bean.SedeBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.request.SedeRequest;


public interface SedeServicio {
	List<SedeBean> findAll(List<UnidadBean> listaUnidadBean);
	List<SedeBean> buscarSede(SedeRequest sedeRequest);
	int registrarSede(SedeRequest sedeRequest);	
	int modificarSede(SedeRequest sedeRequest);	
	int eliminarSede(SedeRequest sedeRequest);
}
