package com.unifacil.servicio;

import java.util.List;

import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.request.UnidadRequest;


public interface UnidadServicio {

	List<UnidadBean> findAll(List<UbicacionBean> listaUbicaciones);
	List<UnidadBean> buscarUnidad(UnidadRequest unidadRequest);
	int registrarUnidad(UnidadBean unidadBean);
	int actualizarUnidad(UnidadBean unidadBean);
	int eliminarUnidad(UnidadBean unidadBean);
}
