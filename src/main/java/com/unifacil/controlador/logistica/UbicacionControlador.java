package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.UbicacionRequest;
import com.unifacil.bean.request.UsuarioRequest;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.servicio.UbicacionServicio;
import com.unifacil.servicioImpl.UbicacionServicioImpl;
import com.unifacil.util.constantes.Constantes;

@RestController
public class UbicacionControlador {

	@Autowired
	UbicacionServicioImpl ubicacionServicio;

	private static final Logger LOG = Logger.getLogger(UbicacionControlador.class.getName());

	@RequestMapping(value = "/listarubicaciones", method = RequestMethod.GET)
	private List<UbicacionBean> listarUbicaciones(HttpServletRequest request) {
		LOG.info("ejecuta listarubicaciones");
		List<UbicacionBean> listaUbicaciones = new ArrayList<UbicacionBean>();
		try {
			UbicacionRequest ubicacionRequest = new UbicacionRequest();
			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				ubicacionRequest
						.setUsuario_perfil(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
			} else {
				ubicacionRequest.setUsuario_perfil(Constantes.UNO_INT);
			}
			listaUbicaciones = ubicacionServicio.findAll(ubicacionRequest);
		} catch (Exception ex) {
			LOG.warning("error en listarUbicacion");
		}

		return listaUbicaciones;
	}

	@RequestMapping(value = "/buscarubicaciones", method = RequestMethod.GET)
	private List<UbicacionBean> buscarUbicaciones(
			@RequestParam(value = "idUbicacion", defaultValue = "0") int idUbicacion,
			@RequestParam(value = "direccion", defaultValue = "") String direccion,
			@RequestParam(value = "responsable", defaultValue = "") String responsable,
			@RequestParam(value = "usuario_perfil", defaultValue = "0") String usuario_perfil
			) {

		LOG.info("ejecuta buscarubicaciones");
		List<UbicacionBean> listarUbicacionBean = new ArrayList<UbicacionBean>();
		try {
			UbicacionRequest ubicacionRequest = new UbicacionRequest();
			ubicacionRequest.setIdUbicacion(idUbicacion);
			ubicacionRequest.setDireccion(direccion);
			ubicacionRequest.setResponsable(responsable);
			ubicacionRequest.setUsuario_perfil(Integer.parseInt(usuario_perfil));
			listarUbicacionBean = ubicacionServicio.buscarUbicacion(ubicacionRequest);
		} catch (Exception e) {
			// TODO: handle exception
			LOG.warning("error en buscarUbicacion");
		}

		return listarUbicacionBean;
	}

	@RequestMapping(value = "/addubicaciones", method = RequestMethod.POST)
	private RespuestaBean registrarUbicacion(final @RequestBody UbicacionBean ubicacionBean,
			HttpServletRequest request) {
		LOG.info("ejecuta addubicaciones");

		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				ubicacionBean.setUsuarioRegistro(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				ubicacionBean.setUsuarioRegistro(Constantes.UNO);
			}
			ubicacionBean.setId(Constantes.CERO);

			int registro = ubicacionServicio.registrarUbicacion(ubicacionBean);

			respuesta.setCodigoRetorno(registro > 0 ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(registro > 0 ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_UBICACION_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en registrarUbicacion");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}

	}

	@RequestMapping(value = "/updateubicaciones", method = RequestMethod.PUT)
	private RespuestaBean actualizarUbicacion(final @RequestBody UbicacionBean ubicacionBean,
			HttpServletRequest request) {
		LOG.info("ejecuta updateubicaciones");

		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				ubicacionBean.setUsuarioModificacion(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				ubicacionBean.setUsuarioModificacion(Constantes.UNO);
			}

			int actualizo = ubicacionServicio.actualizarUbicacion(ubicacionBean);

			respuesta.setCodigoRetorno(actualizo > 0 ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(
					actualizo > 0 ? Constantes.MENSAJE_EXITO_ACTUALIZACION : Constantes.MENSAJE_UBICACION_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en actualizarUbicacion");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}

	}

	@RequestMapping(value = "/deleteubicaciones", method = RequestMethod.PUT)
	private RespuestaBean eliminarUbicacion(final @RequestBody UbicacionBean ubicacionBean,
			HttpServletRequest request) {
		LOG.info("ejecuta deleteubicaciones");

		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				ubicacionBean.setUsuarioModificacion(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				ubicacionBean.setUsuarioModificacion(Constantes.UNO);
			}

			int elimino = ubicacionServicio.eliminarUbicacion(ubicacionBean);

			respuesta.setCodigoRetorno(elimino > 0 ? Constantes.UNO : Constantes.CERO);

			respuesta
					.setMensaje(elimino > 0 ? Constantes.MENSAJE_EXITO_ELIMINACION : Constantes.MENSAJE_ERROR_REGISTRO);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en eliminarUbicacion");
			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);
			respuesta.setCodigoRetorno(Constantes.CERO);
			return respuesta;

		}

	}

}
