package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.RespuestaDataBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.UsuarioRequest;
import com.unifacil.servicioImpl.UsuarioServicioImpl;
import com.unifacil.util.constantes.Constantes;
import com.unifacil.util.convertidores.UsuarioConvertidor;

@RestController
public class UsuarioControlador {

	@Autowired
	public UsuarioServicioImpl usuarioServicio;

	@Autowired
	public UsuarioConvertidor usuarioConvertidor;
	
	private static final Logger LOG = Logger.getLogger(UsuarioControlador.class.getName());

	@RequestMapping(value = "/validarusuario", method = RequestMethod.POST)
	private UsuarioBean validarUsuario(final @RequestBody UsuarioBean usuarioBean, HttpServletRequest request) {
		LOG.info("ejecuta validarusuario");
		LOG.info("************USUARIOOOOOO***** " + usuarioBean.getUsername());
		LOG.info("************PASSWORD***** " + usuarioBean.getPassword());

		UsuarioBean usuarioValido = usuarioServicio.findyUsernameAndPassword(usuarioBean.getUsername(),
				usuarioBean.getPassword());

		UsuarioBean userSesion = new UsuarioBean();
		if (usuarioValido == null) {
			userSesion = new UsuarioBean();
			userSesion.setId(Constantes.CERO);
		}
		if (!Constantes.CERO.equals(usuarioValido.getId())) {

			userSesion.setId(usuarioValido.getId());
			userSesion.setNombreCompleto(usuarioValido.getNombreCompleto());
			userSesion.setPerfilId(usuarioValido.getPerfilId());
			HttpSession sesion = request.getSession();
			sesion.setAttribute(Constantes.USUARIO, userSesion);
		}

		return userSesion;

	}

	@RequestMapping(value = "/buscarusuario", method = RequestMethod.GET)
	private List<UsuarioBean> buscarUsuario(@RequestParam(value = "perfilid", defaultValue = "0") int perfilid,
			@RequestParam(value = "nombre", defaultValue = "") String nombre,
			@RequestParam(value = "usuario", defaultValue = "") String usuario,
			@RequestParam(value = "ubicacionid", defaultValue = "0") int ubicacionid,
			@RequestParam(value = "unidadid", defaultValue = "0") int unidadid,
			@RequestParam(value = "sedeid", defaultValue = "0") int sedeid,
			@RequestParam(value = "correo", defaultValue = "") String correo,
			@RequestParam(value = "usuario_perfil", defaultValue = "0") String usuario_perfil
			) {
		LOG.info("ejecuta buscarusuario");
		UsuarioRequest usuarioRequest = new UsuarioRequest();
		usuarioRequest.setIdPerfil(perfilid);
		usuarioRequest.setNombre(nombre);
		usuarioRequest.setUsuario(usuario);
		usuarioRequest.setIdUbicacion(ubicacionid);
		usuarioRequest.setIdUnidad(unidadid);
		usuarioRequest.setIdSede(sedeid);
		usuarioRequest.setCorreo(correo);
		usuarioRequest.setUsuario_perfil(Integer.parseInt(usuario_perfil));

		List<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();
		try {
			listaUsuarios = usuarioServicio.buscarUsuario(usuarioRequest);
		} catch (Exception ex) {
			LOG.warning("error en buscarusuario");
		}

		return listaUsuarios;

	}

	@RequestMapping(value = "/listarusuarios", method = RequestMethod.GET)
	private List<UsuarioBean> buscarUsuario() {
		LOG.info("ejecuta listarusuarios");
		UsuarioRequest usuarioRequest = new UsuarioRequest();

		List<UsuarioBean> listaUsuarios = new ArrayList<UsuarioBean>();
		try {
			listaUsuarios = usuarioServicio.buscarUsuario(usuarioRequest);
		} catch (Exception ex) {
			LOG.warning("error en listarusuarios");
		}

		return listaUsuarios;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/addusuario")
	public RespuestaBean registrarUsuario(final @RequestBody UsuarioBean usuarioBean, HttpServletRequest request) {
		LOG.info("ejecuta addusuario");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				//System.out.println("sesion"+usuario.getId());
				usuarioBean.setUsuarioCreador(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				usuarioBean.setUsuarioCreador(Constantes.UNO);
			}

			boolean registro = usuarioServicio.registrarUsuario(usuarioBean);

			respuesta.setCodigoRetorno(registro ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(registro ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_USUARIO_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en addusuario");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/updateusuario")
	public RespuestaBean actualizarUsuario(final @RequestBody UsuarioBean usuarioBean, HttpServletRequest request) {
		LOG.info("ejecuta updateusuario");
		RespuestaBean respuesta = new RespuestaBean();

		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				//System.out.println("sesion"+usuario.getId());
				usuarioBean.setUsuarioModificar(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				usuarioBean.setUsuarioModificar(Constantes.UNO);
			}
			boolean actualizo = usuarioServicio.actualizarUsuario(usuarioBean);

			respuesta.setCodigoRetorno(actualizo ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(actualizo ? Constantes.MENSAJE_EXITO_ACTUALIZACION : Constantes.MENSAJE_USUARIO_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en updateusuario");
			
			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/deleteusuario")
	public RespuestaBean eliminarUsuario(final @RequestBody UsuarioBean usuario, HttpServletRequest request) {
		LOG.info("ejecuta deleteusuario");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			UsuarioRequest usuarioRequestEnviar = new UsuarioRequest();
			usuarioRequestEnviar.setIdUsuario(Integer.parseInt(usuario.getId()));
			//System.out.println("sesion"+usuario.getId());
			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean userSesion = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				usuarioRequestEnviar.setIdUsuarioModificador(
						userSesion != null ? Integer.parseInt(userSesion.getId()) : Constantes.UNO_INT);

			} else {
				usuarioRequestEnviar.setIdUsuarioModificador(Constantes.UNO_INT);
			}

			boolean elimino = usuarioServicio.eliminarUsuario(usuarioRequestEnviar);

			respuesta.setMensaje(elimino ? Constantes.MENSAJE_EXITO_ELIMINACION : Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(elimino ? Constantes.UNO : Constantes.CERO);

			return respuesta;
		} catch (Exception e) {
			LOG.warning("error en deleteusuario");
			
			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "/recuperarpassword")
	public RespuestaDataBean recuperarPassword(final @RequestBody UsuarioRequest usuarioRequest) {
		LOG.info("ejecuta recuperarpassword");
		RespuestaDataBean respuesta = new RespuestaDataBean();

		try {

			UsuarioRequest usuarioRequestBuscar = new UsuarioRequest();
			usuarioRequestBuscar.setCorreo(usuarioRequest.getCorreo());

			String nuevoPassword = usuarioServicio.recuperarPassword(usuarioRequestBuscar);
			LOG.info("recuperar pass:"+nuevoPassword);
			if (nuevoPassword == null) {
				respuesta.setCodigoRetorno("-1");
				respuesta.setMensaje("Ocurrio un error al tratar de generar el password");
			} else if (Constantes.CERO.equals(nuevoPassword)) {
				respuesta.setCodigoRetorno(Constantes.CERO);
				respuesta.setMensaje("No se encontro a un usuario con dicho correo");
			} else {
				respuesta.setCodigoRetorno(Constantes.UNO);
				respuesta.setMensaje("Nuevo Password Generado");
				respuesta.setData(nuevoPassword);
			}

		} catch (Exception e) {
			LOG.warning("error en recuperarpassword");
			respuesta.setCodigoRetorno(Constantes.CERO);
		}

		return respuesta;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cambiarpassword")
	public RespuestaBean cambiarPassword(final @RequestBody UsuarioBean usuarioBean) {
		LOG.info("ejecuta cambiarpassword");
		RespuestaBean respuesta = new RespuestaBean();

		try {

			UsuarioRequest usuarioRequest = new UsuarioRequest();
			usuarioRequest.setUsuario(usuarioBean.getUsername());
			usuarioRequest.setNuevoPassword(usuarioBean.getPassword());
			usuarioRequest.setIdUsuario(usuarioBean.getId() != null ? Integer.parseInt(usuarioBean.getId()) : null);
			boolean actualizo = usuarioServicio.cambiarPassword(usuarioRequest);

			respuesta.setCodigoRetorno(actualizo ? Constantes.UNO : Constantes.CERO);
			respuesta
					.setMensaje(actualizo ? Constantes.MENSAJE_EXITO_ACTUALIZACION : Constantes.MENSAJE_ERROR_REGISTRO);

		} catch (Exception e) {
			LOG.warning("error en cambiarpassword");
			respuesta.setCodigoRetorno(Constantes.CERO);
			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);
		}

		return respuesta;
	}

}
