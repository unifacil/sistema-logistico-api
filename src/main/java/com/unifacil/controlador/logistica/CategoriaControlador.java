package com.unifacil.controlador.logistica;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.CategoriaBean;
import com.unifacil.servicio.CategoriaServicio;

@RestController
public class CategoriaControlador {

	@Autowired
	CategoriaServicio categoriaServicio;
	private static final Logger LOG = Logger.getLogger(CategoriaControlador.class.getName());

	@RequestMapping(value = "/listarcategoria", method = RequestMethod.GET)
	private List<CategoriaBean> listarCategoria() {
		LOG.info("ejecuta listarcategoria");
		return categoriaServicio.listarCategoria();
	}

}
