package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.PerfilBean;
import com.unifacil.servicioImpl.PerfilServicioImpl;

@RestController
public class PerfilControlador {

	@Autowired
	public PerfilServicioImpl perfilServicioImpl;
	
	@RequestMapping(value = "/listarPerfiles", method = RequestMethod.GET)
	private List<PerfilBean> listarPerfiles(){
		
		List<PerfilBean> listaPerfiles = new ArrayList<PerfilBean>();
		try {
			listaPerfiles = perfilServicioImpl.listarPerfiles();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listaPerfiles;
		
	}
	
	
}
