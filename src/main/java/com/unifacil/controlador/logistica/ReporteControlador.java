package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.FileBean;
import com.unifacil.bean.ReporteBean;
import com.unifacil.bean.request.ReporteRequest;
import com.unifacil.servicioImpl.ReporteServicioImpl;
import com.unifacil.util.Excel;
import com.unifacil.util.PlantillaMetodos;
import com.unifacil.util.constantes.Constantes;

@RestController
public class ReporteControlador {

	@Autowired
	ReporteServicioImpl reporteServicio;

	@Autowired
	Excel excel;
	
	@Value("${urlFile}")
	String URL_FILE;

	private static final Logger LOG = Logger.getLogger(ReporteControlador.class.getName());

	@RequestMapping(value = "/obtenerreporte", method = RequestMethod.GET)
	private FileBean obtenerReporte(@RequestParam(value = "parametro", defaultValue = "0") int parametro,
			@RequestParam(value = "fechadesde", defaultValue = "") String fechaDesde,
			@RequestParam(value = "fechahasta", defaultValue = "") String fechaHasta) {

		LOG.info("ejecuta obtenerReporte");

		ReporteRequest request = new ReporteRequest();
		request.setParametro(parametro);
		request.setFechaDesde(fechaDesde);
		request.setFechaHasta(fechaHasta);

		List<ReporteBean> reporte = new ArrayList<ReporteBean>();
		String fileName = "";
		try {
			reporte = reporteServicio.obtenerReporte(request);
			if (reporte != null) {
				LOG.info("reporte en proceso" + reporte.toString());
				if (request.getParametro() == Constantes.REPORTE_ACTUAL) {
					fileName = excel.obtenerReporteActual(reporte);
				} else if (request.getParametro() == Constantes.REPORTE_INGRESO) {
					fileName = excel.obtenerReporteIngreso(reporte);
				} else if (request.getParametro() == Constantes.REPORTE_SALIDA) {
					fileName = excel.obtenerReporteSalida(reporte);
				}
			}

		} catch (Exception e) {
			LOG.warning("error en obtenerReporte:"+e.getMessage());
		}
		FileBean file = new FileBean();
		file.setContent(PlantillaMetodos.obtenerArrayArchivo(URL_FILE.concat(fileName)));
		file.setEstado(Constantes.UNO);
		return file;

	}

}
