package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.UnidadRequest;
import com.unifacil.servicio.UnidadServicio;
import com.unifacil.util.constantes.Constantes;

@RestController
public class UnidadControlador {

	@Autowired
	UnidadServicio unidadServicio;
	private static final Logger LOG = Logger.getLogger(UnidadControlador.class.getName());

	@RequestMapping(value = "/listarunidades", method = RequestMethod.POST)
	private List<UnidadBean> listarUnidades(@RequestBody List<UbicacionBean> listUbicacion) {
		LOG.info("ejecutar listarunidades");
		List<UnidadBean> listaUnidades = new ArrayList<UnidadBean>();
		try {
			listaUnidades = unidadServicio.findAll(listUbicacion);
		} catch (Exception ex) {
			LOG.warning("error en listarunidades");
		}

		return listaUnidades;
	}
	
	@RequestMapping(value = "/buscarunidades", method = RequestMethod.GET)
	private List<UnidadBean> listarUnidades(@RequestParam(value = "idUbicacion", defaultValue = "0")
	int idUbicacion,@RequestParam(value = "idUnidad", defaultValue = "0")
	int idUnidad,  @RequestParam(value = "nombre", defaultValue = "")
	String nombre, @RequestParam(value = "abreviatura", defaultValue = "")
	String abreviatura,
	@RequestParam(value = "usuario_perfil", defaultValue = "0") String usuario_perfil
	) {
		LOG.info("ejecutar buscarunidades");
		List<UnidadBean> listaUnidades = new ArrayList<UnidadBean>();
		try {
			UnidadRequest unidadRequest = new UnidadRequest();
			unidadRequest.setIdUbicacion(idUbicacion);
			unidadRequest.setIdUnidad(idUnidad);
			unidadRequest.setNombre(nombre);
			unidadRequest.setAbreviatura(abreviatura);
			unidadRequest.setUsuario_perfil(Integer.parseInt(usuario_perfil));
			
			listaUnidades = unidadServicio.buscarUnidad(unidadRequest);
		} catch (Exception e) {
			LOG.warning("error en buscarunidades");
		}
		
		return listaUnidades;
	}
	
	@RequestMapping(value = "/addunidad", method = RequestMethod.POST)
	private RespuestaBean registrarUnidad(final @RequestBody UnidadBean unidadBean , HttpServletRequest request){
		LOG.info("ejecutar addunidad");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				unidadBean.setUsuarioRegistro(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				unidadBean.setUsuarioRegistro(Constantes.UNO);
			}
			unidadBean.setId(Constantes.CERO);

			int registro = unidadServicio.registrarUnidad(unidadBean);

			respuesta.setCodigoRetorno(registro>0 ? Constantes.UNO: Constantes.CERO);

			respuesta.setMensaje(registro>0 ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_UNIDAD_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en addunidad");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}
	}
	
	@RequestMapping(value = "/updateunidad", method = RequestMethod.PUT)
	private RespuestaBean actualizarUnidad(final @RequestBody UnidadBean unidadBean
			, HttpServletRequest request){
		LOG.info("ejecutar updateunidad");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				unidadBean.setUsuarioModificacion(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				unidadBean.setUsuarioModificacion(Constantes.UNO);
			}

			int actualizo = unidadServicio.actualizarUnidad(unidadBean);

			respuesta.setCodigoRetorno(actualizo>0 ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(actualizo>0 ? Constantes.MENSAJE_EXITO_ACTUALIZACION : Constantes.MENSAJE_UNIDAD_EXISTE);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en updateunidad");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}
		
	}
	
	@RequestMapping(value = "/deleteunidad", method = RequestMethod.PUT)
	private RespuestaBean eliminarUnidad(final @RequestBody UnidadBean unidadBean
			, HttpServletRequest request){
		LOG.info("ejecutar deleteunidad");
		RespuestaBean respuesta = new RespuestaBean();
		try {

			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				//System.out.println("sesion"+usuario.getId());
				unidadBean.setUsuarioModificacion(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				unidadBean.setUsuarioModificacion(Constantes.UNO);
			}

			int elimino = unidadServicio.eliminarUnidad(unidadBean);

			respuesta.setCodigoRetorno(elimino>0 ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(elimino>0 ? Constantes.MENSAJE_EXITO_ELIMINACION : Constantes.MENSAJE_ERROR_REGISTRO);

			return respuesta;

		} catch (Exception e) {
			LOG.warning("error en deleteunidad");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;

		}
		
	}
	
}
