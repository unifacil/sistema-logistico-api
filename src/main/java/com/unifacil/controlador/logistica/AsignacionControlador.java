package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.AsignacionBean;
import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.AsignacionRequest;
import com.unifacil.servicioImpl.AsignacionServicioImpl;
import com.unifacil.util.constantes.Constantes;

@RestController
public class AsignacionControlador {

	@Autowired
	AsignacionServicioImpl asignacionServicio;
	
	private static final Logger LOG = Logger.getLogger(AsignacionControlador.class.getName());
	
	@RequestMapping(value = "/buscarasignacion", method = RequestMethod.GET)
	private List<AsignacionBean> buscarAsignacion(@RequestParam(value = "idproducto", defaultValue = "0") int idproducto,
			@RequestParam(value = "marca", defaultValue = "") String marca,
			@RequestParam(value = "modelo", defaultValue = "") String modelo,
			@RequestParam(value = "serie", defaultValue = "") String serie,
			@RequestParam(value = "placa", defaultValue = "") String placa,
			@RequestParam(value = "codigo", defaultValue = "") String codigo
			){
		
		LOG.info("ejecuta buscarasignacion");
		AsignacionRequest request = new AsignacionRequest();
		request.setIdProducto(idproducto);
		request.setMarca(marca);
		request.setModelo(modelo);
		request.setSerie(serie);
		request.setPlaca(placa);
		request.setCodigo(codigo);
		
		
		List<AsignacionBean> listaBean = new ArrayList<AsignacionBean>();
		try {
			listaBean = asignacionServicio.buscarAsignacion(request);
		} catch (Exception e) {
			LOG.warning("error en buscarasignacion");
			e.printStackTrace();
		}
		return listaBean;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value = "/addasignacion")
	public RespuestaBean registrarUsuario(final @RequestBody AsignacionBean asignacionBean, HttpServletRequest request) {
		
		LOG.info("ejecuta addasignacion");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			
			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				//System.out.println("sesion"+usuario.getId());
				asignacionBean.setIdUsuarioRegistro(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				asignacionBean.setIdUsuarioRegistro(Constantes.UNO);
			}
			
			boolean registro = asignacionServicio.registrarAsignacion(asignacionBean);
			
			respuesta.setCodigoRetorno(registro ? Constantes.UNO : Constantes.CERO);

			respuesta.setMensaje(registro ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_USUARIO_EXISTE);

			return respuesta;
			
		} catch (Exception e) {
			LOG.warning("error en addasignacion");
			
			e.printStackTrace();

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;
		}
		
	}
}
