package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.AgenteBean;
import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.SedeBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.AgenteRequest;
import com.unifacil.bean.request.SedeRequest;
import com.unifacil.bean.request.UnidadRequest;
import com.unifacil.servicio.SedeServicio;
import com.unifacil.util.Formateador;
import com.unifacil.util.constantes.Constantes;

@RestController
public class SedeControlador {

	@Autowired
	SedeServicio sedeServicio;

	@RequestMapping(value = "/listarsedes", method = RequestMethod.POST)
	private List<SedeBean> listarSedes(@RequestBody List<UnidadBean> listaUnidadBean) {
		List<SedeBean> listaUnidades = new ArrayList<SedeBean>();
		try {
			listaUnidades = sedeServicio.findAll(listaUnidadBean);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return listaUnidades;
	}

	@RequestMapping(value = "/buscarsede", method = RequestMethod.GET)
	private List<SedeBean> buscarSede(@RequestParam(value = "idUbicacion", defaultValue = "0") String idUbicacion,
			@RequestParam(value = "idUnidad", defaultValue = "0") String idUnidad,
			@RequestParam(value = "id", defaultValue = "0") String id,
			@RequestParam(value = "nombre", defaultValue = "") String nombre,
			@RequestParam(value = "usuario_perfil", defaultValue = "0") String usuario_perfil
			) {
		SedeRequest sedeRequest = new SedeRequest();
		sedeRequest.setIdUbicacion(Integer.parseInt(idUbicacion));
		sedeRequest.setIdUnidad(Integer.parseInt(idUnidad));
		sedeRequest.setId(Integer.parseInt(id));
		sedeRequest.setNombre(nombre);
		sedeRequest.setUsuario_perfil(Integer.parseInt(usuario_perfil));
		List<SedeBean> listaSedes = new ArrayList<SedeBean>();

		try {
			listaSedes = sedeServicio.buscarSede(sedeRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return listaSedes;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/registrarsede")
	public RespuestaBean registrarSede(final @RequestBody SedeBean sedeBean, HttpServletRequest request) {

		SedeRequest sedeRequest = new SedeRequest();
		sedeRequest.setIdUnidad(Integer.parseInt(sedeBean.getUnidad().getId()));
		sedeRequest.setIdUbicacion(Integer.parseInt(sedeBean.getUbicacion().getId()));
		sedeRequest.setNombre(sedeBean.getNombre());
		sedeRequest.setDireccion(sedeBean.getDireccion());
		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			sedeRequest.setIdUsuarioCreador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			sedeRequest.setIdUsuarioCreador(Constantes.UNO_INT);
		}

		int registro = sedeServicio.registrarSede(sedeRequest);

		RespuestaBean respuesta = new RespuestaBean();
		respuesta.setCodigoRetorno(registro > 0 ? Constantes.UNO : Constantes.CERO);
		respuesta.setMensaje(registro > 0 ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_SEDE_EXISTE);

		return respuesta;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/modificarsede")
	public RespuestaBean modificarSede(final @RequestBody SedeBean sedeBean, HttpServletRequest request) {

		SedeRequest sedeRequest = new SedeRequest();
		sedeRequest.setIdUnidad(Integer.parseInt(sedeBean.getUnidad().getId()));
		sedeRequest.setIdUbicacion(Integer.parseInt(sedeBean.getUnidad().getId()));
		sedeRequest.setNombre(sedeBean.getNombre());
		sedeRequest.setDireccion(sedeBean.getDireccion());
		sedeRequest.setId(Integer.parseInt(sedeBean.getId()));
		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			sedeRequest
					.setIdUsuarioModificador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			sedeRequest.setIdUsuarioModificador(Constantes.UNO_INT);
		}

		int registro = sedeServicio.modificarSede(sedeRequest);

		RespuestaBean respuesta = new RespuestaBean();
		respuesta.setCodigoRetorno(registro > 0 ? Constantes.UNO : Constantes.CERO);
		respuesta.setMensaje(registro > 0 ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_SEDE_EXISTE);

		return respuesta;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/eliminarsede")
	public RespuestaBean eliminarSede(final @RequestBody SedeBean sedeBean, HttpServletRequest request) {

		SedeRequest sedeRequest = new SedeRequest();
		sedeRequest.setId(Integer.parseInt(sedeBean.getId()));
		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			sedeRequest
					.setIdUsuarioModificador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			sedeRequest.setIdUsuarioModificador(Constantes.UNO_INT);
		}

		int registro = sedeServicio.eliminarSede(sedeRequest);

		RespuestaBean respuesta = new RespuestaBean();
		respuesta.setCodigoRetorno(registro > 0 ? Constantes.UNO : Constantes.CERO);
		respuesta.setMensaje(registro > 0 ? Constantes.MENSAJE_EXITO_ELIMINACION : Constantes.MENSAJE_ERROR_REGISTRO);

		return respuesta;
	}
}
