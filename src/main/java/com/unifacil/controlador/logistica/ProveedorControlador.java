package com.unifacil.controlador.logistica;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.ProveedorBean;
import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.ProveedorRequest;
import com.unifacil.servicio.ProveedorServicio;
import com.unifacil.util.constantes.Constantes;

@RestController
public class ProveedorControlador {

	@Autowired
	ProveedorServicio proveedorService;

	@RequestMapping(value = "/buscarproveedor", method = RequestMethod.GET)
	private List<ProveedorBean> buscarProveedor(@RequestParam(value = "categoria", defaultValue = "0") String categoria,
			@RequestParam(value = "razonSocial", defaultValue = "") String razonSocial,
			@RequestParam(value = "ruc", defaultValue = "") String ruc,
			@RequestParam(value = "telefono", defaultValue = "") String telefono,
			@RequestParam(value = "correo", defaultValue = "") String correo,
			@RequestParam(value = "contacto", defaultValue = "") String contacto,
			@RequestParam(value = "id", defaultValue = "0") String id) {

		ProveedorRequest request = new ProveedorRequest();
		request.setCategoria(categoria != null ? Integer.parseInt(categoria) : null);
		request.setRazonSocial(razonSocial);
		request.setRuc(ruc);
		request.setTelefono(telefono);
		request.setCorreo(correo);
		request.setContacto(contacto);
		request.setId(Integer.parseInt(id));
		return proveedorService.buscarProveedor(request);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/registrarproveedor")
	public RespuestaBean registrarProveedor(final @RequestBody ProveedorBean proveedorBean,
			HttpServletRequest request) {

		ProveedorRequest proveedorResquest = new ProveedorRequest();
		proveedorResquest.setCategoria(
				proveedorBean.getCategoria() != null ? Integer.parseInt(proveedorBean.getCategoria()) : null);
		proveedorResquest.setContacto(proveedorBean.getContacto());
		proveedorResquest.setCorreo(proveedorBean.getCorreo());
		proveedorResquest.setDireccion(proveedorBean.getDireccion());
		proveedorResquest.setRazonSocial(proveedorBean.getRazon());
		proveedorResquest.setRuc(proveedorBean.getRuc());
		proveedorResquest.setTelefono(proveedorBean.getTelefono());

		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			proveedorResquest
					.setUsuarioRegistro(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			proveedorResquest.setUsuarioRegistro(Constantes.UNO_INT);
		}

		boolean respuesta = proveedorService.registrarProveedor(proveedorResquest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_REGISTRO);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_PROVEEDOR_EXISTE);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/modificarproveedor")
	public RespuestaBean modificarProveedor(final @RequestBody ProveedorBean proveedorBean,
			HttpServletRequest request) {

		ProveedorRequest proveedorResquest = new ProveedorRequest();
		proveedorResquest.setCategoria(
				proveedorBean.getCategoria() != null ? Integer.parseInt(proveedorBean.getCategoria()) : null);
		proveedorResquest.setContacto(proveedorBean.getContacto());
		proveedorResquest.setCorreo(proveedorBean.getCorreo());
		proveedorResquest.setDireccion(proveedorBean.getDireccion());
		proveedorResquest.setId(proveedorBean.getId() != null ? Integer.parseInt(proveedorBean.getId()) : null);
		proveedorResquest.setRazonSocial(proveedorBean.getRazon());
		proveedorResquest.setRuc(proveedorBean.getRuc());
		proveedorResquest.setTelefono(proveedorBean.getTelefono());

		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			proveedorResquest
					.setUsuarioRegistro(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			proveedorResquest.setUsuarioRegistro(Constantes.UNO_INT);
		}

		boolean respuesta = proveedorService.modificarProveedor(proveedorResquest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_ACTUALIZACION);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_PROVEEDOR_EXISTE);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/eliminarproveedor")
	public RespuestaBean eliminarProveedor(@RequestBody ProveedorBean proveedorBean, HttpServletRequest request) {

		ProveedorRequest proveedorResquest = new ProveedorRequest();
		proveedorResquest.setId(Integer.parseInt(proveedorBean.getId()));
		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			proveedorResquest
					.setUsuarioRegistro(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			proveedorResquest.setUsuarioRegistro(Constantes.UNO_INT);
		}
		boolean respuesta = proveedorService.eliminarProveedor(proveedorResquest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_ELIMINACION);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);
		}
		return bean;
	}

}
