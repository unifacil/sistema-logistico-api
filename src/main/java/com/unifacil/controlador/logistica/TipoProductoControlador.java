package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.TipoProductoBean;
import com.unifacil.bean.request.TipoProductoRequest;
import com.unifacil.servicioImpl.TipoProductoServicioImpl;

@RestController
public class TipoProductoControlador {
	
	@Autowired 
	TipoProductoServicioImpl tipoProductoServicio;
	
	private static final Logger LOG = Logger.getLogger(TipoProductoControlador.class.getName());
	
	@RequestMapping(value = "/listartipoproducto", method = RequestMethod.GET)
	private List<TipoProductoBean> listarTipoProducto(@RequestParam(value = "subcategoriaid", defaultValue = "0") 
	int subcategoriaid) {
		LOG.info("ejecuta listartipoproducto");
		TipoProductoRequest request = new TipoProductoRequest();
		request.setIdSubcategoria(subcategoriaid);

		List<TipoProductoBean> listaTipoProducto = new ArrayList<TipoProductoBean>();
		try {
			listaTipoProducto = tipoProductoServicio.listarTipoProducto(request);
		} catch (Exception ex) {
			LOG.warning("error en listartipoproducto");
		}

		return listaTipoProducto;
	}

}
