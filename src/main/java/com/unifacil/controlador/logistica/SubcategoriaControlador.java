package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.SubcategoriaBean;
import com.unifacil.bean.SubcategoriaRequest;
import com.unifacil.servicioImpl.SubcategoriaServicioImpl;



@RestController
public class SubcategoriaControlador {
	
	@Autowired 
	SubcategoriaServicioImpl subcategoriaServicio;
	
	
	private static final Logger LOG = Logger.getLogger(SubcategoriaControlador.class.getName());
	
	@RequestMapping(value = "/listarsubcategoria", method = RequestMethod.GET)
	private List<SubcategoriaBean> listarSubcategoria(@RequestParam(value = "categoriaid", defaultValue = "0") 
	int categoriaid) {
		LOG.info("ejecuta listarsubcategoria");
		SubcategoriaRequest request = new SubcategoriaRequest();
		request.setIdCategoria(categoriaid);

		List<SubcategoriaBean> listaSubcategoria = new ArrayList<SubcategoriaBean>();
		try {
			listaSubcategoria = subcategoriaServicio.listarSubcategoria(request);
		} catch (Exception ex) {
			LOG.warning("error en listarusuarios");
		}

		return listaSubcategoria;
	}
}
