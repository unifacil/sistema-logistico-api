package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.unifacil.bean.AgenteBean;
import com.unifacil.bean.FileBean;
import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.AgenteRequest;
import com.unifacil.servicio.AgenteServicio;
import com.unifacil.util.Formateador;
import com.unifacil.util.PlantillaMetodos;
import com.unifacil.util.constantes.Constantes;
import com.unifacil.util.convertidores.AgenteConvertidor;

@RestController
public class AgenteControlador {

	@Autowired
	public AgenteServicio agenteServicio;

	@Autowired
	public AgenteConvertidor agenteConvertidor;
	@Value("${urlFile}")
	String URL_FILE;
	private static final Logger LOG = Logger.getLogger(AgenteControlador.class.getName());

	@RequestMapping(value = "/buscaragente", method = RequestMethod.GET)
	private List<AgenteBean> buscarAgente(@RequestParam(value = "nombre", defaultValue = "") String nombre,
			@RequestParam(value = "dni", defaultValue = "") String dni,
			@RequestParam(value = "carnet", defaultValue = "") String carnet,
			@RequestParam(value = "fecha_ini", defaultValue = "") String fecha_ini,
			@RequestParam(value = "fecha_hasta", defaultValue = "") String fecha_hasta,
			@RequestParam(value = "fecha_ces_ini", defaultValue = "") String fecha_ces_ini,
			@RequestParam(value = "fecha_ces_hasta", defaultValue = "") String fecha_ces_hasta,
			@RequestParam(value = "ubicacion", defaultValue = "") String ubicacion,
			@RequestParam(value = "unidad", defaultValue = "") String unidad,
			@RequestParam(value = "sede", defaultValue = "") String sede,
			@RequestParam(value = "id", defaultValue = "0") String id,
			@RequestParam(value = "usuario_perfil", defaultValue = "0") String usuario_perfil
			) {
		AgenteRequest agenteRequest = new AgenteRequest();
		agenteRequest.setNombres(nombre);
		agenteRequest.setDni(dni);
		agenteRequest.setCarnet(carnet);
		agenteRequest.setFecha_Ini(Formateador.convertirStringToDate(fecha_ini));
		agenteRequest.setFecha_Hasta(Formateador.convertirStringToDate(fecha_hasta));
		agenteRequest.setFecha_ces_Ini(Formateador.convertirStringToDate(fecha_ces_ini));
		agenteRequest.setFecha_ces_Hasta(Formateador.convertirStringToDate(fecha_ces_hasta));
		agenteRequest.setUbicacion(ubicacion);
		agenteRequest.setUnidad(unidad);
		agenteRequest.setSedes(sede);
		agenteRequest.setId(Integer.parseInt(id));
		agenteRequest.setUsuario_perfil(Integer.parseInt(usuario_perfil));

		List<AgenteBean> listaAgentes = new ArrayList<AgenteBean>();
		try {
			listaAgentes = agenteServicio.buscarAgente(agenteRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return listaAgentes;

	}

	@RequestMapping(value = "/listaragentes", method = RequestMethod.GET)
	private List<AgenteBean> buscarAgente() {
		AgenteRequest agenteRequest = new AgenteRequest();

		List<AgenteBean> listaAgentes = new ArrayList<AgenteBean>();
		try {
			listaAgentes = agenteServicio.buscarAgente(agenteRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return listaAgentes;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/registraragente")
	public RespuestaBean registrarAgente(final @RequestBody AgenteBean agenteBean, HttpServletRequest request) {

		AgenteRequest agenteRequest = new AgenteRequest();

		agenteRequest.setNombres(agenteBean.getNombres());
		agenteRequest.setApellidos(agenteBean.getApellidos());
		agenteRequest.setDni(agenteBean.getDni());
		agenteRequest.setLicencia(agenteBean.getLicencia());
		agenteRequest.setCarnet(agenteBean.getCarnet());
		agenteRequest.setFecha_Ini(Formateador.convertirStringToDate(agenteBean.getFechaInit()));
		agenteRequest.setCelular(agenteBean.getCelular());
		agenteRequest.setDireccion(agenteBean.getDireccion());
		agenteRequest.setSedes(agenteBean.listaSedes.stream().map(x -> x.getId()).map(x -> x.toString())
				.collect(Collectors.joining(",")));

		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			agenteRequest.setIdUsuarioCreador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			agenteRequest.setIdUsuarioCreador(Constantes.UNO_INT);
		}

		boolean respuesta = agenteServicio.registrarAgente(agenteRequest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_REGISTRO);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_AGENTE_EXISTE);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/modificaragente")
	public RespuestaBean modificarAgente(final @RequestBody AgenteBean agenteBean, HttpServletRequest request) {

		AgenteRequest agenteRequest = new AgenteRequest();

		agenteRequest.setNombres(agenteBean.getNombres());
		agenteRequest.setApellidos(agenteBean.getApellidos());
		agenteRequest.setDni(agenteBean.getDni());
		agenteRequest.setLicencia(agenteBean.getLicencia());
		agenteRequest.setCarnet(agenteBean.getCarnet());
		agenteRequest.setFecha_Ini(Formateador.convertirStringToDate(agenteBean.getFechaInit()));
		agenteRequest.setFecha_ces_Ini(Formateador.convertirStringToDate(agenteBean.getFechaCese()));
		agenteRequest.setCelular(agenteBean.getCelular());
		agenteRequest.setDireccion(agenteBean.getDireccion());
		agenteRequest.setSedes(agenteBean.listaSedes.stream().map(x -> x.getId()).map(x -> x.toString())
				.collect(Collectors.joining(",")));
		agenteRequest.setId(Integer.parseInt(agenteBean.getId()));

		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			agenteRequest
					.setIdUsuarioModificador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			agenteRequest.setIdUsuarioModificador(Constantes.UNO_INT);
		}

		boolean respuesta = agenteServicio.modificarAgente(agenteRequest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_REGISTRO);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_AGENTE_EXISTE);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/eliminaragente")
	public RespuestaBean eliminarAgente(final @RequestBody AgenteBean agenteBean, HttpServletRequest request) {
		AgenteRequest agenteRequest = new AgenteRequest();

		agenteRequest.setId(Integer.parseInt(agenteBean.getId()));
		HttpSession sesion = request.getSession();
		if (sesion != null) {
			UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
			agenteRequest
					.setIdUsuarioModificador(usuario != null ? Integer.parseInt(usuario.getId()) : Constantes.UNO_INT);
		} else {
			agenteRequest.setIdUsuarioModificador(Constantes.UNO_INT);
		}

		boolean respuesta = agenteServicio.eliminarAgente(agenteRequest);
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_ELIMINACION);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/cargarLicencia")
	public RespuestaBean cargarLincencia(@RequestParam("licencia") MultipartFile uploadfile) {

		boolean respuesta = agenteServicio.cargarLicencia(Arrays.asList(uploadfile));
		RespuestaBean bean = new RespuestaBean();
		if (respuesta) {
			bean.setCodigoRetorno(Constantes.UNO);
			bean.setMensaje(Constantes.MENSAJE_EXITO_REGISTRO);
		} else {
			bean.setCodigoRetorno(Constantes.CERO);
			bean.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);
		}
		return bean;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/descargarLicencia")
	public FileBean descargarLicencia(@RequestParam("licencia") String archivo) {

		FileBean fileBean = new FileBean();
		fileBean.setEstado(Constantes.ACTIVO);
		fileBean.setContent(PlantillaMetodos.obtenerArrayArchivo(URL_FILE.concat(archivo)));
		fileBean.setNombreArchivo(archivo);

		return fileBean;
	}
}
