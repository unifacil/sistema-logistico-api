package com.unifacil.controlador.logistica;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unifacil.bean.InventarioBean;
import com.unifacil.bean.InventarioDetalleBean;
import com.unifacil.bean.RespuestaBean;
import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.InventarioRequest;
import com.unifacil.servicioImpl.InventarioServicioImpl;
import com.unifacil.util.constantes.Constantes;

@RestController
public class InventarioControlador {

	@Autowired
	InventarioServicioImpl inventarioServicio;

	private static final Logger LOG = Logger.getLogger(InventarioControlador.class.getName());
	
	@RequestMapping(value = "/buscarinventario", method = RequestMethod.GET)
	private List<InventarioBean> buscarInventario(@RequestParam(value = "idCategoria", defaultValue = "0") int idCategoria,
			@RequestParam(value = "idSubCategoria", defaultValue = "0") int idSubCategoria,
			@RequestParam(value = "tipo", defaultValue = "0") int tipo,
			@RequestParam(value = "marca", defaultValue = "") String marca,
			@RequestParam(value = "modelo", defaultValue = "") String modelo,
			@RequestParam(value = "serie", defaultValue = "") String serie,
			@RequestParam(value = "placa", defaultValue = "") String placa,
			@RequestParam(value = "codigo", defaultValue = "") String codigo,
			@RequestParam(value = "talla", defaultValue = "") String talla){
		InventarioRequest request = new InventarioRequest();
		request.setCodigo(codigo);
		request.setIdCategoria(idCategoria);
		request.setIdSubCategoria(idSubCategoria);
		request.setMarca(marca);
		request.setModelo(modelo);
		request.setPlaca(placa);
		request.setSerie(serie);
		request.setTalla(talla);
		request.setTipo(tipo);
		
		List<InventarioBean> listaInventarioBean = new ArrayList<InventarioBean>();
		
		try {
			listaInventarioBean = inventarioServicio.buscarInventario(request);
		} catch (Exception e) {
			LOG.warning("error en buscarinventario");
			e.printStackTrace();
		}
		
		return listaInventarioBean;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/addinventario")
	public RespuestaBean registrarInventario(
			final @RequestBody InventarioDetalleBean inventarioDetalleBean, HttpServletRequest request) {
		LOG.info("ejecuta addinventario");
		RespuestaBean respuesta = new RespuestaBean();
		try {
			
			HttpSession sesion = request.getSession();
			if (sesion != null) {
				UsuarioBean usuario = (UsuarioBean) sesion.getAttribute(Constantes.USUARIO);
				//System.out.println("sesion"+usuario.getId());
				inventarioDetalleBean.setUsuarioRegistrador(usuario != null ? usuario.getId() : Constantes.UNO);
			} else {
				inventarioDetalleBean.  setUsuarioRegistrador(Constantes.UNO);
			}
			
			boolean registro = inventarioServicio.registrarInventario(inventarioDetalleBean);
			
			respuesta.setCodigoRetorno(registro? Constantes.UNO : Constantes.CERO);
			
			respuesta.setMensaje(registro ? Constantes.MENSAJE_EXITO_REGISTRO : Constantes.MENSAJE_ERROR_REGISTRO);

			return respuesta;
			
		} catch (Exception e) {
			LOG.warning("error en addusuario");

			respuesta.setMensaje(Constantes.MENSAJE_ERROR_REGISTRO);

			respuesta.setCodigoRetorno(Constantes.CERO);

			return respuesta;
		}
		
		
	}
	
	
	
}
