package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.InventarioRequest;
import com.unifacil.entidad.Inventario;
import com.unifacil.entidad.InventarioDetalle;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;;

@Component
public class InventarioAccess {
	
	
	private JdbcSQLServer db;

	private static final Logger LOG = Logger.getLogger(UsuarioAccess.class.getName());
	
	private static final String SUBCATEGORIA = "SUBCATEGORIA";
	private static final String TIPO = "TIPO";
	private static final String INV_NOMBRE = "INV_NOMBRE";
	private static final String INV_MARCA = "INV_MARCA";
	private static final String INV_MODELO = "INV_MODELO";
	private static final String INV_SERIE = "INV_SERIE";
	private static final String PRO_RAZON_SOCIAL = "PRO_RAZON_SOCIAL";
	private static final String UBI_NOMBRE = "UBI_NOMBRE";
	private static final String INV_RESOLUCION = "INV_RESOLUCION";
	private static final String INV_PLACA = "INV_PLACA";
	private static final String INV_HDD = "INV_HDD";
	private static final String INV_RAM = "INV_RAM";
	private static final String INV_SO = "INV_SO";
	private static final String INV_VOLTAJE = "INV_VOLTAJE";
	private static final String INV_CODIGO = "INV_CODIGO";
	private static final String INV_COLOR = "INV_COLOR";
	private static final String INV_TARJ_PROP = "INV_TARJ_PROP";
	private static final String INV_FOTO = "INV_FOTO";
	private static final String INV_TALLA = "INV_TALLA";
	private static final String CANT = "CANT";
	private static final String FECHA_COMPRA = "FECHA_COMPRA";
	private static final String INV_STOCK = "INV_STOCK";
	private static final String ID_MED_DETALLE = "ID_MED_DETALLE";
	private static final String INV_ID = "INV_ID";
	private static final String SUB_ID = "SUB_ID";
	private static final String TPR_ID = "TPR_ID";
	
	private static final String RESULTADO = "RESULTADO";
	
	private InventarioAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public boolean registrarInventario(InventarioDetalle inventarioDetalle){
		CallableStatement cs = null;
		boolean respuesta =false;
		try {
			db.openConnection();
			
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_INVENTARIO(?,?,?,?,?,?,?,?,?,?  ,?,?,?,?,?,?,?,?,?,?  ,?,?,? )}");
			cs.setInt(1, inventarioDetalle.getInventario().getTipoProducto().getId().intValue());
			cs.setString(2, inventarioDetalle.getInventario().getNombre());
			cs.setInt(3, inventarioDetalle.getInventario().getProveedor().getId());
			cs.setInt(4,inventarioDetalle.getInventario().getUbicacion().getId()==null?0: inventarioDetalle.getInventario().getUbicacion().getId().intValue());
			cs.setString(5, inventarioDetalle.getInventario().getTalla());
			cs.setString(6, inventarioDetalle.getInventario().getMarca());
			cs.setString(7, inventarioDetalle.getInventario().getModelo());
			cs.setString(8, inventarioDetalle.getInventario().getSerie());
			cs.setString(9, inventarioDetalle.getInventario().getResolucion());
			cs.setString(10, inventarioDetalle.getInventario().getPlaca());
			cs.setString(11, inventarioDetalle.getInventario().getHdd());
			cs.setString(12, inventarioDetalle.getInventario().getRam());
			cs.setString(13, inventarioDetalle.getInventario().getSo());
			cs.setString(14, inventarioDetalle.getInventario().getVoltaje());
			cs.setString(15, inventarioDetalle.getInventario().getCodigo());
			cs.setString(16, inventarioDetalle.getInventario().getColor());
			cs.setString(17, inventarioDetalle.getInventario().getTarj_prop());
			cs.setString(18, inventarioDetalle.getInventario().getFoto());
			cs.setInt(19, inventarioDetalle.getInventario().getStock());
			cs.setString(20, inventarioDetalle.getFechaCompra());
			//cs.setDate(20, null);
			//cs.setInt(21, inventarioDetalle.getCantidad());
			cs.setInt(21,inventarioDetalle.getCantidad());//Estaba enviando 0
			//cs.setInt(22, inventarioDetalle.getInventario().getId().intValue());
			cs.setInt(22, inventarioDetalle.getInventario().getId().intValue()); //Estaba enviando 0
			cs.setInt(23, inventarioDetalle.getUsuarioRegistrador());
			
			//respuesta = cs.execute();
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		}catch(SQLException e){
			LOG.warning(e.getMessage());
		}
		catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return respuesta;
	}
	
	public List<Inventario> buscarInventario(InventarioRequest request){
		List<Inventario> listaInventario = null;
		CallableStatement cs = null;
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			db.openConnection();
			
			listaInventario = new ArrayList<Inventario>();
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_INVENTARIO(?,?,?,?,?,?,?,?,?)}");
			cs.setInt(1, request.getIdCategoria());
			cs.setInt(2, request.getIdSubCategoria());
			cs.setInt(3, request.getTipo());
			cs.setString(4, request.getMarca());
			cs.setString(5, request.getModelo());
			cs.setString(6, request.getSerie());
			cs.setString(7, request.getPlaca());
			cs.setString(8, request.getCodigo());
			cs.setString(9, request.getTalla());

			ResultSet rs = cs.executeQuery();
			
			Inventario item;
			while(rs.next()){
				item = new Inventario();
				item.getTipoProducto().getSubcategoria().setNombre(rs.getString(SUBCATEGORIA));
				item.getTipoProducto().setNombre(rs.getString(TIPO));
				item.setNombre(rs.getString(INV_NOMBRE));
				item.setMarca(rs.getString(INV_MARCA));
				item.setModelo(rs.getString(INV_MODELO));
				item.setSerie(rs.getString(INV_SERIE));
				item.getProveedor().setRazonSocial(rs.getString(PRO_RAZON_SOCIAL));
				item.getUbicacion().setNombres(rs.getString(UBI_NOMBRE));
				item.setResolucion(rs.getString(INV_RESOLUCION));
				item.setPlaca(rs.getString(INV_PLACA));
				item.setHdd(rs.getString(INV_HDD));
				item.setRam(rs.getString(INV_RAM));
				item.setSo(rs.getString(INV_SO));
				item.setVoltaje(rs.getString(INV_VOLTAJE));
				item.setCodigo(rs.getString(INV_CODIGO));
				item.setColor(rs.getString(INV_COLOR));
				item.setTarj_prop(rs.getString(INV_TARJ_PROP));
				item.setFoto(rs.getString(INV_FOTO));
				item.setTalla(rs.getString(INV_TALLA));
				
				item.setCantidad(rs.getInt(CANT));
				item.setFechaCompra(rs.getString(FECHA_COMPRA));
				item.setStock(rs.getInt(INV_STOCK));
				item.setId_med_detalle(rs.getInt(ID_MED_DETALLE));
				item.setId(rs.getLong(INV_ID));
				item.getTipoProducto().getSubcategoria().setId(rs.getLong(SUB_ID));
				item.getTipoProducto().setId(rs.getLong(TPR_ID));
				
				listaInventario.add(item);
			}
			if(listaInventario != null){
				LOG.info("inventario encontrados:" + listaInventario.toString());
			}
			
		} catch(SQLException e){
			LOG.warning(e.getMessage());
		}
		catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaInventario;
	}
	
}
