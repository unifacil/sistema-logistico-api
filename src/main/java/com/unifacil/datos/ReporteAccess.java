package com.unifacil.datos;


import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.ReporteRequest;
import com.unifacil.entidad.Reporte;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;

@Component
public class ReporteAccess {

	private JdbcSQLServer db;

	private static final Logger LOG = Logger.getLogger(ReporteAccess.class.getName());
	
	private static final String INV_ID = "INV_ID";
	private static final String INV_NOMBRE = "INV_NOMBRE";
	private static final String INV_MARCA = "INV_MARCA";
	private static final String INV_MODELO = "INV_MODELO";
	private static final String INV_SERIE = "INV_SERIE";
	private static final String INV_RESOLUCION = "INV_RESOLUCION";
	private static final String INV_PLACA = "INV_PLACA";
	private static final String INV_HDD = "INV_HDD";
	private static final String INV_RAM = "INV_RAM";
	private static final String INV_SO = "INV_SO";
	private static final String INV_VOLTAJE = "INV_VOLTAJE";
	private static final String INV_CODIGO = "INV_CODIGO";
	private static final String INV_COLOR = "INV_COLOR";
	private static final String INV_TALLA = "INV_TALLA";
	private static final String INV_TARJ_PROP = "INV_TARJ_PROP";
	private static final String INV_FOTO = "INV_FOTO";
	private static final String ESTADO = "ESTADO";
	private static final String INV_USUARIO_REGIS = "INV_USUARIO_REGIS";
	private static final String INV_FEC_REGIS = "INV_FEC_REGIS";
	private static final String INV_USUARIO_MODI = "INV_USUARIO_MODI";
	private static final String INV_FEC_MODI = "INV_FEC_MODI";
	private static final String IND_CANTIDAD = "IND_CANTIDAD";
	private static final String IND_FECHA_COMPRA = "IND_FECHA_COMPRA";
	private static final String IND_USUARIO_REGIS = "IND_USUARIO_REGIS";
	private static final String IND_FEC_REGIS = "IND_FEC_REGIS";
	private static final String IND_USUARIO_MODI = "IND_USUARIO_MODI";
	private static final String IND_FEC_MODI = "IND_FEC_MODI";
	
	private static final String AVP_ID = "AVP_ID";
	private static final String UNI_ID = "UNI_ID";
	private static final String SEDE_ID = "SEDE_ID";
	private static final String ASI_CANT = "ASI_CANT";
	private static final String ASI_FECHA_ASIGNA = "ASI_FECHA_ASIGNA";
	
	private static final String SUBCATEGORIA = "SUBCATEGORIA";
	private static final String TIPO = "TIPO";
	private static final String PRO_RAZON_SOCIAL = "PRO_RAZON_SOCIAL";
	private static final String UBI_NOMBRE = "UBI_NOMBRE";
	private static final String INV_STOCK = "INV_STOCK";
	
	private ReporteAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public List<Reporte> obtenerReporte(ReporteRequest request){
		List<Reporte> listaReporte = null;
		CallableStatement cs = null;
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			db.openConnection();
			
			LOG.info("parametro :"+request.getParametro());
			LOG.info("Fecha desde :"+request.getFechaDesde());
			LOG.info("Fecha hasta :"+request.getFechaHasta());
			listaReporte = new ArrayList<Reporte>();
			cs = db.getConnection().prepareCall("{call USP_GENERAR_REPORTE(?,?,?)}");
			cs.setInt(1, request.getParametro());
			cs.setString(2, request.getFechaDesde());
			cs.setString(3, request.getFechaHasta());
			
			ResultSet rs = cs.executeQuery();
			
			Reporte item;
			
			while(rs.next()){
				item = new Reporte();
				if(request.getParametro()==Constantes.REPORTE_ACTUAL){
					LOG.info("Reporte Actual");
					item.getInventario().getTipoProducto().getSubcategoria().setNombre(rs.getString(SUBCATEGORIA));
					item.getInventario().getTipoProducto().setNombre(rs.getString(TIPO));
					item.getInventario().setNombre(rs.getString(INV_NOMBRE));
					item.getInventario().setMarca(rs.getString(INV_MARCA));
					item.getInventario().setModelo(rs.getString(INV_MODELO));
					item.getInventario().setSerie(rs.getString(INV_SERIE));
					item.getInventario().getProveedor().setRazonSocial(rs.getString(PRO_RAZON_SOCIAL));
					item.getInventario().getUbicacion().setNombres(rs.getString(UBI_NOMBRE));
					item.getInventario().setResolucion(rs.getString(INV_RESOLUCION));
					item.getInventario().setPlaca(rs.getString(INV_PLACA));
					item.getInventario().setHdd(rs.getString(INV_HDD));
					item.getInventario().setRam(rs.getString(INV_RAM));
					item.getInventario().setSo(rs.getString(INV_SO));
					item.getInventario().setVoltaje(rs.getString(INV_VOLTAJE));
					item.getInventario().setCodigo(rs.getString(INV_CODIGO));
					item.getInventario().setColor(rs.getString(INV_COLOR));
					item.getInventario().setTarj_prop(rs.getString(INV_TARJ_PROP));
					item.getInventario().setFoto(rs.getString(INV_FOTO));
					item.getInventario().setTalla(rs.getString(INV_TALLA));
					item.getInventario().setStock(rs.getInt(INV_STOCK));
				}
				else if(request.getParametro() == Constantes.REPORTE_INGRESO){
					LOG.info("Reporte Ingreso");
					item.getInventarioDetalle().getInventario().setId(rs.getLong(INV_ID));
					item.getInventarioDetalle().getInventario().setNombre(rs.getString(INV_NOMBRE));
					item.getInventarioDetalle().getInventario().setMarca(rs.getString(INV_MARCA));
					item.getInventarioDetalle().getInventario().setModelo(rs.getString(INV_MODELO));
					item.getInventarioDetalle().getInventario().setSerie(rs.getString(INV_SERIE));
					item.getInventarioDetalle().getInventario().setResolucion(rs.getString(INV_RESOLUCION));
					item.getInventarioDetalle().getInventario().setPlaca(rs.getString(INV_PLACA));
					item.getInventarioDetalle().getInventario().setHdd(rs.getString(INV_HDD));
					item.getInventarioDetalle().getInventario().setRam(rs.getString(INV_RAM));
					item.getInventarioDetalle().getInventario().setSo(rs.getString(INV_SO));
					item.getInventarioDetalle().getInventario().setVoltaje(rs.getString(INV_VOLTAJE));
					item.getInventarioDetalle().getInventario().setCodigo(rs.getString(INV_CODIGO));
					item.getInventarioDetalle().getInventario().setColor(rs.getString(INV_COLOR));
					item.getInventarioDetalle().getInventario().setTalla(rs.getString(INV_TALLA));
					item.getInventarioDetalle().getInventario().setTarj_prop(rs.getString(INV_TARJ_PROP));
					item.getInventarioDetalle().getInventario().setFoto(rs.getString(INV_FOTO));
					item.getInventarioDetalle().getInventario().setEstado(Constantes.ACTIVO.equals(rs.getString(ESTADO))? true:false);
					item.getInventarioDetalle().getInventario().setUsuarioRegistrador(rs.getInt(INV_USUARIO_REGIS));
					item.getInventarioDetalle().getInventario().setFechaRegistro(rs.getString(INV_FEC_REGIS));
					item.getInventarioDetalle().getInventario().setUsuarioModificador(rs.getInt(INV_USUARIO_MODI));
					item.getInventarioDetalle().getInventario().setFechaModificacion(rs.getString(INV_FEC_MODI));
					item.getInventarioDetalle().setCantidad(rs.getInt(IND_CANTIDAD));
					item.getInventarioDetalle().setFechaCompra(rs.getString(IND_FECHA_COMPRA));
					item.getInventarioDetalle().setUsuarioRegistrador(rs.getInt(IND_USUARIO_REGIS));
					item.getInventarioDetalle().setFechaRegistro(rs.getString(IND_FEC_REGIS));
					item.getInventarioDetalle().setUsuarioModificador(rs.getInt(IND_USUARIO_MODI));
					item.getInventarioDetalle().setFechaModificacion(rs.getString(IND_FEC_MODI));
				}else if(request.getParametro() == Constantes.REPORTE_SALIDA){
					LOG.info("Reporte Salida");
					item.getAsignacion().getInventario().setId(rs.getLong(INV_ID));
					item.getAsignacion().getInventario().setNombre(rs.getString(INV_NOMBRE));
					item.getAsignacion().getInventario().setMarca(rs.getString(INV_MARCA));
					item.getAsignacion().getInventario().setModelo(rs.getString(INV_MODELO));
					item.getAsignacion().getInventario().setSerie(rs.getString(INV_SERIE));
					item.getAsignacion().getInventario().setResolucion(rs.getString(INV_RESOLUCION));
					item.getAsignacion().getInventario().setPlaca(rs.getString(INV_PLACA));
					item.getAsignacion().getInventario().setHdd(rs.getString(INV_HDD));
					item.getAsignacion().getInventario().setRam(rs.getString(INV_RAM));
					item.getAsignacion().getInventario().setSo(rs.getString(INV_SO));
					item.getAsignacion().getInventario().setVoltaje(rs.getString(INV_VOLTAJE));
					item.getAsignacion().getInventario().setCodigo(rs.getString(INV_CODIGO));
					item.getAsignacion().getInventario().setColor(rs.getString(INV_COLOR));
					item.getAsignacion().getInventario().setTalla(rs.getString(INV_TALLA));
					item.getAsignacion().getInventario().setTarj_prop(rs.getString(INV_TARJ_PROP));
					item.getAsignacion().getInventario().setFoto(rs.getString(INV_FOTO));
					item.getAsignacion().getInventario().setEstado(Constantes.ACTIVO.equals(rs.getString(ESTADO))? true:false);
					item.getAsignacion().getInventario().setUsuarioRegistrador(rs.getInt(INV_USUARIO_REGIS));
					item.getAsignacion().getInventario().setFechaRegistro(rs.getString(INV_FEC_REGIS));
					item.getAsignacion().getInventario().setUsuarioModificador(rs.getInt(INV_USUARIO_MODI));
					item.getAsignacion().getInventario().setFechaModificacion(rs.getString(INV_FEC_MODI));
					item.getAsignacion().getAgente().setId(rs.getLong(AVP_ID));
					item.getAsignacion().getUnidad().setId(rs.getLong(UNI_ID));
					item.getAsignacion().getSede().setId(rs.getLong(SEDE_ID));
					item.getAsignacion().setCantidad(rs.getInt(ASI_CANT));
					item.getAsignacion().setFechaAsignacion(rs.getString(ASI_FECHA_ASIGNA));
				}
				listaReporte.add(item);
				
				
			}
			LOG.info("Lista reporte : "+listaReporte.toString());
		} catch (SQLException ex) {
			LOG.warning(ex.getMessage());
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaReporte;
	}
	
}
