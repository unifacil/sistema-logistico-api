package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.TipoProductoRequest;
import com.unifacil.entidad.TipoProducto;
import com.unifacil.util.conexion.JdbcSQLServer;
import java.util.logging.Logger;

@Component
public class TipoProductoAccess {
	
	
	private static final String TPR_ID = "TPR_ID";
	private static final String TPR_NOMBRE = "TPR_NOMBRE";
	private JdbcSQLServer db;
	private static final Logger LOG = Logger.getLogger(TipoProductoAccess.class.getName());

	private TipoProductoAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public List<TipoProducto> listarSubcategoria(TipoProductoRequest request) {
		CallableStatement cs = null;
		List<TipoProducto> listaTipoProducto = null;
		try {
			listaTipoProducto = new ArrayList<TipoProducto>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_TIPO(?)}");
			cs.setInt(1, request.getIdSubcategoria());
			ResultSet rs = cs.executeQuery();

			TipoProducto item;
			while (rs.next()) {
				item = new TipoProducto();

				item.setId(rs.getLong(TPR_ID));
				item.setNombre(rs.getString(TPR_NOMBRE));

				listaTipoProducto.add(item);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaTipoProducto;
	}
	
}
