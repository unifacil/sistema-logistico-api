package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.UnidadRequest;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.entidad.Unidad;
import com.unifacil.util.conexion.JdbcSQLServer;

@Component
public class UnidadAccess {
	
	private JdbcSQLServer db;
	
	static final String UNI_ID = "UNI_ID";
	static final String UNI_NOMBRE_RAZON = "UNI_NOMBRE_RAZON";
	static final String UBI_NOMBRE = "UBI_NOMBRE";
	static final String UBI_RESPONSABLE = "UBI_RESPONSABLE";
	static final String UNI_ABREVIATURA = "UNI_ABREVIATURA";
	static final String ESTADO = "ESTADO";
	static final String FECHA_REGISTRO = "FECHA_REGISTRO";
	static final String UBI_ID = "UBI_ID";
	static final String ACTIVO = "ACTIVO";
	private static final String RESULTADO = "RESULTADO";
	
	private static final Logger LOG = Logger.getLogger(UnidadAccess.class.getName());
	
	private UnidadAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public List<Unidad> listarUnidades(List<Ubicacion> ubicaciones){
		CallableStatement cs = null;
		List<Unidad> listaUnidades = null;
		
		try {
			listaUnidades = new ArrayList<Unidad>();			
	
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_UNIDAD(?)}");
			cs.setString(1, ubicaciones.stream().map(x -> x.getId()).map(x -> x.toString())
					.collect(Collectors.joining(",")));
			ResultSet rs = cs.executeQuery();

			Unidad item;
			
			while(rs.next()){
				item = new Unidad();
				
				item.setId(rs.getLong(UNI_ID));
				item.setNombreRazon(rs.getString(UNI_NOMBRE_RAZON));
				
				listaUnidades.add(item);
			}
					
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaUnidades;
	}
	
	public List<Unidad> buscarUnidad(UnidadRequest unidadRequest){
		CallableStatement cs = null;
		List<Unidad> listaUnidades = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			listaUnidades = new ArrayList<Unidad>();			
	
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_UNIDAD(?,?,?,?,?)}");
			
			cs.setInt(1, unidadRequest.getIdUbicacion());
			cs.setInt(2, unidadRequest.getIdUnidad());
			cs.setString(3, unidadRequest.getNombre());
			cs.setString(4, unidadRequest.getAbreviatura());
			cs.setInt(5, unidadRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Unidad item;
			
			while(rs.next()){
				item = new Unidad();
				item.getUbicacion().setNombres(rs.getString(UBI_NOMBRE));
				item.getUbicacion().setResponsable(rs.getString(UBI_RESPONSABLE));
				item.setNombreRazon(rs.getString(UNI_NOMBRE_RAZON));
				item.setAbreviatura(rs.getString(UNI_ABREVIATURA));
				item.setEstado(ACTIVO.equals(rs.getString(ESTADO))?true:false);
				item.setFechaRegistro(new java.sql.Date(formatter.parse(rs.getString(FECHA_REGISTRO)).getTime()));
				item.getUbicacion().setId(new Long(rs.getInt(UBI_ID)));
				
				item.setId(new Long(rs.getInt(UNI_ID)));
				
				listaUnidades.add(item);
			}
					
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaUnidades;
	}
	
	public int registrarUnidad(Unidad unidad){
		
		CallableStatement cs = null;
		int registro = 0;
		
		try {
	
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_UNIDAD(?,?,?,?)}");
			cs.setInt(1, unidad.getUbicacion().getId().intValue());
			cs.setString(2, unidad.getNombreRazon());
			cs.setString(3, unidad.getAbreviatura());
			cs.setInt(4, unidad.getUsuarioRegistro());
			ResultSet rs = cs.executeQuery();

			if(rs.next()){
				registro = rs.getInt(1);
			}
					
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return registro;
	}
	
	public int actualizarUnidad(Unidad unidad){
		
		CallableStatement cs = null;
		int actualizo = 0;
		
		try {
	
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_UNIDAD(?,?,?,?,?)}");
			cs.setInt(1, unidad.getUbicacion().getId().intValue());
			cs.setString(2, unidad.getNombreRazon());
			cs.setString(3, unidad.getAbreviatura());
			cs.setInt(4, unidad.getId().intValue());
			cs.setInt(5, unidad.getUsuarioModificacion());
			ResultSet rs = cs.executeQuery();

			if(rs.next()){
				actualizo = rs.getInt(1);
			}
					
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return actualizo;
	}
	
	public int eliminarUnidad(Unidad unidad){

		CallableStatement cs = null;
		int actualizo = 0;
		
		try {
	
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_UNIDAD(?,?)}");
			cs.setInt(1, unidad.getId().intValue());
			cs.setInt(2, unidad.getUsuarioModificacion());
			ResultSet rs = cs.executeQuery();

			if(rs.next()){
				actualizo = rs.getInt(1);
			}
					
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return actualizo;
	}
	
}
