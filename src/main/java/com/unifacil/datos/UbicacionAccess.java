package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.UbicacionRequest;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.util.conexion.JdbcSQLServer;

@Component
public class UbicacionAccess {

	private JdbcSQLServer db;

	static final String UBI_ID = "UBI_ID";
	static final String UBI_NOMBRE = "UBI_NOMBRE";
	static final String UBI_DIRECCION = "UBI_DIRECCION";
	static final String UBI_RESPONSABLE = "UBI_RESPONSABLE";
	static final String ESTADO = "ESTADO";
	static final String FECHA_REGISTRO = "FECHA_REGISTRO";
	static final String ACTIVO = "ACTIVO";
	private static final String RESULTADO = "RESULTADO";

	private static final Logger LOG = Logger.getLogger(UbicacionAccess.class.getName());

	private UbicacionAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public List<Ubicacion> listarUbicaciones(UbicacionRequest ubicacionRequest) {
		CallableStatement cs = null;
		List<Ubicacion> listaUbicaciones = null;

		try {
			listaUbicaciones = new ArrayList<Ubicacion>();
			LOG.info(ubicacionRequest.toString());
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_UBICACION(?)}");
			cs.setInt(1, ubicacionRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Ubicacion item;

			while (rs.next()) {
				item = new Ubicacion();

				item.setId(rs.getLong(UBI_ID));
				item.setNombres(rs.getString(UBI_NOMBRE));
				LOG.info("item :"+item.getId());
				listaUbicaciones.add(item);
			}

		}catch (SQLException e) {
			LOG.warning(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		return listaUbicaciones;
	}

	public List<Ubicacion> buscarUbicacion(UbicacionRequest ubicacionRequest) {

		CallableStatement cs = null;
		List<Ubicacion> listaUbicaciones = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		try {
			listaUbicaciones = new ArrayList<Ubicacion>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_UBICACION(?,?,?,?)}");
			
			cs.setInt(1, ubicacionRequest.getIdUbicacion());
			cs.setString(2, ubicacionRequest.getDireccion());
			cs.setString(3, ubicacionRequest.getResponsable());
			cs.setInt(4, ubicacionRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Ubicacion item;

			while (rs.next()) {
				item = new Ubicacion();

				item.setId(rs.getLong(UBI_ID));
				item.setNombres(rs.getString(UBI_NOMBRE));
				item.setDireccion(rs.getString(UBI_DIRECCION));
				item.setResponsable(rs.getString(UBI_RESPONSABLE));
				item.setEstado(ACTIVO.equals(rs.getString(ESTADO)) ? true : false);

				item.setFechaCreacion(new java.sql.Date(formatter.parse(rs.getString(FECHA_REGISTRO)).getTime()));
				listaUbicaciones.add(item);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		return listaUbicaciones;

	}

	public int registrarUbicacion(Ubicacion ubicacion) {
		CallableStatement cs = null;
		int registro = 0;
		try {
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_UBICACION(?,?,?,?)}");
			cs.setString(1, ubicacion.getNombres());
			cs.setString(2, ubicacion.getDireccion());
			cs.setString(3, ubicacion.getResponsable());
			cs.setInt(4, ubicacion.getUsuarioCreador());
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				registro = Integer.parseInt(res);
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return registro;
	}

	public int actualizarUbicacion(Ubicacion ubicacion) {
		CallableStatement cs = null;
		int actualizo = 0;

		try {
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_UBICACION(?,?,?,?,?)}");
			cs.setString(1, ubicacion.getNombres());
			cs.setString(2, ubicacion.getDireccion());
			cs.setString(3, ubicacion.getResponsable());
			cs.setInt(4, ubicacion.getId().intValue());
			cs.setInt(5, ubicacion.getUsuarioModificador());
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				actualizo = Integer.parseInt(res);
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return actualizo;
	}

	public int eliminarUbicacion(Ubicacion ubicacion) {
		CallableStatement cs = null;
		int actualizo = 0;

		try {
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_UBICACION(?,?)}");
			cs.setInt(1, ubicacion.getId().intValue());
			cs.setInt(2, ubicacion.getUsuarioModificador());
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				actualizo = Integer.parseInt(res);
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return actualizo;
	}

}
