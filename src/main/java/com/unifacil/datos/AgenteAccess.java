package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.AgenteRequest;
import com.unifacil.entidad.Agente;
import com.unifacil.entidad.Sede;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.entidad.Unidad;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;

@Component
public class AgenteAccess {

	private static final String AVP_ID = "AVP_ID";
	private static final String NOMBRES = "NOMBRES";
	private static final String APELLIDOS = "APELLIDOS";
	private static final String DNI = "DNI";
	private static final String CARNET = "CARNET";
	private static final String CELULAR = "CELULAR";
	private static final String DIRECCION = "DIRECCION";
	private static final String FECHA_INI = "FECHA_INI";
	private static final String FECHA_CESE = "FECHA_CESE";
	private static final String UBICACION_ID = "UBICACION_ID";
	private static final String UBICACION = "UBICACION";
	private static final String UNIDAD_ID = "UNIDAD_ID";
	private static final String UNIDAD = "UNIDAD";
	private static final String SEDE_ID = "SEDE_ID";
	private static final String SEDE = "SEDE";
	private static final String LICENCIA="LICENCIA";
	private static final String RESULTADO = "RESULTADO";

	private JdbcSQLServer db;

	private static final Logger LOG = Logger.getLogger(AgenteAccess.class.getName());

	private AgenteAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public List<Agente> buscarAgente(AgenteRequest agenteRequest) {
		CallableStatement cs = null;
		List<Agente> listaAgentes = null;
		try {
			listaAgentes = new ArrayList<Agente>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_AVP(?,?,?,?,?,?,?,?,?,?,?,?)}");
			
			cs.setString(1, agenteRequest.getNombres());
			cs.setString(2, agenteRequest.getDni());
			cs.setString(3, agenteRequest.getCarnet());
			cs.setDate(4, agenteRequest.getFecha_Ini());
			cs.setDate(5, agenteRequest.getFecha_Hasta());
			cs.setDate(6, agenteRequest.getFecha_ces_Ini());
			cs.setDate(7, agenteRequest.getFecha_ces_Hasta());
			cs.setString(8, agenteRequest.getUbicacion());
			cs.setString(9, agenteRequest.getUnidad());
			cs.setString(10, agenteRequest.getSedes());
			cs.setInt(11, agenteRequest.getId());
			cs.setInt(12, agenteRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Agente item;
			while (rs.next()) {
				item = new Agente();

				item.setId(rs.getLong(AVP_ID));
				item.setNombres(rs.getString(NOMBRES));
				item.setApellidos(rs.getString(APELLIDOS));
				item.setDni(rs.getString(DNI));
				item.setCarnet(rs.getString(CARNET));
				item.setCelular(rs.getString(CELULAR));
				item.setDireccion(rs.getString(DIRECCION));
				item.setFechaInit(rs.getString(FECHA_INI));
				item.setFechaCese(rs.getString(FECHA_CESE));
				item.setLicencia(rs.getString(LICENCIA));

				item.listaUbicaciones = new ArrayList<Ubicacion>();
				Ubicacion ubicacion;
				String[] ubicaciones = rs.getString(UBICACION).split(",");
				String[] idUbicaciones = rs.getString(UBICACION_ID).split(",");
				for (int i = 0; i < ubicaciones.length; i++) {
					ubicacion = new Ubicacion();
					ubicacion.setNombres(ubicaciones[i]);
					ubicacion.setId(Long.parseLong(idUbicaciones[i]));
					item.listaUbicaciones.add(ubicacion);
				}

				item.listaUnidades = new ArrayList<Unidad>();
				Unidad unidad;
				String[] unidades = rs.getString(UNIDAD).split(",");
				String[] idUnidades = rs.getString(UNIDAD_ID).split(",");
				for (int i = 0; i < unidades.length; i++) {
					unidad = new Unidad();
					unidad.setNombreRazon(unidades[i]);
					unidad.setId(Long.parseLong(idUnidades[i]));
					item.listaUnidades.add(unidad);
				}

				item.listaSedes = new ArrayList<Sede>();
				Sede sede;
				String[] sedes = rs.getString(SEDE).split(",");
				String[] idSedes = rs.getString(SEDE_ID).split(",");
				for (int i = 0; i < sedes.length; i++) {
					sede = new Sede();
					sede.setNombre(sedes[i]);
					sede.setId(Long.parseLong(idSedes[i]));
					item.listaSedes.add(sede);
				}

				listaAgentes.add(item);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaAgentes;
	}

	public boolean registrarAgente(AgenteRequest agenteRequest) {
		CallableStatement cs = null;
		boolean respuesta = false;
		try {

			LOG.info("registrar Agente datos: " + agenteRequest.toString());
			LOG.info("Nombres : " + agenteRequest.getNombres());
			LOG.info("Apellidos: " + agenteRequest.getApellidos());
			LOG.info("DNI: " + agenteRequest.getDni());
			LOG.info("Licencia: " + agenteRequest.getLicencia());
			LOG.info("Carnet: " + agenteRequest.getCarnet());
			LOG.info("Fecha_Ini: " + agenteRequest.getFecha_Ini());
			LOG.info("Celular: " + agenteRequest.getCelular());
			LOG.info("Direccion: " + agenteRequest.getDireccion());
			LOG.info("Sedes: " + agenteRequest.getSedes());
			LOG.info("IdUsuarioCreador: " + agenteRequest.getIdUsuarioCreador());
				
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_AVP(?,?,?,?,?,?,?,?,?,?)}");

			cs.setString(1, agenteRequest.getNombres());
			cs.setString(2, agenteRequest.getApellidos());
			cs.setString(3, agenteRequest.getDni());
			cs.setString(4, agenteRequest.getLicencia());
			cs.setString(5, agenteRequest.getCarnet());
			cs.setDate(6, agenteRequest.getFecha_Ini());
			cs.setString(7, agenteRequest.getCelular());
			cs.setString(8, agenteRequest.getDireccion());
			cs.setString(9, agenteRequest.getSedes());
			cs.setInt(10, agenteRequest.getIdUsuarioModificador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean modificarAgente(AgenteRequest agenteRequest) {
		CallableStatement cs = null;
		boolean respuesta =false;
		try {
			db.openConnection();

			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_AVP(?,?,?,?,?,?,?,?,?,?,?,?)}");
			
			LOG.info("Nombres: "+agenteRequest.getNombres());
			LOG.info("Apellidos: "+agenteRequest.getApellidos());
			LOG.info("dni: "+agenteRequest.getDni());
			LOG.info("Licencia: "+agenteRequest.getLicencia());
			LOG.info("Carnet: "+agenteRequest.getCarnet());
			LOG.info("Fecha Ini: "+agenteRequest.getFecha_Ini());
			LOG.info("Fecha ces ini: "+agenteRequest.getFecha_ces_Ini());
			LOG.info("Celular: "+agenteRequest.getCelular());
			LOG.info("Direccion: "+agenteRequest.getDireccion());
			LOG.info("getId: "+agenteRequest.getId());
			LOG.info("getIdUsuarioModificador: "+agenteRequest.getIdUsuarioModificador());

			cs.setString(1, agenteRequest.getNombres());
			cs.setString(2, agenteRequest.getApellidos());
			cs.setString(3, agenteRequest.getDni());
			cs.setString(4, agenteRequest.getLicencia());
			cs.setString(5, agenteRequest.getCarnet());
			cs.setDate(6, agenteRequest.getFecha_Ini());
			cs.setDate(7, agenteRequest.getFecha_ces_Ini());
			cs.setString(8, agenteRequest.getCelular());
			cs.setString(9, agenteRequest.getDireccion());
			cs.setString(10, agenteRequest.getSedes());
			cs.setInt(11, agenteRequest.getId());
			cs.setInt(12, agenteRequest.getIdUsuarioModificador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("respuesta:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean eliminarAgente(AgenteRequest agenteRequest) {
		CallableStatement cs = null;
		boolean respuesta = false;
		try {
			db.openConnection();

			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_AVP(?,?)}");

			cs.setInt(1, agenteRequest.getId());
			cs.setInt(2, agenteRequest.getIdUsuarioModificador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return respuesta;
	}
}
