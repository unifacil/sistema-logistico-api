package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.SubcategoriaRequest;
import com.unifacil.entidad.Subcategoria;
import com.unifacil.util.conexion.JdbcSQLServer;

@Component
public class SubcategoriaAccess {

	
	private static final String SUB_ID = "SUB_ID";
	private static final String SUB_NOMBRE = "SUB_NOMBRE";
	private JdbcSQLServer db;
	private static final Logger LOG = Logger.getLogger(SubcategoriaAccess.class.getName());

	private SubcategoriaAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public List<Subcategoria> listarSubcategoria(SubcategoriaRequest request) {
		CallableStatement cs = null;
		List<Subcategoria> listaSubcategorias = null;
		try {
			listaSubcategorias = new ArrayList<Subcategoria>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_SUBCATEGORIA(?)}");
			cs.setInt(1, request.getIdCategoria());
			ResultSet rs = cs.executeQuery();

			Subcategoria subcategoria;
			while (rs.next()) {
				subcategoria = new Subcategoria();

				subcategoria.setId(rs.getLong(SUB_ID));
				subcategoria.setNombre(rs.getString(SUB_NOMBRE));

				listaSubcategorias.add(subcategoria);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaSubcategorias;
	}
}
