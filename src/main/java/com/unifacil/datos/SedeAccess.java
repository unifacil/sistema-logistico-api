package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.SedeRequest;
import com.unifacil.entidad.Sede;
import com.unifacil.entidad.Unidad;
import com.unifacil.util.conexion.JdbcSQLServer;
import java.util.logging.Logger;

@Component
public class SedeAccess {

	private static final String SED_ID = "SED_ID";
	private static final String SED_NOMBRE = "SED_NOMBRE";
	private static final String SED_DIRECCION = "SED_DIRECCION";
	private static final String ESTADO = "ESTADO";
	private static final String FECHA_REGISTRO = "FECHA_REGISTRO";
	private static final String UBI_ID = "UBI_ID";
	private static final String UBI_NOMBRE = "UBI_NOMBRE";
	private static final String UNI_ID = "UNI_ID";
	private static final String UNI_NOMBRE_RAZON = "UNI_NOMBRE_RAZON";
	private static final String UNI_ABREVIATURA = "UNI_ABREVIATURA";
	static final String ACTIVO = "ACTIVO";
	private static final Logger LOG = Logger.getLogger(SedeAccess.class.getName());

	private JdbcSQLServer db;

	private SedeAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public List<Sede> listarSedes(List<Unidad> unidadades) {
		CallableStatement cs = null;
		List<Sede> listaSedes = null;

		try {
			listaSedes = new ArrayList<Sede>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_SEDE(?)}");
			cs.setString(1,
					unidadades.stream().map(x -> x.getId()).map(x -> x.toString()).collect(Collectors.joining(",")));
			ResultSet rs = cs.executeQuery();

			Sede item;

			while (rs.next()) {
				item = new Sede();

				item.setId(rs.getLong("SED_ID"));
				item.setNombre(rs.getString("SED_NOMBRE"));

				listaSedes.add(item);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaSedes;
	}

	public List<Sede> buscarSede(SedeRequest sedeRequest) {
		CallableStatement cs = null;
		List<Sede> listaSedes = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		try {
			listaSedes = new ArrayList<Sede>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_SEDE(?,?,?,?,?)}");
			
			cs.setInt(1, sedeRequest.getIdUbicacion());
			cs.setInt(2, sedeRequest.getIdUnidad());
			cs.setInt(3, sedeRequest.getId());
			cs.setString(4, sedeRequest.getNombre());
			cs.setInt(5, sedeRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Sede item;

			while (rs.next()) {
				item = new Sede();

				item.getUbicacion().setNombres(rs.getString(UBI_NOMBRE));
				item.getUnidad().setNombrerazon(rs.getString(UNI_NOMBRE_RAZON));
				item.getUnidad().setAbreviatura(rs.getString(UNI_ABREVIATURA));
				item.setNombre(rs.getString(SED_NOMBRE));
				item.setDireccion(rs.getString(SED_DIRECCION));
				item.setEstado(ACTIVO.equals(rs.getString(ESTADO)) ? true : false);
				item.setFechaRegistro(new java.sql.Date(formatter.parse(rs.getString(FECHA_REGISTRO)).getTime()));
				item.getUbicacion().setId(new Long(rs.getInt(UBI_ID)));
				item.getUnidad().setId(new Long(rs.getInt(UNI_ID)));
				item.setId(Long.parseLong(rs.getString(SED_ID)));

				listaSedes.add(item);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaSedes;
	}

	public int registrarSede(SedeRequest sedeRequest) {

		CallableStatement cs = null;
		int registro = 0;

		try {

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_SEDE(?,?,?,?)}");
			cs.setInt(1, sedeRequest.getIdUnidad());
			cs.setString(2, sedeRequest.getNombre());
			cs.setString(3, sedeRequest.getDireccion());
			cs.setInt(4, sedeRequest.getIdUsuarioCreador());
			ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				registro = rs.getInt(1);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return registro;
	}

	public int modificarSede(SedeRequest sedeRequest) {

		CallableStatement cs = null;
		int actualizo = 0;

		try {

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_SEDE(?,?,?,?,?)}");
			cs.setInt(1, sedeRequest.getIdUnidad());
			cs.setString(2, sedeRequest.getNombre());
			cs.setString(3, sedeRequest.getDireccion());
			cs.setInt(4, sedeRequest.getId());
			cs.setInt(5, sedeRequest.getIdUsuarioModificador());
			ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				actualizo = rs.getInt(1);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return actualizo;
	}

	public int eliminarSede(SedeRequest sedeRequest) {

		CallableStatement cs = null;
		int actualizo = 0;

		try {

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_SEDE(?,?)}");
			cs.setInt(1, sedeRequest.getId());
			cs.setInt(2, sedeRequest.getIdUsuarioModificador());
			ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				actualizo = rs.getInt(1);
			}

		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return actualizo;
	}
}
