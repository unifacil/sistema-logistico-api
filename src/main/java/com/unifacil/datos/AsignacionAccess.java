package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.AsignacionRequest;
import com.unifacil.entidad.Asignacion;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;

@Component
public class AsignacionAccess {
	
	private static final Logger LOG = Logger.getLogger(AsignacionAccess.class.getName());
	private JdbcSQLServer db;
	
	private static final String CAT_NOMBRE = "CAT_NOMBRE";
	private static final String SUB_NOMBRE = "SUB_NOMBRE";
	private static final String TPR_NOMBRE = "TPR_NOMBRE";
	private static final String INV_MARCA = "INV_MARCA";
	private static final String INV_MODELO = "INV_MODELO";
	private static final String INV_SERIE = "INV_SERIE";
	private static final String INV_RESOLUCION = "INV_RESOLUCION";
	private static final String INV_PLACA = "INV_PLACA";
	private static final String INV_HDD = "INV_HDD";
	private static final String INV_RAM = "INV_RAM";
	private static final String INV_SO = "INV_SO";
	private static final String INV_VOLTAJE = "INV_VOLTAJE";
	private static final String INV_CODIGO = "INV_CODIGO";
	private static final String INV_COLOR = "INV_COLOR";
	private static final String INV_TARJ_PROP = "INV_TARJ_PROP";
	private static final String INV_FOTO = "INV_FOTO";
	private static final String ASI_CANT = "ASI_CANT";
	private static final String ASIGNADO_A = "ASIGNADO A";
	private static final String ASI_FECHA_ASIGNA = "ASI_FECHA_ASIGNA";
	private static final String INV_ID = "INV_ID";
	private static final String INV_NOMBRE = "INV_NOMBRE";
	
	private static final String RESULTADO = "RESULTADO";
	
	
	private AsignacionAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public List<Asignacion> buscarAsignacion(AsignacionRequest request){
		CallableStatement cs = null;
		List<Asignacion> listaAsignaciones = null;
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			db.openConnection();
			
			listaAsignaciones = new ArrayList<Asignacion>();
			
			cs = db.getConnection().prepareCall("{call USP_BUSCAR_ASIGNACION(?,?,?,?,?,?)}");
			cs.setInt(1, request.getIdProducto());
			cs.setString(2, request.getMarca());
			cs.setString(3, request.getModelo());
			cs.setString(4, request.getSerie());
			cs.setString(5, request.getPlaca());
			cs.setString(6, request.getCodigo());
			
			ResultSet rs = cs.executeQuery();
			
			Asignacion item;
			
			while(rs.next()){
				item = new Asignacion();
				item.getInventario().getTipoProducto().getSubcategoria().getCategoria().setNombre(rs.getString(CAT_NOMBRE));
				item.getInventario().getTipoProducto().getSubcategoria().setNombre(rs.getString(SUB_NOMBRE));
				item.getInventario().getTipoProducto().setNombre(rs.getString(TPR_NOMBRE));
				item.getInventario().setMarca(rs.getString(INV_MARCA));
				item.getInventario().setModelo(rs.getString(INV_MODELO));
				item.getInventario().setSerie(rs.getString(INV_SERIE));
				item.getInventario().setResolucion(rs.getString(INV_RESOLUCION));
				item.getInventario().setPlaca(rs.getString(INV_PLACA));
				item.getInventario().setHdd(rs.getString(INV_HDD));
				item.getInventario().setRam(rs.getString(INV_RAM));
				item.getInventario().setSo(rs.getString(INV_SO));
				item.getInventario().setVoltaje(rs.getString(INV_VOLTAJE));
				item.getInventario().setCodigo(rs.getString(INV_CODIGO));
				item.getInventario().setColor(rs.getString(INV_COLOR));
				item.getInventario().setTarj_prop(rs.getString(INV_TARJ_PROP));
				item.getInventario().setFoto(rs.getString(INV_FOTO));
				item.setCantidad(rs.getInt(ASI_CANT));
				item.getAgente().setNombres(rs.getString(ASIGNADO_A));
				item.setFechaAsignacion(rs.getString(ASI_FECHA_ASIGNA));
				item.getInventario().setId(rs.getLong(INV_ID));
				item.getInventario().setNombre(rs.getString(INV_NOMBRE));
				listaAsignaciones.add(item);
			}
			
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return listaAsignaciones;
	}
	
	public boolean registrarAsignacion(Asignacion asignacion){
		CallableStatement cs = null;
		boolean respuesta =false;
		try {
			
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			
			
			LOG.info("asignacion : "+asignacion.toString());
			LOG.info("id inventario:"+ asignacion.getInventario().getId().intValue());
			if(asignacion.getListaAgentes()!=null){
				LOG.info("Lista de agentes:"+asignacion.getListaAgentes().stream().map(x -> x.getId())
						.map(x -> x.toString()).collect(Collectors.joining(",")));
			}else{
				LOG.info("lista agentes : null");
			}
			
			if(asignacion.getListaUnidades()!=null){
				LOG.info("Lista de unidades:"+asignacion.getListaUnidades().stream().map(x -> x.getId())
						.map(x -> x.toString()).collect(Collectors.joining(",")));
			}else{
				LOG.info("lista unidades : null");
			}
			
			
			if(asignacion.getListaSedes()!=null){
				LOG.info("Lista de sedes:"+asignacion.getListaSedes().stream().map(x -> x.getId())
						.map(x -> x.toString()).collect(Collectors.joining(",")));
			}else{
				LOG.info("lista sedes : null");
			}
			
			LOG.info("cantidad:"+asignacion.getCantidad());
			LOG.info("Fecha de asignacion"+asignacion.getFechaAsignacion());
			LOG.info("Fecha id usuario registro"+asignacion.getIdUsuarioRegistro());
			
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_ASIGNACION(?,?,?,?,?,?,?)}");
			cs.setInt(1, asignacion.getInventario().getId().intValue());
			cs.setString(2, asignacion.getListaAgentes() != null? asignacion.getListaAgentes().stream().map(x -> x.getId())
						.map(x -> x.toString()).collect(Collectors.joining(","))		: Constantes.VACIO);
			cs.setString(3, asignacion.getListaUnidades() != null? asignacion.getListaUnidades().stream().map(x -> x.getId())
					.map(x -> x.toString()).collect(Collectors.joining(",")) : Constantes.VACIO);
			cs.setString(4, asignacion.getListaSedes() != null? asignacion.getListaSedes().stream().map(x -> x.getId())
					.map(x -> x.toString()).collect(Collectors.joining(",")) : Constantes.VACIO);
			
			cs.setInt(5, asignacion.getCantidad());
			cs.setString(6, asignacion.getFechaAsignacion());
			cs.setInt(7, asignacion.getIdUsuarioRegistro());
			
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (Exception e) {
			LOG.warning(e.getMessage());
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				LOG.warning(se2.getMessage());
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				LOG.warning(se.getMessage());
			}
		}
		return respuesta;
	}
	
}
