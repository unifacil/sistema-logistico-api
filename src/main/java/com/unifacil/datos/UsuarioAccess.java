package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.UsuarioRequest;
import com.unifacil.controlador.logistica.UsuarioControlador;
import com.unifacil.entidad.Sede;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.entidad.Unidad;
import com.unifacil.entidad.Usuario;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;

@Component
public class UsuarioAccess {

	static final String NOMBRES = "NOMBRES";
	static final String APELLIDOS = "APELLIDOS";;
	static final String NOMBRE_COMPLETO = "NOMBRE_COMPLETO";
	static final String PERFIL = "PERFIL";
	static final String USUARIO = "USUARIO";
	static final String UBICACION = "UBICACION";
	static final String UBICACION_ID = "UBICACION_ID";
	static final String UNIDAD = "UNIDAD";
	static final String UNIDAD_ID = "UNIDAD_ID";
	static final String SEDE = "SEDE";
	static final String SEDE_ID = "SEDE_ID";
	static final String CORREO = "CORREO";
	static final String USU_ID = "USU_ID";
	static final String PERF_ID = "PERF_ID";
	static final String ESTADO = "ESTADO";
	static final String NEWPSW_USUARIO = "NEWPSW_USUARIO";
	private static final String RESULTADO = "RESULTADO";
	private JdbcSQLServer db;

	private static final Logger LOG = Logger.getLogger(UsuarioAccess.class.getName());
	
	private UsuarioAccess() {
		LOG.info("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public Usuario validarUsuario(String username, String password) {
		CallableStatement cs = null;
		Usuario usuario = new Usuario();
		try {

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_VALIDAR_LOGIN(?,?)}");
			cs.setString(1, username);
			cs.setString(2, password);

			ResultSet rs = cs.executeQuery();

			if (rs.next()) {
				Long id = rs.getLong("USUARIO_ID");
				Long perfilId = rs.getLong("PERFIL_ID");
				String user = rs.getString("USU_NOMBRE");

				usuario.setId(id.longValue());
				usuario.setPerfilId(perfilId);
				usuario.setNombreCompleto(user);

			} else {
				throw new Exception("NO DEVOLVIO NADA");
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return usuario;
	}

	public List<Usuario> buscarUsuario(UsuarioRequest usuarioRequest) {
		List<Usuario> listaUsuarios = null;
		CallableStatement cs = null;
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			db.openConnection();

			listaUsuarios = new ArrayList<Usuario>();

			cs = db.getConnection().prepareCall("{call USP_BUSCAR_USUARIO(?,?,?,?,?,?,?,?)}");
			
			cs.setInt(1, usuarioRequest.getIdPerfil());
			cs.setString(2, usuarioRequest.getNombre());
			cs.setString(3, usuarioRequest.getUsuario());
			cs.setInt(4, usuarioRequest.getIdUbicacion());
			cs.setInt(5, usuarioRequest.getIdUnidad());
			cs.setInt(6, usuarioRequest.getIdSede());
			cs.setString(7, usuarioRequest.getCorreo());
			cs.setInt(8, usuarioRequest.getUsuario_perfil());
			ResultSet rs = cs.executeQuery();

			Usuario item;
			while (rs.next()) {
				item = new Usuario();

				item.setNombres(rs.getString(NOMBRES));
				item.setApellidos(rs.getString(APELLIDOS));
				item.setNombreCompleto(rs.getString(NOMBRE_COMPLETO));
				item.setPerfilNombre(rs.getString(PERFIL));
				item.setUsername(rs.getString(USUARIO));

				item.listaUbicaciones = new ArrayList<Ubicacion>();
				Ubicacion ubicacion;
				String cadenaUbicaciones = rs.getString(UBICACION);
				String[] ubicaciones = cadenaUbicaciones!=null?cadenaUbicaciones.split(","):new String[0];
				String cadenaIdUbicaciones = rs.getString(UBICACION_ID);
				String[] idUbicaciones = cadenaIdUbicaciones!=null?cadenaIdUbicaciones.split(","):new String[0];
				for (int i = 0; i < ubicaciones.length; i++) {
					ubicacion = new Ubicacion();
					ubicacion.setNombres(ubicaciones[i]);
					ubicacion.setId(Long.parseLong(idUbicaciones[i]));
					item.listaUbicaciones.add(ubicacion);
				}

				item.listaUnidades = new ArrayList<Unidad>();
				Unidad unidad;
				String cadenaUnidades= rs.getString(UNIDAD);
				String[] unidades = cadenaUnidades!=null?cadenaUnidades.split(","):new String[0];
				String cadenaIdUnidades = rs.getString(UNIDAD_ID);
				String[] idUnidades = cadenaIdUnidades!=null?cadenaIdUnidades.split(","):new String[0];
				for (int i = 0; i < unidades.length; i++) {
					unidad = new Unidad();
					unidad.setNombreRazon(unidades[i]);
					unidad.setId(Long.parseLong(idUnidades[i]));
					item.listaUnidades.add(unidad);
				}

				item.listaSedes = new ArrayList<Sede>();
				Sede sede;
				String cadenaSede= rs.getString(SEDE);
				String[] sedes = cadenaSede!=null?cadenaSede.split(","):new String[0];
				String cadenaIdSede = rs.getString(SEDE_ID);
				String[] idSedes = cadenaIdSede!=null?cadenaIdSede.split(","):new String[0];
				for (int i = 0; i < sedes.length; i++) {
					sede = new Sede();
					sede.setNombre(sedes[i]);
					sede.setId(Long.parseLong(idSedes[i]));
					item.listaSedes.add(sede);
				}

				item.setCorreo(rs.getString(CORREO));
				item.setId(rs.getLong(USU_ID));
				item.setPerfilId(rs.getLong(PERF_ID));
				item.setEstado(rs.getString(ESTADO));

				listaUsuarios.add(item);
			}
			if (listaUsuarios != null) {
				LOG.info("usuario encontrados:" + listaUsuarios.toString());
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaUsuarios;
	}

	public boolean registrarUsuario(Usuario usuario) {
		CallableStatement cs = null;
		boolean respuesta =false;
		try {
			db.openConnection();

			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_USUARIO(?,?,?,?,?,?,?,?)}");
			cs.setLong(1, usuario.getPerfilId());
			cs.setString(2, usuario.getNombres());
			cs.setString(3, usuario.getApellidos());
			cs.setString(4, usuario.getCorreo());
			cs.setString(5, usuario.getUsername());
			cs.setString(6, usuario.getPassword());
			cs.setString(7, usuario.getListaSedes().stream().map(x -> x.getId()).map(x -> x.toString())
					.collect(Collectors.joining(",")));
			cs.setInt(8, usuario.getUsuarioCreador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean actualizarUsuario(Usuario usuario) {

		CallableStatement cs = null;
		boolean respuesta =false;
		try {

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_USUARIO(?,?,?,?,?,?,?,?,?)}");
			cs.setLong(1, usuario.getPerfilId());
			cs.setString(2, usuario.getNombres());
			cs.setString(3, usuario.getApellidos());
			cs.setString(4, usuario.getCorreo());
			cs.setString(5, usuario.getUsername());
			cs.setString(6, usuario.getListaSedes().stream().map(x -> x.getId()).map(x -> x.toString())
					.collect(Collectors.joining(",")));
			cs.setInt(7, usuario.getId().intValue());
			cs.setBoolean(8, Constantes.CERO.equals(usuario.getEstado()) ? false : true);
			cs.setInt(9, usuario.getUsuarioModificador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (SQLException e2) {
			e2.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean eliminarUsuario(UsuarioRequest usuario) {
		CallableStatement cs = null;
		boolean respuesta = false;
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_USUARIO(?,?)}");
			cs.setInt(1, usuario.getIdUsuario());
			cs.setInt(2, usuario.getIdUsuarioModificador());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (SQLException e2) {
			e2.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public String recuperarPassword(UsuarioRequest usuario) {

		String clave = Constantes.VACIO;
		CallableStatement cs = null;
		try {

			LOG.info("********correo" + usuario.getCorreo());
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_RECUPERAR_PSW(?)}");
			cs.setString(1, usuario.getCorreo());

			ResultSet rs = cs.executeQuery();
			System.out.println(rs);
			if (rs.next()) {
				LOG.info("obtener clave");
				clave = rs.getString(NEWPSW_USUARIO);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		LOG.info(clave);
		return clave;
	}

	public boolean cambiarPassword(UsuarioRequest usuarioRequest) {
		CallableStatement cs = null;
		boolean respuesta = false;
		try {
			if (db == null) {
				LOG.warning("objeto conecction es null");
			}
			LOG.info(usuarioRequest.getUsuario());
			LOG.info(usuarioRequest.getNuevoPassword());
			LOG.info(usuarioRequest.getNuevoPassword());
			
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_CAMBIAR_PSW(?,?,?,?)}");
			cs.setString(1, usuarioRequest.getUsuario());
			cs.setString(2, usuarioRequest.getNuevoPassword());
			cs.setString(3, usuarioRequest.getNuevoPassword());
			cs.setInt(4, usuarioRequest.getIdUsuario());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

}
