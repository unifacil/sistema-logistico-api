package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.unifacil.entidad.Perfil;
import com.unifacil.util.conexion.JdbcSQLServer;

@Component
public class PerfilAccess {

	static final String PERF_NOMBRE = "PERF_NOMBRE";
	static final String PERF_ID = "PERF_ID";
	private JdbcSQLServer db;
	
	private PerfilAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}
	
	public List<Perfil> listarPerfiles(){
		List<Perfil> listaPerfiles = null;
		CallableStatement cs = null;
		
		try {
			if (db == null) {
				System.out.println("objeto conecction es null");
			}
			db.openConnection();
			
			listaPerfiles = new ArrayList<Perfil>();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_PERFIL()}");
			
			ResultSet rs = cs.executeQuery();
			
			Perfil item;
			while(rs.next()){
				item = new Perfil();
				item.setId(rs.getLong(PERF_ID));
				item.setNombre(rs.getString(PERF_NOMBRE));
				listaPerfiles.add(item);
			}
			if (listaPerfiles != null) {
				System.out.println("perfiles obtenidos:" + listaPerfiles.toString());
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaPerfiles;
	}
	
}
