package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.ProveedorRequest;
import com.unifacil.entidad.Proveedor;
import com.unifacil.util.conexion.JdbcSQLServer;
import com.unifacil.util.constantes.Constantes;


@Component
public class ProveedorAccess {

	private static final String CATEGORIA = "CATEGORIA";
	private static final String RAZON_SOCIAL = "RAZON_SOCIAL";
	
	private static final String RUC = "RUC";
	private static final String TELEFONO = "TELEFONO";
	private static final String CORREO = "CORREO";
	private static final String DIRECCION = "DIRECCION";
	private static final String CONTACTO = "CONTACTO";
	private static final String PRO_ID = "PRO_ID";
	private static final String CAT_ID = "CAT_ID";
	private static final String RESULTADO = "RESULTADO";
	private JdbcSQLServer db;
	private static final Logger LOG = Logger.getLogger(ProveedorAccess.class.getName());

	private ProveedorAccess() {
		this.db = JdbcSQLServer.getDb();
	}

	public List<Proveedor> buscarProveedor(ProveedorRequest proveedorRequest) {
		CallableStatement cs = null;
		List<Proveedor> listaProveedor = new ArrayList<>();
		Proveedor proveedor;
		try {

			db.openConnection();

			cs = db.getConnection().prepareCall("{call USP_BUSCAR_PROVEEDOR(?,?,?,?,?,?,?)}");
			cs.setInt(1, proveedorRequest.getCategoria());
			cs.setString(2, proveedorRequest.getRazonSocial());
			cs.setString(3, proveedorRequest.getRuc());
			cs.setString(4, proveedorRequest.getTelefono());
			cs.setString(5, proveedorRequest.getCorreo());
			cs.setString(6, proveedorRequest.getContacto());
			cs.setInt(7, proveedorRequest.getId());

			ResultSet rs = cs.executeQuery();

			while (rs.next()) {

				proveedor = new Proveedor();
				proveedor.setCategoria(rs.getString(CATEGORIA));
				proveedor.setCategoriaId(rs.getInt(CAT_ID));
				proveedor.setContacto(rs.getString(CONTACTO));
				proveedor.setCorreo(rs.getString(CORREO));
				proveedor.setDireccion(rs.getString(DIRECCION));
				proveedor.setId(rs.getInt(PRO_ID));
				proveedor.setRazonSocial(rs.getString(RAZON_SOCIAL));
				proveedor.setRuc(rs.getString(RUC));
				proveedor.setTelefono(rs.getString(TELEFONO));
				listaProveedor.add(proveedor);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaProveedor;
	}

	public boolean registrarProveedor(ProveedorRequest proveedorRequest) {
		boolean respuesta = false;
		CallableStatement cs = null;
		try {

			db.openConnection();

			cs = db.getConnection().prepareCall("{call USP_REGISTRAR_PROVEEDOR(?,?,?,?,?,?,?,?)}");
			cs.setInt(1, proveedorRequest.getCategoria());
			cs.setString(2, proveedorRequest.getRazonSocial());
			cs.setString(3, proveedorRequest.getRuc());
			cs.setString(4, proveedorRequest.getDireccion());
			cs.setString(5, proveedorRequest.getTelefono());
			cs.setString(6, proveedorRequest.getCorreo());
			cs.setString(7, proveedorRequest.getContacto());
			cs.setInt(8, proveedorRequest.getUsuarioRegistro());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean modificarProveedor(ProveedorRequest proveedorRequest) {
		boolean respuesta = false;
		CallableStatement cs = null;
		try {
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ACTUALIZAR_PROVEEDOR(?,?,?,?,?,?,?,?,?)}");
			cs.setInt(1, proveedorRequest.getCategoria());
			cs.setString(2, proveedorRequest.getRazonSocial());
			cs.setString(3, proveedorRequest.getRuc());
			cs.setString(4, proveedorRequest.getDireccion());
			cs.setString(5, proveedorRequest.getTelefono());
			cs.setString(6, proveedorRequest.getCorreo());
			cs.setString(7, proveedorRequest.getContacto());
			cs.setInt(8, proveedorRequest.getId());
			cs.setInt(9, proveedorRequest.getUsuarioRegistro());

			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

	public boolean eliminarProveedor(ProveedorRequest proveedorRequest) {
		boolean respuesta = false;
		CallableStatement cs = null;
		try {
			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_ELIMINAR_PROVEEDOR(?,?)}");
			cs.setInt(1, proveedorRequest.getId());
			cs.setInt(2, proveedorRequest.getUsuarioRegistro());
			ResultSet result = cs.executeQuery();
			LOG.info("result:"+result.toString());
			if(result.next()) {
				String res = result.getString(RESULTADO);
				LOG.info("rs:"+res);
				respuesta = Constantes.UNO.equals(res) ? true : false;
			}else {
				LOG.info("NO EXISTEN REGISTROS");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return respuesta;
	}

}
