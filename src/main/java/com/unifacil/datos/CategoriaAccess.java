package com.unifacil.datos;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.unifacil.bean.request.CategoriaRequest;
import com.unifacil.entidad.Categoria;
import com.unifacil.util.conexion.JdbcSQLServer;

@Component
public class CategoriaAccess {

	private static final String CAT_ID = "CAT_ID";
	private static final String CAT_NOMBRE = "CAT_NOMBRE";

	private JdbcSQLServer db;

	private CategoriaAccess() {
		System.out.println("cargando conecction");
		this.db = JdbcSQLServer.getDb();
	}

	public List<Categoria> listarCategoria() {
		CallableStatement cs = null;
		List<Categoria> listaCategorias = null;
		try {
			listaCategorias = new ArrayList<Categoria>();

			db.openConnection();
			cs = db.getConnection().prepareCall("{call USP_LISTAR_CATEGORIA()}");
			ResultSet rs = cs.executeQuery();

			Categoria categoria;
			while (rs.next()) {
				categoria = new Categoria();

				categoria.setId(rs.getLong(CAT_ID));
				categoria.setDescripcion(rs.getString(CAT_NOMBRE));

				listaCategorias.add(categoria);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (cs != null)
					cs.close();
			} catch (SQLException se2) {
				se2.printStackTrace();
			}
			try {
				if (db.getConnection() != null)
					db.closeConnection();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		return listaCategorias;
	}

	
}
