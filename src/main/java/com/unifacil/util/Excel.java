package com.unifacil.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.unifacil.bean.ReporteBean;
import com.unifacil.util.constantes.Constantes;

@Component
public class Excel {
	private static final String FILE_NAME_RAIZ = "C:\\TEMP\\";
	private static final String FILE_NAME_REPORTE_ACTUAL = "ReporteActual.xlsx";
	private static final String FILE_NAME_REPORTE_INGRESO = "ReporteIngreso.xlsx";
	private static final String FILE_NAME_REPORTE_SALIDA = "ReporteSalida.xlsx";

	private static final String INV_ID = "INV_ID";
	private static final String INV_NOMBRE = "INV_NOMBRE";
	private static final String INV_MARCA = "INV_MARCA";
	private static final String INV_MODELO = "INV_MODELO";
	private static final String INV_SERIE = "INV_SERIE";
	private static final String INV_RESOLUCION = "INV_RESOLUCION";
	private static final String INV_PLACA = "INV_PLACA";
	private static final String INV_HDD = "INV_HDD";
	private static final String INV_RAM = "INV_RAM";
	private static final String INV_SO = "INV_SO";
	private static final String INV_VOLTAJE = "INV_VOLTAJE";
	private static final String INV_CODIGO = "INV_CODIGO";
	private static final String INV_COLOR = "INV_COLOR";
	private static final String INV_TALLA = "INV_TALLA";
	private static final String INV_TARJ_PROP = "INV_TARJ_PROP";
	private static final String INV_FOTO = "INV_FOTO";
	private static final String ESTADO = "ESTADO";
	private static final String INV_USUARIO_REGIS = "INV_USUARIO_REGIS";
	private static final String INV_FEC_REGIS = "INV_FEC_REGIS";
	private static final String INV_USUARIO_MODI = "INV_USUARIO_MODI";
	private static final String INV_FEC_MODI = "INV_FEC_MODI";
	private static final String IND_CANTIDAD = "IND_CANTIDAD";
	private static final String IND_FECHA_COMPRA = "IND_FECHA_COMPRA";
	private static final String IND_USUARIO_REGIS = "IND_USUARIO_REGIS";
	private static final String IND_FEC_REGIS = "IND_FEC_REGIS";
	private static final String IND_USUARIO_MODI = "IND_USUARIO_MODI";
	private static final String IND_FEC_MODI = "IND_FEC_MODI";

	private static final String AVP_ID = "AVP_ID";
	private static final String UNI_ID = "UNI_ID";
	private static final String SEDE_ID = "SEDE_ID";
	private static final String ASI_CANT = "ASI_CANT";
	private static final String ASI_FECHA_ASIGNA = "ASI_FECHA_ASIGNA";

	private static final String SUBCATEGORIA = "SUBCATEGORIA";
	private static final String TIPO = "TIPO";
	private static final String PRO_RAZON_SOCIAL = "PRO_RAZON_SOCIAL";
	private static final String UBI_NOMBRE = "UBI_NOMBRE";
	private static final String INV_STOCK = "INV_STOCK";

	private XSSFFont headerFont;
	private XSSFCellStyle headerStyle;
	
	private static final Logger LOG = Logger.getLogger(Excel.class.getName());

	public String obtenerReporteActual(List<ReporteBean> listaReporte) {

		List<List<String>> listaData = new ArrayList<List<String>>();

		List<String> titulo = Arrays.asList(Constantes.VACIO, Constantes.VACIO, Constantes.VACIO, Constantes.VACIO,
				"REPORTE DE STOCK ACTUAL");

		listaData.add(titulo);

		List<String> espacio = Arrays.asList("");
		listaData.add(espacio);

		List<String> encabezado = Arrays.asList(SUBCATEGORIA, TIPO, INV_NOMBRE, INV_MARCA, INV_MODELO, INV_SERIE,
				PRO_RAZON_SOCIAL, UBI_NOMBRE, INV_RESOLUCION, INV_PLACA, INV_HDD, INV_RAM, INV_SO, INV_VOLTAJE,
				INV_CODIGO, INV_COLOR, INV_TARJ_PROP, INV_FOTO, INV_TALLA, INV_STOCK);
		listaData.add(encabezado);

		List<String> datos;

		for (ReporteBean item : listaReporte) {
			datos = new ArrayList<String>();
			datos = Arrays.asList(item.getInventario().getTipoProducto().getSubcategoria().getNombre(),
					item.getInventario().getTipoProducto().getNombre(), item.getInventario().getNombre(),
					item.getInventario().getMarca(), item.getInventario().getModelo(), item.getInventario().getSerie(),
					item.getInventario().getProveedor().getRazon(), item.getInventario().getUbicacion().getNombre(),
					item.getInventario().getResolucion(), item.getInventario().getPlaca(),
					item.getInventario().getHdd(), item.getInventario().getRam(), item.getInventario().getSo(),
					item.getInventario().getVoltaje(), item.getInventario().getCodigo(),
					item.getInventario().getColor(), item.getInventario().getTarj_prop(),
					item.getInventario().getFoto(), item.getInventario().getTalla(), item.getInventario().getStock()

			);
			listaData.add(datos);
		}
		generarCuerpo(listaData, FILE_NAME_REPORTE_ACTUAL);

		return FILE_NAME_REPORTE_ACTUAL;
	}

	public String obtenerReporteIngreso(List<ReporteBean> listaReporte) {

		List<List<String>> listaData = new ArrayList<List<String>>();

		List<String> titulo = Arrays.asList(Constantes.VACIO, Constantes.VACIO, Constantes.VACIO, Constantes.VACIO,
				"REPORTE DE ENTRADAS");

		listaData.add(titulo);

		List<String> espacio = Arrays.asList("");
		listaData.add(espacio);

		List<String> encabezado = Arrays.asList(INV_ID, INV_NOMBRE, INV_MARCA, INV_MODELO, INV_SERIE, INV_RESOLUCION,
				INV_PLACA, INV_HDD, INV_RAM, INV_SO, INV_VOLTAJE, INV_CODIGO, INV_COLOR, INV_TALLA, INV_TARJ_PROP,
				INV_FOTO, ESTADO, INV_USUARIO_REGIS, INV_FEC_REGIS, INV_USUARIO_MODI, INV_FEC_MODI, IND_CANTIDAD,
				IND_FECHA_COMPRA, IND_USUARIO_REGIS, IND_FEC_REGIS, IND_USUARIO_MODI, IND_FEC_MODI);
		listaData.add(encabezado);

		List<String> datos;
		for (ReporteBean item : listaReporte) {
			datos = new ArrayList<String>();
			datos = Arrays.asList(item.getInventarioDetalle().getInventario().getId(),
					item.getInventarioDetalle().getInventario().getNombre(),
					item.getInventarioDetalle().getInventario().getMarca(),
					item.getInventarioDetalle().getInventario().getModelo(),
					item.getInventarioDetalle().getInventario().getSerie(),
					item.getInventarioDetalle().getInventario().getResolucion(),
					item.getInventarioDetalle().getInventario().getPlaca(),
					item.getInventarioDetalle().getInventario().getHdd(),
					item.getInventarioDetalle().getInventario().getRam(),
					item.getInventarioDetalle().getInventario().getSo(),
					item.getInventarioDetalle().getInventario().getVoltaje(),
					item.getInventarioDetalle().getInventario().getCodigo(),
					item.getInventarioDetalle().getInventario().getColor(),
					item.getInventarioDetalle().getInventario().getTalla(),
					item.getInventarioDetalle().getInventario().getTarj_prop(),
					item.getInventarioDetalle().getInventario().getFoto(),
					item.getInventarioDetalle().getInventario().getEstado(),
					item.getInventarioDetalle().getInventario().getUsuarioRegistrador(),
					item.getInventarioDetalle().getInventario().getFechaRegistro(),
					item.getInventarioDetalle().getInventario().getUsuarioModificador(),
					item.getInventarioDetalle().getInventario().getFechaModificacion(),
					item.getInventarioDetalle().getCantidad(), item.getInventarioDetalle().getFechaCompra(),
					item.getInventarioDetalle().getUsuarioRegistrador(), item.getInventarioDetalle().getFechaRegistro(),
					item.getInventarioDetalle().getUsuarioModificador(),
					item.getInventarioDetalle().getFechaModificacion());
			listaData.add(datos);
		}
		generarCuerpo(listaData, FILE_NAME_REPORTE_INGRESO);
		return FILE_NAME_REPORTE_INGRESO;
	}

	public String obtenerReporteSalida(List<ReporteBean> listaReporte) {

		List<List<String>> listaData = new ArrayList<List<String>>();

		List<String> titulo = Arrays.asList(Constantes.VACIO, Constantes.VACIO, Constantes.VACIO, Constantes.VACIO,
				"REPORTE DE SALIDAS");

		listaData.add(titulo);

		List<String> espacio = Arrays.asList("");
		listaData.add(espacio);

		List<String> encabezado = Arrays.asList(INV_ID, INV_NOMBRE, INV_MARCA, INV_MODELO, INV_SERIE, INV_RESOLUCION,
				INV_PLACA, INV_HDD, INV_RAM, INV_SO, INV_VOLTAJE, INV_CODIGO, INV_COLOR, INV_TALLA, INV_TARJ_PROP,
				INV_FOTO, ESTADO, INV_USUARIO_REGIS, INV_FEC_REGIS, INV_USUARIO_MODI, INV_FEC_MODI, AVP_ID, UNI_ID,
				SEDE_ID, ASI_CANT, ASI_FECHA_ASIGNA);
		listaData.add(encabezado);

		List<String> datos;
		for (ReporteBean item : listaReporte) {
			datos = new ArrayList<String>();
			datos = Arrays.asList(item.getAsignacion().getInventario().getId(),
					item.getAsignacion().getInventario().getNombre(), item.getAsignacion().getInventario().getMarca(),
					item.getAsignacion().getInventario().getModelo(), item.getAsignacion().getInventario().getSerie(),
					item.getAsignacion().getInventario().getResolucion(),
					item.getAsignacion().getInventario().getPlaca(), item.getAsignacion().getInventario().getHdd(),
					item.getAsignacion().getInventario().getRam(), item.getAsignacion().getInventario().getSo(),
					item.getAsignacion().getInventario().getVoltaje(), item.getAsignacion().getInventario().getCodigo(),
					item.getAsignacion().getInventario().getColor(), item.getAsignacion().getInventario().getTalla(),
					item.getAsignacion().getInventario().getTarj_prop(), item.getAsignacion().getInventario().getFoto(),
					item.getAsignacion().getInventario().getEstado(),
					item.getAsignacion().getInventario().getUsuarioRegistrador(),
					item.getAsignacion().getInventario().getFechaRegistro(),
					item.getAsignacion().getInventario().getUsuarioModificador(),
					item.getAsignacion().getInventario().getFechaModificacion(),
					item.getAsignacion().getAgente().getId(), item.getAsignacion().getUnidad().getId(),
					item.getAsignacion().getSede().getId(), item.getAsignacion().getCantidad(),
					item.getAsignacion().getFechaAsignacion());
			listaData.add(datos);
		}
		generarCuerpo(listaData, FILE_NAME_REPORTE_SALIDA);
		return FILE_NAME_REPORTE_SALIDA;
	}

	@SuppressWarnings("deprecation")
	private void generarCuerpo(List<List<String>> listaData, String fileName) {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Reporte 01");

		// Generate fonts
		headerFont = PlantillaMetodos.createFont(HSSFColor.WHITE.index, (short) 12, true, workbook);

		// Generate styles
		headerStyle = PlantillaMetodos.createStyle(headerFont, XSSFCellStyle.ALIGN_CENTER, HSSFColor.BLUE_GREY.index,
				true, HSSFColor.WHITE.index, workbook);

		int rowNum = 0;
		LOG.info("Creating excel");

		for (List<String> datatype : listaData) {
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (String field : datatype) {
				Cell cell = row.createCell(colNum++);
				LOG.info("campo : "+field);
				cell.setCellValue((String) field);
				if (rowNum <= Constantes.NUMERO_FILAS_CABECERA)
					cell.setCellStyle(headerStyle);
			}
		}

		for (int i = 0; i < listaData.get(rowNum - 1).size(); i++) {
			sheet.autoSizeColumn(i);
		}

		try {
			FileOutputStream outputStream = new FileOutputStream(FILE_NAME_RAIZ.concat(fileName));
			
			workbook.write(outputStream);
			workbook.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		LOG.info("Done");

	}
}
