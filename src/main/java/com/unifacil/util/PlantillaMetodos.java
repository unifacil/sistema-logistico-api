package com.unifacil.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.unifacil.util.constantes.Constantes;

public class PlantillaMetodos {

	@Value("${urlFile}")
	String URL_FILE;

	private static final Logger LOG = Logger.getLogger(PlantillaMetodos.class.getName());

	public static int obtenerEntero(String valor) {
		return valor == null || Constantes.VACIO.equals(valor) ? 0 : Integer.parseInt(valor);
	}

	public static Long obtenerLong(String valor) {
		return valor == null || Constantes.VACIO.equals(valor) ? 0 : Long.parseLong(valor);
	}

	public static byte[] obtenerArrayArchivo(String fileName) {
		File file = new File(fileName);
		Path path = Paths.get(file.getAbsolutePath());
		byte[] data = null;
		try {
			data = Files.readAllBytes(path);
			LOG.info("DATA:" + data);
		} catch (IOException e) {
			LOG.warning(e.getMessage());
		}

		return data;
	}
	/*
	 * Metodos para reporte
	 */

	public static XSSFFont createFont(short fontColor, short fontHeight, boolean fontBold, XSSFWorkbook workbook) {

		XSSFFont font = workbook.createFont();
		font.setBold(fontBold);
		font.setFontName("Arial");
		font.setFontHeightInPoints(fontHeight);

		return font;
	}

	public static XSSFCellStyle createStyle(XSSFFont font, short cellAlign, short cellColor, boolean cellBorder,
			short cellBorderColor, XSSFWorkbook workbook) {

		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		return style;
	}
}
