package com.unifacil.util.constantes;

public class Constantes {

	public static final String CERO = "0";
	public static final String UNO = "1";
	public static final String MENSAJE_EXITO_REGISTRO = "Registrado correctamente";
	public static final String MENSAJE_EXITO_ACTUALIZACION = "Actualizado correctamente";
	public static final String MENSAJE_EXITO_ELIMINACION = "Eliminado correctamente";
	public static final String MENSAJE_ERROR_REGISTRO = "Ocurrio un error";
	public static final String VACIO = "";
	public static final String USUARIO = "usuario";
	public static final int UNO_INT = 1;
	public static final int CERO_INT = 1;
	public static final String FORMATO_FECHA = "dd/MM/yyyy";
	public static final String MENSAJE_ERROR_CARGA_ARCHIVO = "Error al cargar archivo";
	public static final String MENSAJE_UBICACION_EXISTE="La ubicacion ya existe";
	public static final String MENSAJE_UNIDAD_EXISTE="La unidad y/o abreviatura ya existe";
	public static final String MENSAJE_SEDE_EXISTE="La sede ya existe";
	public static final String MENSAJE_USUARIO_EXISTE="El usuario ya existe";
	public static final String MENSAJE_PROVEEDOR_EXISTE="El proveedor ya existe";
	public static final String MENSAJE_AGENTE_EXISTE="El agente ya existe";
	public static final int REPORTE_ACTUAL = 1;
	public static final int REPORTE_INGRESO = 2;
	public static final int REPORTE_SALIDA = 3;
	public static final String ACTIVO = "ACTIVO";
	public static final int NUMERO_FILAS_CABECERA = 3;
}
