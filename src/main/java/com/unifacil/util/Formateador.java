package com.unifacil.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Formateador {
	public static Date ParseFecha(String fecha) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fechaDate = null;
		try {
			fechaDate = formato.parse(fecha);
		} catch (ParseException ex) {
			System.out.println(ex);
		}
		return fechaDate;
	}

	public static java.sql.Date convertirStringToDate(String fecha) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date sqlDate = new java.sql.Date(1);
		try {

			if (fecha == null || fecha.length() == 0) {
				return null;
			}
			DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
			java.util.Date utilDate = formatter.parse(fecha);
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");
			String convertido = fechaHora.format(utilDate);
			java.util.Date utilDate2 = formatter2.parse(convertido);
			sqlDate = new java.sql.Date(utilDate2.getTime());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sqlDate;
	}

	public static String convertirDateToString(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String fechaComoCadena = sdf.format(date);

		return fechaComoCadena;
	}

	public static String formatearFecha(String date, String formatoEntrada, String formatoSalida) {
		String resultado = "";

		SimpleDateFormat formato = new SimpleDateFormat(formatoEntrada);
		Date fechaDate = null;
		try {
			fechaDate = formato.parse(date);
			SimpleDateFormat sdf = new SimpleDateFormat(formatoSalida);
			resultado = sdf.format(fechaDate);

		} catch (ParseException ex) {
			System.out.println(ex);
		}

		return resultado;
	}

}
