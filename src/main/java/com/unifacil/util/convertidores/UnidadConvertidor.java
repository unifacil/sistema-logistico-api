package com.unifacil.util.convertidores;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.unifacil.bean.UnidadBean;
import com.unifacil.entidad.Unidad;
import com.unifacil.util.constantes.Constantes;

@Component
public class UnidadConvertidor {

	@Autowired
	UbicacionConvertidor ubicacionConvertidor;
	
	public UnidadBean entityToBean(Unidad unidad) {
		if (unidad == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		UnidadBean unidadBean = new UnidadBean();

		unidadBean.setId(unidad.getId()!=null && !Constantes.VACIO.equals(unidad.getId()) ?  String.valueOf(unidad.getId()) : null);
		unidadBean.setNombreRazon(unidad.getNombreRazon());
		unidadBean.setAbreviatura(unidad.getAbreviatura());
		unidadBean.setEstado(unidad.getEstado() ? Constantes.UNO : Constantes.CERO);
		unidadBean.setUbicacion(ubicacionConvertidor.entityToBean(unidad.getUbicacion()));
		unidadBean.setFechaRegistro(unidad.getFechaRegistro() !=null? formatter.format(unidad.getFechaRegistro()): null);
		return unidadBean;
	}

	public List<UnidadBean> toListBean(List<Unidad> listaUnidades) {
		if (listaUnidades == null || listaUnidades.isEmpty()) {
			return null;
		}

		return listaUnidades.stream().map(x -> entityToBean(x)).collect(Collectors.toList());

	}

	public Unidad beanToEntity(UnidadBean unidadBean) {
		if (unidadBean == null) {
			return null;
		}

		Unidad unidad = new Unidad();

		unidad.setId(unidadBean.getId() != null && !Constantes.VACIO.equals(unidadBean.getId()) ? Long.parseLong(unidadBean.getId()) : null);
		unidad.setNombreRazon(unidadBean.getNombreRazon());
		unidad.setAbreviatura(unidadBean.getAbreviatura());
		unidad.setEstado(Constantes.UNO.equals(unidadBean.getEstado()) ? true : false);
		unidad.setUbicacion(ubicacionConvertidor.beanToEntity(unidadBean.getUbicacion()));
		unidad.setUsuarioRegistro(Integer.parseInt(unidadBean.getUsuarioRegistro() != null? unidadBean.getUsuarioRegistro() : "0"));
		unidad.setUsuarioModificacion(Integer.parseInt(unidadBean.getUsuarioModificacion() != null? unidadBean.getUsuarioModificacion() : "0"));
		return unidad;
	}

	public List<Unidad> toListEntity(List<UnidadBean> listaUnidadesBean) {
		if (listaUnidadesBean == null || listaUnidadesBean.isEmpty()) {
			return null;
		}

		return listaUnidadesBean.stream().map(x -> beanToEntity(x)).collect(Collectors.toList());
	}
}
