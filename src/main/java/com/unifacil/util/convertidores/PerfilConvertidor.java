package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.PerfilBean;
import com.unifacil.entidad.Perfil;

@Component
public class PerfilConvertidor {
	
	public PerfilBean entityToBean(Perfil perfil){
		
		if(perfil == null){
			return null;
		}
		
		PerfilBean perfilBean = new PerfilBean();
		perfilBean.setDescripcion(perfil.getDescripcion());
		perfilBean.setNombre(perfil.getNombre());
		perfilBean.setId(String.valueOf(perfil.getId()));
		perfilBean.setEstado(perfil.isEstado()? "1" : "0");
		
		return perfilBean;
		
	}
	
	public Perfil beanToEntity(PerfilBean perfilBean){
		
		if(perfilBean == null){
			return null;
		}
		
		Perfil perfil = new Perfil();
		
		perfil.setDescripcion(perfilBean.getDescripcion());
		perfil.setNombre(perfilBean.getNombre());
		perfil.setId(Long.parseLong(perfilBean.getId()));
		perfil.setEstado(perfilBean.getEstado().equals("1")? true : false);
		
		return perfil;
	}
	
	public List<PerfilBean> toListBean(List<Perfil> listaPerfiles){
		
		if(listaPerfiles == null || listaPerfiles.isEmpty()){
			return null;
		}
		
		return listaPerfiles.stream()
					 .map(x -> entityToBean(x))
					 .collect(Collectors.toList());
		
	}
	
	public List<Perfil> toListEntity(List<PerfilBean> listaPerfilesBean){
		
		if(listaPerfilesBean == null || listaPerfilesBean.isEmpty()){
			return null;
		}
		
		return listaPerfilesBean.stream()
					 .map(x -> beanToEntity(x))
					 .collect(Collectors.toList());
		
	}
	
}
