package com.unifacil.util.convertidores;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.InventarioDetalleBean;
import com.unifacil.entidad.InventarioDetalle;
import com.unifacil.util.constantes.Constantes;

@Component
public class InventarioDetalleConvertidor {
	
	@Autowired
	InventarioConvertidor inventarioConvertidor;
	
	public InventarioDetalle beanToEntity(InventarioDetalleBean bean){
		if(bean == null){
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		InventarioDetalle entidad = new InventarioDetalle();
		entidad.setCantidad(bean.getCantidad() == null ||Constantes.VACIO.equals(bean.getCantidad())? 0 : Integer.parseInt(bean.getCantidad()) );
		entidad.setEstado(Constantes.UNO.equals(bean.getEstado())? true : false);
		
		entidad.setFechaCompra(bean.getFechaCompra());
		
		entidad.setId(bean.getId() == null || Constantes.VACIO.equals(bean.getId())? 0 : Long.parseLong(bean.getId()));
		entidad.setInventario(inventarioConvertidor.beanToEntity(bean.getInventario()));
		entidad.setUsuarioRegistrador(bean.getUsuarioRegistrador() == null || Constantes.VACIO.equals(bean.getUsuarioRegistrador()) ? 0 : Integer.parseInt(bean.getUsuarioRegistrador()));
		entidad.setUsuarioModificador(bean.getUsuarioModificador() == null || Constantes.VACIO.equals(bean.getUsuarioModificador()) ? 0 : Integer.parseInt(bean.getUsuarioModificador()));
		entidad.setFechaRegistro(bean.getFechaRegistro());
		entidad.setFechaModificacion(bean.getFechaModificacion());
		return entidad;
	}
	
	public InventarioDetalleBean entityToBean(InventarioDetalle entidad){
		if(entidad == null){
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		InventarioDetalleBean bean = new InventarioDetalleBean();
		bean.setCantidad(String.valueOf(entidad.getCantidad()));
		bean.setEstado(entidad.isEstado() ? Constantes.UNO : Constantes.CERO);
		bean.setFechaCompra(entidad.getFechaCompra());
		bean.setId(entidad.getId() == null ? null : String.valueOf(entidad.getId()));
		bean.setInventario(inventarioConvertidor.entityToBean(entidad.getInventario()));
		
		bean.setUsuarioRegistrador(String.valueOf(entidad.getUsuarioRegistrador()));
		bean.setUsuarioModificador(String.valueOf(entidad.getUsuarioModificador()));
		bean.setFechaRegistro(entidad.getFechaRegistro());
		bean.setFechaModificacion(entidad.getFechaModificacion());
		return bean;
	}
	
	public List<InventarioDetalle> toListEntity(List<InventarioDetalleBean> listaBean){
		if(listaBean == null || listaBean.isEmpty()){
			return null;
		}
		return listaBean.stream()
						.map(x -> beanToEntity(x))
						.collect(Collectors.toList());
	}
	
	public List<InventarioDetalleBean> toListBean(List<InventarioDetalle> listaEntidad){
		if(listaEntidad == null || listaEntidad.isEmpty()){
			return null;
		}
		return listaEntidad.stream()
							.map(x -> entityToBean(x))
							.collect(Collectors.toList());
	}
	
}
