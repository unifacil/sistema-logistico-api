package com.unifacil.util.convertidores;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.AgenteBean;
import com.unifacil.entidad.Agente;
import com.unifacil.util.constantes.Constantes;

@Component("agenteConvertidor")
public class AgenteConvertidor {

	@Autowired
	UbicacionConvertidor ubicacionConvertidor;

	@Autowired
	UnidadConvertidor unidadConvertidor;

	@Autowired
	SedeConvertidor sedeConvertidor;

	public AgenteBean entityToBean(Agente agente) {
		if (agente != null) {
			AgenteBean agenteBean = new AgenteBean();

			agenteBean.setId(String.valueOf(agente.getId()));
			agenteBean.setNombres(agente.getNombres());
			agenteBean.setApellidos(agente.getApellidos());
			agenteBean.setDni(agente.getDni());
			agenteBean.setLicencia(agente.getLicencia());
			agenteBean.setCarnet(agente.getCarnet());
			agenteBean.setFechaInit(agente.getFechaInit());
			agenteBean.setFechaCese(agente.getFechaCese());
			agenteBean.setCelular(agente.getCelular());
			agenteBean.setDireccion(agente.getDireccion());
			agenteBean.setEstado(agente.getEstado() ? "1" : "0");
			agenteBean.listaUbicaciones = ubicacionConvertidor.toListBean(agente.listaUbicaciones);
			agenteBean.listaUnidades = unidadConvertidor.toListBean(agente.listaUnidades);
			agenteBean.listaSedes = sedeConvertidor.toListBean(agente.listaSedes);

			return agenteBean;
		} else {
			return null;
		}
	}

	public Agente beanToEntity(AgenteBean agenteBean) {

		if (agenteBean != null) {
			Agente agente = new Agente();

			agente.setId(Long.valueOf(agenteBean.getId()));
			agente.setNombres(agenteBean.getNombres());
			agente.setApellidos(agenteBean.getApellidos());
			agente.setDni(agenteBean.getDni());
			agente.setLicencia(agenteBean.getLicencia());
			agente.setCarnet(agenteBean.getCarnet());
			agente.setFechaInitDate(agenteBean.getFechaInit() != null ? Date.valueOf(agenteBean.getFechaInit()) : null);
			agente.setFechaCeseDate(agenteBean.getFechaCese() != null ? Date.valueOf(agenteBean.getFechaCese()) : null);
			agente.setCelular(agenteBean.getCelular());
			agente.setDireccion(agenteBean.getDireccion());
			agente.setEstado(Constantes.UNO.equals(agenteBean.getEstado()) ? true : false);
			// agente.setUsuarioRegistro(agenteBean.getUsuarioRegistro());
			// agente.setFechaRegistro(Date.valueOf(agenteBean.getFechaRegistro()));
			// agente.setUsuarioModificacion(agenteBean.getUsuarioModificacion());
			// agente.setFechaModificacion(Date.valueOf(agenteBean.getFechaModificacion()));
			agente.listaUbicaciones = ubicacionConvertidor.toListEntity(agenteBean.listaUbicaciones);
			agente.listaUnidades = unidadConvertidor.toListEntity(agenteBean.listaUnidades);
			agente.listaSedes = sedeConvertidor.toListEntity(agenteBean.listaSedes);

			return agente;
		} else {
			return null;
		}
	}

	public List<Agente> toListEntity(List<AgenteBean> listaAgenteBean) {

		if (listaAgenteBean == null || listaAgenteBean.isEmpty()) {
			return null;
		}

		return listaAgenteBean.stream().map(x -> beanToEntity(x)).collect(Collectors.toList());

	}

	public List<AgenteBean> toListBean(List<Agente> listaAgente) {

		if (listaAgente == null || listaAgente.isEmpty()) {
			return null;
		}

		return listaAgente.stream().map(x -> entityToBean(x)).collect(Collectors.toList());
	}
}
