package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.ReporteBean;
import com.unifacil.entidad.Reporte;

@Component
public class ReporteConvertidor {

	@Autowired
	InventarioConvertidor inventarioConvertidor;
	
	@Autowired
	AsignacionConvertidor asignacionConvertidor;
	
	@Autowired
	InventarioDetalleConvertidor inventarioDetalleConvertidor;
	
	public ReporteBean entityToBean(Reporte entidad){
		if(entidad == null){
			return null;
		}
		ReporteBean bean = new ReporteBean();
		bean.setInventario(inventarioConvertidor.entityToBean(entidad.getInventario()));
		bean.setAsignacion(asignacionConvertidor.entityToBean(entidad.getAsignacion()));
		bean.setInventarioDetalle(inventarioDetalleConvertidor.entityToBean(entidad.getInventarioDetalle()));
		
		return bean;
	}
	
	public List<ReporteBean> toListBean(List<Reporte> listaEntidad){
		if(listaEntidad == null ){
			return null;
		}
		return listaEntidad.stream()
							.map(x -> entityToBean(x))
							.collect(Collectors.toList());
	}
	
}
