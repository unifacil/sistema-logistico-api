package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.UsuarioBean;
import com.unifacil.entidad.Usuario;
import com.unifacil.util.constantes.Constantes;

@Component
public class UsuarioConvertidor {

	@Autowired
	UbicacionConvertidor ubicacionConvertidor;

	@Autowired
	UnidadConvertidor unidadConvertidor;

	@Autowired
	SedeConvertidor sedeConvertidor;

	public UsuarioBean entityToBean(Usuario usuario) {
		if (usuario != null) {
			UsuarioBean usuarioBean = new UsuarioBean();

			usuarioBean.setApellidos(usuario.getApellidos());
			usuarioBean.setCorreo(usuario.getCorreo());
			usuarioBean.setEstado(usuario.getEstado());
			usuarioBean.setId(usuario.getId() != null ? String.valueOf(usuario.getId()): null   );
			usuarioBean.setNombres(usuario.getNombres());
			usuarioBean.setPassword(usuario.getPassword());
			usuarioBean.setUsername(usuario.getUsername());
			usuarioBean.setPerfilId(usuario.getPerfilId());
			usuarioBean.setPerfilNombre(usuario.getPerfilNombre());
			usuarioBean.setNombreCompleto(usuario.getNombreCompleto());
			usuarioBean.setListaUbicaciones(ubicacionConvertidor.toListBean(usuario.getListaUbicaciones()));
			usuarioBean.setListaSedes(sedeConvertidor.toListBean(usuario.getListaSedes()));
			usuarioBean.setListaUnidades(unidadConvertidor.toListBean(usuario.getListaUnidades()));

			return usuarioBean;
		} else {
			return null;
		}

	}

	public Usuario beanToEntity(UsuarioBean usuarioBean) {

		if (usuarioBean != null) {
			Usuario usuario = new Usuario();

			usuario.setApellidos(usuarioBean.getApellidos());
			usuario.setCorreo(usuarioBean.getCorreo());
			usuario.setEstado(usuarioBean.getEstado());
			usuario.setId(usuarioBean.getId() != null && !Constantes.VACIO.equals(usuarioBean.getId()) ? Long.parseLong(usuarioBean.getId()) : null);
			usuario.setNombres(usuarioBean.getNombres());
			usuario.setPassword(usuarioBean.getPassword());
			usuario.setUsername(usuarioBean.getUsername());
			usuario.setPerfilId(usuarioBean.getPerfilId() != null ? usuarioBean.getPerfilId() : null);
			usuario.setNombreCompleto(usuarioBean.getNombreCompleto());
			usuario.setListaUbicaciones(ubicacionConvertidor.toListEntity(usuarioBean.getListaUbicaciones()));
			usuario.setListaUnidades(unidadConvertidor.toListEntity(usuarioBean.getListaUnidades()));
			usuario.setListaSedes(sedeConvertidor.toListEntity(usuarioBean.getListaSedes()));
			usuario.setUsuarioCreador(
					usuarioBean.getUsuarioCreador() != null ? Integer.parseInt(usuarioBean.getUsuarioCreador()) : null);
			usuario.setUsuarioModificador(
					usuarioBean.getUsuarioModificar() != null ? Integer.parseInt(usuarioBean.getUsuarioModificar())
							: null);
			return usuario;
		} else {
			return null;
		}

	}

	public List<Usuario> toListEntity(List<UsuarioBean> listaUsuariosBean) {

		if (listaUsuariosBean == null || listaUsuariosBean.isEmpty()) {
			return null;
		}

		return listaUsuariosBean.stream().map(x -> beanToEntity(x)).collect(Collectors.toList());

	}

	public List<UsuarioBean> toListBean(List<Usuario> listaUsuarios) {

		if (listaUsuarios == null || listaUsuarios.isEmpty()) {
			return null;
		}

		return listaUsuarios.stream().map(x -> entityToBean(x)).collect(Collectors.toList());
	}

}
