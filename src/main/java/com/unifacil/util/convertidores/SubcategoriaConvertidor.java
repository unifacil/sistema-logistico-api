package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.SubcategoriaBean;
import com.unifacil.entidad.Subcategoria;
import com.unifacil.util.constantes.Constantes;

@Component
public class SubcategoriaConvertidor {

	@Autowired
	CategoriaConvertidor categoriaConvertidor;
	
	public SubcategoriaBean entityToBean(Subcategoria subcategoria){
		if(subcategoria == null){
			return null;
		}
		
		SubcategoriaBean item = new SubcategoriaBean();
		item.setId(String.valueOf(subcategoria.getId()!=null? subcategoria.getId() : Constantes.CERO_INT));
		item.setNombre(subcategoria.getNombre());
		item.setCategoria(categoriaConvertidor.entityToBean(subcategoria.getCategoria()));
		
		return item;
	}
	
	public Subcategoria beanToEntity(SubcategoriaBean subcategoriaBean){
		if(subcategoriaBean == null){
			return null;
		}
		
		Subcategoria item = new Subcategoria();
		item.setId(new Long(subcategoriaBean.getId()!=null? subcategoriaBean.getId() : Constantes.CERO));
		item.setNombre(subcategoriaBean.getNombre());
		item.setCategoria(categoriaConvertidor.beanToEntity(subcategoriaBean.getCategoria()));
		return item;
	}
	
	public List<SubcategoriaBean> toListBean(List<Subcategoria> listaSubcategorias){
		if(listaSubcategorias == null){
			return null;
		}
		
		return listaSubcategorias.stream()
								.map(x -> entityToBean(x))
								.collect(Collectors.toList());
	}
	
}
