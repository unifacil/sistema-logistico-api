package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.unifacil.bean.CategoriaBean;
import com.unifacil.entidad.Categoria;
import com.unifacil.util.PlantillaMetodos;

@Component("categoriaConvertidor")
public class CategoriaConvertidor {

	public CategoriaBean entityToBean(Categoria categoria) {
		if (categoria == null) {
			return null;
		}
		CategoriaBean categoriaBean = new CategoriaBean();
		categoriaBean.setId(String.valueOf(categoria.getId()));
		categoriaBean.setNombre(categoria.getDescripcion());
		return categoriaBean;
	}

	public List<CategoriaBean> toListBean(List<Categoria> listaCategoria) {

		if (listaCategoria == null || listaCategoria.isEmpty()) {
			return null;
		}

		return listaCategoria.stream().map(x -> entityToBean(x)).collect(Collectors.toList());
	}
	
	public Categoria beanToEntity(CategoriaBean bean){
		if(bean == null){
			return null;
		}
		Categoria entidad = new Categoria();
		entidad.setId(PlantillaMetodos.obtenerLong(bean.getId()));
		entidad.setNombre(bean.getNombre());
		
		return entidad;
	}
	
	public List<Categoria> toListEntity(List<CategoriaBean> listaBean){
		if(listaBean == null || listaBean.isEmpty()){
			return null;
		}
		return listaBean.stream()
						.map(x -> beanToEntity(x))
						.collect(Collectors.toList());
	}
	
}
