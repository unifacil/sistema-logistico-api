package com.unifacil.util.convertidores;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.unifacil.bean.SedeBean;
import com.unifacil.entidad.Sede;
import com.unifacil.util.constantes.Constantes;

@Component
public class SedeConvertidor {
	@Autowired
	UbicacionConvertidor ubicacionConvertidor;
	@Autowired
	UnidadConvertidor unidadConvertidor;
	public SedeBean entityToBean(Sede sede) {
		if (sede == null) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		SedeBean sedeBean = new SedeBean();

		sedeBean.setId(String.valueOf(sede.getId()));
		sedeBean.setNombre(String.valueOf(sede.getNombre()));
		sedeBean.setDireccion(String.valueOf(sede.getDireccion()));
		sedeBean.setEstado(sede.getEstado() ? Constantes.UNO : Constantes.CERO);
		sedeBean.setUbicacion(ubicacionConvertidor.entityToBean(sede.getUbicacion()));
		sedeBean.setUnidad(unidadConvertidor.entityToBean(sede.getUnidad()));
		sedeBean.setFechaRegistro(sede.getFechaRegistro() !=null? formatter.format(sede.getFechaRegistro()): null);
		
		return sedeBean;
	}

	public List<SedeBean> toListBean(List<Sede> listaSedes) {
		if (listaSedes == null || listaSedes.isEmpty()) {
			return null;
		}
		return listaSedes.stream().map(x -> entityToBean(x)).collect(Collectors.toList());

	}

	public Sede beanToEntity(SedeBean sedeBean) {

		if (sedeBean == null) {
			return null;
		}
		Sede sede = new Sede();

		sede.setId(Long.parseLong(sedeBean.getId()));
		sede.setNombre(String.valueOf(sedeBean.getNombre()));
		sede.setDireccion(String.valueOf(sedeBean.getDireccion()));
		sede.setEstado(Constantes.UNO.equals(sedeBean.getEstado()) ? true : false);
		sede.setUbicacion(ubicacionConvertidor.beanToEntity(sedeBean.getUbicacion()));
		sede.setUsuarioRegistro(Integer.parseInt(sedeBean.getUsuarioRegistro() != null? sedeBean.getUsuarioRegistro() : "0"));
		sede.setUsuarioModificacion(Integer.parseInt(sedeBean.getUsuarioModificacion() != null? sedeBean.getUsuarioModificacion() : "0"));

		return sede;

	}

	public List<Sede> toListEntity(List<SedeBean> listaSedesBean) {
		if (listaSedesBean == null || listaSedesBean.isEmpty()) {
			return null;
		}

		return listaSedesBean.stream().map(x -> beanToEntity(x)).collect(Collectors.toList());
	}
}
