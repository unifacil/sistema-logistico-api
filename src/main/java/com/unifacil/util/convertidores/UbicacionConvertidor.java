package com.unifacil.util.convertidores;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import com.unifacil.bean.UbicacionBean;
import com.unifacil.entidad.Ubicacion;
import com.unifacil.util.constantes.Constantes;

@Component
public class UbicacionConvertidor {

	public UbicacionBean entityToBean(Ubicacion ubicacion) {
		
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		if (ubicacion == null) {
			return null;
		}

		UbicacionBean ubicacionBean = new UbicacionBean();

		ubicacionBean.setDireccion(ubicacion.getDireccion());
		ubicacionBean.setEstado(ubicacion.getEstado() ? "1" : "0");
		ubicacionBean.setId(ubicacion.getId()!= null? String.valueOf(ubicacion.getId()): null);
		ubicacionBean.setNombre(ubicacion.getNombres());
		ubicacionBean.setResponsable(ubicacion.getResponsable());
		ubicacionBean.setFechaRegistro(ubicacion.getFechaCreacion()!=null? formatter.format(ubicacion.getFechaCreacion()): null);
		return ubicacionBean;
	}

	public Ubicacion beanToEntity(UbicacionBean ubicacionBean) {

		if (ubicacionBean == null) {
			return null;
		}

		Ubicacion ubicacion = new Ubicacion();

		ubicacion.setDireccion(ubicacionBean.getDireccion());
		ubicacion.setEstado(Constantes.UNO.equals(ubicacionBean.getEstado()) ? true : false);
		ubicacion.setId(ubicacionBean.getId() != null && !Constantes.VACIO.equals(ubicacionBean.getId())? Long.parseLong(ubicacionBean.getId()): null);
		ubicacion.setNombres(ubicacionBean.getNombre());
		ubicacion.setResponsable(ubicacionBean.getResponsable());
		ubicacion.setUsuarioCreador(ubicacionBean.getUsuarioRegistro()!=null? Integer.parseInt(ubicacionBean.getUsuarioRegistro()) : null);
		ubicacion.setUsuarioModificador(ubicacionBean.getUsuarioModificacion()!=null? Integer.parseInt(ubicacionBean.getUsuarioModificacion()) : null);
		return ubicacion;
	}

	public List<UbicacionBean> toListBean(List<Ubicacion> listaUbicaciones) {

		if (listaUbicaciones == null || listaUbicaciones.isEmpty()) {
			return null;
		}

		return listaUbicaciones.stream().map(x -> entityToBean(x)).collect(Collectors.toList());

	}

	public List<Ubicacion> toListEntity(List<UbicacionBean> listaUbicacionesBean) {

		if (listaUbicacionesBean == null || listaUbicacionesBean.isEmpty()) {
			return null;
		}

		return listaUbicacionesBean.stream().map(x -> beanToEntity(x)).collect(Collectors.toList());

	}

}