package com.unifacil.util.convertidores;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.unifacil.bean.ProveedorBean;
import com.unifacil.entidad.Proveedor;
import com.unifacil.util.constantes.Constantes;

@Component
public class ProveedorConvertidor {

	public ProveedorBean entityToBean(Proveedor proveedor) {
		if (proveedor == null) {
			return null;
		}

		ProveedorBean bean = new ProveedorBean();
		bean.setContacto(proveedor.getContacto());
		bean.setCorreo(proveedor.getCorreo());
		bean.setDireccion(proveedor.getDireccion());
		bean.setId(String.valueOf(proveedor.getId()));
		bean.setRazon(proveedor.getRazonSocial());
		bean.setRuc(proveedor.getRuc());
		bean.setTelefono(proveedor.getTelefono());
		bean.setCategoria(proveedor.getCategoria());
		bean.setCategoriaId(String.valueOf(proveedor.getCategoriaId()));
		bean.setEstado(proveedor.getEstado());
		return bean;
	}

	public Proveedor beanToEntity(ProveedorBean proveedorBean) {

		if (proveedorBean == null) {
			return null;
		}

		Proveedor proveedor = new Proveedor();
		proveedor.setContacto(proveedorBean.getContacto());
		proveedor.setCorreo(proveedorBean.getCorreo());
		proveedor.setDireccion(proveedorBean.getDireccion());
		proveedor.setEstado(proveedorBean.getEstado());
		proveedor.setRazonSocial(proveedorBean.getRazon());
		proveedor.setRuc(proveedorBean.getRuc());
		proveedor.setTelefono(proveedorBean.getTelefono());
		proveedor.setCategoriaId(
				proveedorBean.getCategoria() == null || Constantes.VACIO.equals(proveedorBean.getCategoria()) ? 0
						: Integer.parseInt(proveedorBean.getCategoria()));
		proveedor.setCategoria(proveedorBean.getCategoria());
		proveedor.setId(proveedorBean.getId() == null || Constantes.VACIO.equals(proveedorBean.getId()) ? 0
				: Integer.parseInt(proveedorBean.getId()));
		return proveedor;
	}

	public List<ProveedorBean> listaEntityToListBean(List<Proveedor> listaEntidad) {
		if (listaEntidad == null || listaEntidad.isEmpty()) {
			return null;
		}
		List<ProveedorBean> listaProveedorBean = new ArrayList<ProveedorBean>();
		for (Proveedor entitad : listaEntidad) {
			ProveedorBean proveedorBean = new ProveedorBean();
			proveedorBean = entityToBean(entitad);
			listaProveedorBean.add(proveedorBean);
		}
		return listaProveedorBean;
	}

}
