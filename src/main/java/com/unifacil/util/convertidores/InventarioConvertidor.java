package com.unifacil.util.convertidores;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.InventarioBean;
import com.unifacil.entidad.Inventario;
import com.unifacil.util.PlantillaMetodos;
import com.unifacil.util.constantes.Constantes;

@Component
public class InventarioConvertidor {

	@Autowired
	ProveedorConvertidor proveedorConvertidor;
	
	@Autowired
	TipoProductoConvertidor tipoProductoConvertidor;
	
	@Autowired
	UbicacionConvertidor ubicacionConvertidor;
	
	public Inventario beanToEntity(InventarioBean inventarioBean){
		if(inventarioBean == null){
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		Inventario inventario = new Inventario();
		inventario.setCodigo(inventarioBean.getCodigo());
		inventario.setColor(inventarioBean.getColor());
		inventario.setEstado(Constantes.UNO.equals(inventarioBean.getEstado())? true : false);
		inventario.setFoto(inventarioBean.getFoto());
		inventario.setHdd(inventarioBean.getHdd());
		inventario.setId(inventarioBean.getId() == null || Constantes.VACIO.equals(inventarioBean.getId()) ? 0: Long.parseLong(inventarioBean.getId()));
		inventario.setMarca(inventarioBean.getMarca());
		inventario.setModelo(inventarioBean.getModelo());
		inventario.setNombre(inventarioBean.getNombre());
		inventario.setPlaca(inventarioBean.getPlaca());
		inventario.setProveedor(proveedorConvertidor.beanToEntity(inventarioBean.getProveedor()));
		inventario.setRam(inventarioBean.getRam());
		inventario.setResolucion(inventarioBean.getResolucion());
		inventario.setSerie(inventarioBean.getSerie());
		inventario.setSo(inventarioBean.getSo());
		inventario.setStock(inventarioBean.getStock() == null || Constantes.VACIO.equals(inventarioBean.getStock()) ? 0 : Integer.parseInt(inventarioBean.getStock()));
		inventario.setTalla(inventarioBean.getTalla());
		inventario.setTarj_prop(inventarioBean.getTarj_prop());
		inventario.setTipoProducto(tipoProductoConvertidor.beanToEntity(inventarioBean.getTipoProducto()));
		inventario.setUbicacion(ubicacionConvertidor.beanToEntity(inventarioBean.getUbicacion()));
		inventario.setVoltaje(inventarioBean.getVoltaje());
		
		
		inventario.setCantidad(inventarioBean.getCantidad() == null || Constantes.VACIO.equals(inventarioBean.getCantidad())? 0 : Integer.parseInt(inventarioBean.getCantidad()));
		inventario.setFechaCompra(inventarioBean.getFechaCompra());
		inventario.setId_med_detalle(inventarioBean.getId_med_detalle() == null || Constantes.VACIO.equals(inventarioBean.getId_med_detalle()) ? 0 : Integer.parseInt(inventarioBean.getId_med_detalle()));
		
		inventario.setUsuarioRegistrador(PlantillaMetodos.obtenerEntero(inventarioBean.getUsuarioRegistrador()));
		inventario.setUsuarioModificador(PlantillaMetodos.obtenerEntero(inventarioBean.getUsuarioModificador()));
		inventario.setFechaRegistro(inventarioBean.getFechaRegistro());
		inventario.setFechaModificacion(inventarioBean.getFechaModificacion());
		
		return inventario;
	}
	
	public InventarioBean entityToBean(Inventario inventario){
		if(inventario == null){
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat(Constantes.FORMATO_FECHA);
		
		InventarioBean inventarioBean = new InventarioBean();
		
		inventarioBean.setCodigo(inventario.getCodigo());
		inventarioBean.setColor(inventario.getColor());
		inventarioBean.setEstado(inventario.isEstado()? Constantes.UNO : Constantes.CERO);
		inventarioBean.setFoto(inventario.getFoto());
		inventarioBean.setHdd(inventario.getHdd());
		inventarioBean.setId(inventario.getId() == null  ? Constantes.CERO : String.valueOf(inventario.getId()));
		inventarioBean.setMarca(inventario.getMarca());
		inventarioBean.setModelo(inventario.getModelo());
		inventarioBean.setNombre(inventario.getNombre());
		inventarioBean.setPlaca(inventario.getPlaca());
		inventarioBean.setProveedor(proveedorConvertidor.entityToBean(inventario.getProveedor()));
		inventarioBean.setRam(inventario.getRam());
		inventarioBean.setResolucion(inventario.getResolucion());
		inventarioBean.setSerie(inventario.getSerie());
		inventarioBean.setSo(inventario.getSo());
		inventarioBean.setStock(String.valueOf(inventario.getStock()));
		inventarioBean.setTalla(inventario.getTalla());
		inventarioBean.setTarj_prop(inventario.getTarj_prop());
		inventarioBean.setTipoProducto(tipoProductoConvertidor.entityToBean(inventario.getTipoProducto() ));
		inventarioBean.setUbicacion(ubicacionConvertidor.entityToBean(inventario.getUbicacion() ));
		inventarioBean.setVoltaje(inventario.getVoltaje());
		
		inventarioBean.setCantidad(String.valueOf(inventario.getCantidad()));
		inventarioBean.setFechaCompra(inventario.getFechaCompra());
		inventarioBean.setId_med_detalle(String.valueOf(inventario.getId_med_detalle()));
		
		inventarioBean.setUsuarioRegistrador(String.valueOf(inventario.getUsuarioRegistrador()));
		inventarioBean.setUsuarioModificador(String.valueOf(inventario.getUsuarioModificador()));
		inventario.setFechaRegistro(inventarioBean.getFechaRegistro());
		inventario.setFechaModificacion(inventarioBean.getFechaModificacion());
		
		return inventarioBean;
	}
	
	public List<Inventario> toListEntity(List<InventarioBean> listaBean){
		if(listaBean == null || listaBean.isEmpty()){
			return null;
		}
		return listaBean.stream()
						.map(x -> beanToEntity(x))
						.collect(Collectors.toList());
	}
	
	public List<InventarioBean> toListBean(List<Inventario> listaEntidad){
		if(listaEntidad == null || listaEntidad.isEmpty()){
			return null;
		}
		return listaEntidad.stream()
							.map(x -> entityToBean(x))
							.collect(Collectors.toList());
	}
	
}
