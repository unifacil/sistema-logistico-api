package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.TipoProductoBean;
import com.unifacil.entidad.TipoProducto;
import com.unifacil.util.constantes.Constantes;

@Component
public class TipoProductoConvertidor {
	
	@Autowired
	SubcategoriaConvertidor subcategoriaConvertidor;
	
	
	
	public TipoProductoBean entityToBean(TipoProducto tipoProducto){
		if(tipoProducto == null){
			return null;
		}
		TipoProductoBean item = new TipoProductoBean();
		item.setId(String.valueOf(tipoProducto.getId() != null? tipoProducto.getId() : Constantes.CERO_INT));
		item.setNombre(tipoProducto.getNombre());
		item.setEstado(tipoProducto.isEstado()?Constantes.UNO : Constantes.CERO);
		item.setSubcategoria(subcategoriaConvertidor.entityToBean(tipoProducto.getSubcategoria()));
		
		return item;
	}
	
	public TipoProducto beanToEntity(TipoProductoBean tipoProductoBean){
		if(tipoProductoBean == null){
			return null;
		}
		TipoProducto item = new TipoProducto();
		item.setEstado(Constantes.UNO.equals(tipoProductoBean.getEstado())? true:false);
		item.setId(tipoProductoBean.getId() != null? new Long(tipoProductoBean.getId()) : new Long(Constantes.CERO_INT));
		item.setNombre(tipoProductoBean.getNombre());
		item.setSubcategoria(subcategoriaConvertidor.beanToEntity(tipoProductoBean.getSubcategoria()));
		
		return item;
	}
	
	public List<TipoProductoBean> toListBean(List<TipoProducto> listaTipoProducto){
		return listaTipoProducto.stream()
								.map(x -> entityToBean(x))
								.collect(Collectors.toList());
	}
	
}
