package com.unifacil.util.convertidores;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.unifacil.bean.AsignacionBean;
import com.unifacil.entidad.Asignacion;
import com.unifacil.util.Formateador;
import com.unifacil.util.PlantillaMetodos;
import com.unifacil.util.constantes.Constantes;

@Component
public class AsignacionConvertidor {

	@Autowired
	InventarioConvertidor inventarioConvertidor;
	
	@Autowired
	SedeConvertidor sedeConvertidor;
	
	@Autowired
	UnidadConvertidor unidadConvertidor;
	
	@Autowired
	AgenteConvertidor agenteConvertidor;
	
	public Asignacion beanToEntity(AsignacionBean bean){
		if(bean == null){
			return null;
		}
		Asignacion entidad = new Asignacion();
		entidad.setCantidad(PlantillaMetodos.obtenerEntero(bean.getCantidad()));
		entidad.setEstado(Constantes.UNO.equals(bean.getEstado())?true:false);
		entidad.setFechaAsignacion(bean.getFechaAsignacion());
		entidad.setId(PlantillaMetodos.obtenerLong(bean.getId()));
		entidad.setAgente(agenteConvertidor.beanToEntity(bean.getAgente()));
		entidad.setInventario(inventarioConvertidor.beanToEntity(bean.getInventario()));
		entidad.setSede(sedeConvertidor.beanToEntity(bean.getSede()));
		entidad.setUnidad(unidadConvertidor.beanToEntity(bean.getUnidad()));
		
		entidad.setIdUsuarioRegistro(PlantillaMetodos.obtenerEntero(bean.getIdUsuarioRegistro()));
		entidad.setIdUsuarioModificador(PlantillaMetodos.obtenerEntero(bean.getIdUsuarioModificador()));
		
		entidad.setFechaRegistro(bean.getFechaRegistro());
		entidad.setFechaModificacion(bean.getFechaModificacion());
		
		entidad.setListaAgentes(agenteConvertidor.toListEntity(bean.getListaAgentes()));
		entidad.setListaSedes(sedeConvertidor.toListEntity(bean.getListaSedes()));
		entidad.setListaUnidades(unidadConvertidor.toListEntity(bean.getListaUnidades()));
		
		return entidad;
	}
	
	public AsignacionBean entityToBean(Asignacion entidad){
		if(entidad == null){
			return null;
		}
		AsignacionBean bean = new AsignacionBean();
		bean.setCantidad(String.valueOf(entidad.getCantidad()));
		bean.setEstado(entidad.isEstado()?Constantes.UNO : Constantes.CERO);
		bean.setFechaAsignacion(entidad.getFechaAsignacion());
		bean.setId(String.valueOf(entidad.getId()));
		bean.setAgente(agenteConvertidor.entityToBean(entidad.getAgente()));
		bean.setInventario(inventarioConvertidor.entityToBean(entidad.getInventario()));
		bean.setSede(sedeConvertidor.entityToBean(entidad.getSede()));
		bean.setUnidad(unidadConvertidor.entityToBean(entidad.getUnidad()));
		
		bean.setIdUsuarioRegistro(String.valueOf(entidad.getIdUsuarioRegistro()));
		bean.setIdUsuarioModificador(String.valueOf(entidad.getIdUsuarioModificador()));
		
		bean.setFechaRegistro(entidad.getFechaRegistro());
		bean.setFechaModificacion(entidad.getFechaModificacion());
		
		return bean;
	}
	
	public List<Asignacion> toListEntity(List<AsignacionBean> listaBean){
		if(listaBean == null || listaBean.isEmpty()){
			return null;
		}
		return listaBean.stream()
						.map(x -> beanToEntity(x))
						.collect(Collectors.toList());
	}
	
	public List<AsignacionBean> toListBean(List<Asignacion> listaEntidad){
		if(listaEntidad == null || listaEntidad.isEmpty()){
			return null;
		}
		return listaEntidad.stream()
						.map(x -> entityToBean(x))
						.collect(Collectors.toList());
	}
	
}
