package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.CategoriaBean;
import com.unifacil.datos.CategoriaAccess;
import com.unifacil.servicio.CategoriaServicio;
import com.unifacil.util.convertidores.CategoriaConvertidor;

@Service
public class CategoriaServicioImpl implements CategoriaServicio {

	@Autowired
	CategoriaConvertidor categoriaConvertidor;
	@Autowired
	CategoriaAccess categoriaAcceso;

	@Override
	public List<CategoriaBean> listarCategoria() {
		return categoriaConvertidor.toListBean(categoriaAcceso.listarCategoria());
	}

}
