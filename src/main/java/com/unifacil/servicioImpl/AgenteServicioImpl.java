package com.unifacil.servicioImpl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.unifacil.bean.AgenteBean;
import com.unifacil.bean.request.AgenteRequest;
import com.unifacil.controlador.logistica.AgenteControlador;
import com.unifacil.datos.AgenteAccess;
import com.unifacil.entidad.Agente;
import com.unifacil.servicio.AgenteServicio;
import com.unifacil.util.convertidores.AgenteConvertidor;

@Service
public class AgenteServicioImpl implements AgenteServicio{

	private static final Logger LOG = Logger.getLogger(AgenteControlador.class.getName());
	@Autowired
	private AgenteAccess agenteAccess;
	
	@Autowired
	AgenteConvertidor agenteConvertidor;
	
	@Value("${urlFile}")
	String URL_FILE;
	
	@Override
	public AgenteBean findById(Long id) {
		
		//return new AgenteConvertidor().entityToBean(agenteRepositorio.findById(id) );
		return null;
	}
	
	@Override
	public List<AgenteBean> findAll(AgenteRequest agenteRequest) {
		//no hace nada
		return null;
	}
	
	@Override
	public List<AgenteBean> buscarAgente(AgenteRequest agenteRequest){
		return agenteConvertidor.toListBean(agenteAccess.buscarAgente(agenteRequest));
	}

	@Override
	public boolean registrarAgente(AgenteRequest agenteRequest) {
		return agenteAccess.registrarAgente(agenteRequest);
	}

	@Override
	public boolean modificarAgente(AgenteRequest agenteRequest) {
		return agenteAccess.modificarAgente(agenteRequest);
	}

	@Override
	public boolean eliminarAgente(AgenteRequest agenteRequest) {
		return agenteAccess.eliminarAgente(agenteRequest);		
	}

	@Override
	public boolean cargarLicencia(List<MultipartFile> files)  {
		
			System.out.println("url file:"+URL_FILE);
			if(URL_FILE==null) {
				URL_FILE="C:\\prueba";
			}
			try {
		        for (MultipartFile file : files) {
	
		            if (file.isEmpty()) {
		                continue; //next pls
		            }
	
		            byte[] bytes = file.getBytes();
		            Path path = Paths.get(URL_FILE + file.getOriginalFilename());
		            Files.write(path, bytes);
	
		        }
			}catch(Exception e) {
				LOG.warning(e.getMessage());
				return false;
			}
	        return true;
	}
}
