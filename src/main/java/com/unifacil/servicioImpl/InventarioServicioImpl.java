package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.InventarioBean;
import com.unifacil.bean.InventarioDetalleBean;
import com.unifacil.bean.request.InventarioRequest;
import com.unifacil.datos.InventarioAccess;
import com.unifacil.servicio.InventarioServicio;
import com.unifacil.util.convertidores.InventarioConvertidor;
import com.unifacil.util.convertidores.InventarioDetalleConvertidor;

@Service
public class InventarioServicioImpl implements InventarioServicio {

	@Autowired
	InventarioDetalleConvertidor inventarioDetalleConvertidor;
	
	@Autowired
	InventarioConvertidor inventarioConvertidor;
	
	@Autowired
	InventarioAccess inventarioAccess;
	
	@Override
	public boolean registrarInventario(InventarioDetalleBean inventarioDetalleBean) {
		// TODO Auto-generated method stub
		return inventarioAccess.registrarInventario(inventarioDetalleConvertidor.beanToEntity(inventarioDetalleBean));
	}

	@Override
	public List<InventarioBean> buscarInventario(InventarioRequest request) {
		// TODO Auto-generated method stub
		return inventarioConvertidor.toListBean(inventarioAccess.buscarInventario(request));
	}

}
