package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.request.UnidadRequest;
import com.unifacil.datos.UnidadAccess;
import com.unifacil.servicio.UnidadServicio;
import com.unifacil.util.convertidores.UbicacionConvertidor;
import com.unifacil.util.convertidores.UnidadConvertidor;

@Service
public class UnidadServicioImpl implements UnidadServicio {

	@Autowired
	private UnidadAccess unidadAccess;

	@Autowired
	UnidadConvertidor unidadConvertidor;

	@Autowired
	UbicacionConvertidor ubicacionConvertidor;

	@Override
	public List<UnidadBean> findAll(List<UbicacionBean> listaUbicaciones) {
		return unidadConvertidor
				.toListBean(unidadAccess.listarUnidades(ubicacionConvertidor.toListEntity(listaUbicaciones)));
	}

	@Override
	public List<UnidadBean> buscarUnidad(UnidadRequest unidadRequest) {
		// TODO Auto-generated method stub
		return unidadConvertidor.toListBean(unidadAccess.buscarUnidad(unidadRequest));
	}

	@Override
	public int registrarUnidad(UnidadBean unidadBean) {
		// TODO Auto-generated method stub
		return unidadAccess.registrarUnidad(unidadConvertidor.beanToEntity(unidadBean));
	}

	@Override
	public int actualizarUnidad(UnidadBean unidadBean) {
		// TODO Auto-generated method stub
		return unidadAccess.actualizarUnidad(unidadConvertidor.beanToEntity(unidadBean));
	}

	@Override
	public int eliminarUnidad(UnidadBean unidadBean) {
		// TODO Auto-generated method stub
		return unidadAccess.eliminarUnidad(unidadConvertidor.beanToEntity(unidadBean));
	}
}
