package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.AsignacionBean;
import com.unifacil.bean.request.AsignacionRequest;
import com.unifacil.datos.AsignacionAccess;
import com.unifacil.servicio.AsignacionServicio;
import com.unifacil.util.convertidores.AsignacionConvertidor;

@Service
public class AsignacionServicioImpl implements AsignacionServicio{

	@Autowired
	AsignacionAccess asignacionAccess;
	
	@Autowired
	AsignacionConvertidor asignacionConvertidor;
	
	@Override
	public boolean registrarAsignacion(AsignacionBean bean) {
		// TODO Auto-generated method stub
		return asignacionAccess.registrarAsignacion(asignacionConvertidor.beanToEntity(bean));
	}

	@Override
	public List<AsignacionBean> buscarAsignacion(AsignacionRequest request) {
		// TODO Auto-generated method stub
		return asignacionConvertidor.toListBean(asignacionAccess.buscarAsignacion(request));
	}
	
}
