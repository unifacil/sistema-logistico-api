package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.TipoProductoBean;
import com.unifacil.bean.request.TipoProductoRequest;
import com.unifacil.datos.TipoProductoAccess;
import com.unifacil.servicio.TipoProductoServicio;
import com.unifacil.util.convertidores.TipoProductoConvertidor;

@Service
public class TipoProductoServicioImpl implements TipoProductoServicio{

	@Autowired
	TipoProductoConvertidor tipoProductoConvertidor;
	@Autowired
	TipoProductoAccess tipoProductoAccess;
	
	@Override
	public List<TipoProductoBean> listarTipoProducto(TipoProductoRequest request) {
		return tipoProductoConvertidor.toListBean(tipoProductoAccess.listarSubcategoria(request));
	}

}
