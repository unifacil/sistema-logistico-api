package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.UbicacionBean;
import com.unifacil.bean.request.UbicacionRequest;
import com.unifacil.datos.UbicacionAccess;
import com.unifacil.servicio.UbicacionServicio;
import com.unifacil.util.convertidores.UbicacionConvertidor;

@Service
public class UbicacionServicioImpl implements UbicacionServicio {

	@Autowired
	private UbicacionAccess ubicacionAccess;

	@Autowired
	UbicacionConvertidor ubicacionConvertidor;

	@Override
	public List<UbicacionBean> findAll(UbicacionRequest ubicacionRequest) {
		return ubicacionConvertidor.toListBean(ubicacionAccess.listarUbicaciones(ubicacionRequest));
	}

	@Override
	public List<UbicacionBean> buscarUbicacion(UbicacionRequest ubicacionRequest) {
		// TODO Auto-generated method stub
		return ubicacionConvertidor.toListBean(ubicacionAccess.buscarUbicacion(ubicacionRequest));
	}

	@Override
	public int registrarUbicacion(UbicacionBean ubicacionBean) {
		// TODO Auto-generated method stub
		return ubicacionAccess.registrarUbicacion(ubicacionConvertidor.beanToEntity(ubicacionBean));
	}

	@Override
	public int actualizarUbicacion(UbicacionBean ubicacionBean) {
		// TODO Auto-generated method stub
		return ubicacionAccess.actualizarUbicacion(ubicacionConvertidor.beanToEntity(ubicacionBean));
	}

	@Override
	public int eliminarUbicacion(UbicacionBean ubicacionBean) {
		// TODO Auto-generated method stub
		return ubicacionAccess.eliminarUbicacion(ubicacionConvertidor.beanToEntity(ubicacionBean));
	}

	
}
