package com.unifacil.servicioImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.UsuarioBean;
import com.unifacil.bean.request.UsuarioRequest;
import com.unifacil.datos.UsuarioAccess;
import com.unifacil.entidad.Usuario;
import com.unifacil.servicio.UsuarioServicio;
import com.unifacil.util.convertidores.UsuarioConvertidor;

@Service
public class UsuarioServicioImpl implements UsuarioServicio {

	@Autowired
	private UsuarioAccess usuarioAccess;

	@Autowired
	private UsuarioConvertidor usuarioConvertidor;

	@Override
	public UsuarioBean findyUsernameAndPassword(String username, String password) {

		return usuarioConvertidor.entityToBean(usuarioAccess.validarUsuario(username, password));

	}

	@Override
	public List<UsuarioBean> buscarUsuario(UsuarioRequest usuarioRequest) {

		return usuarioConvertidor.toListBean(usuarioAccess.buscarUsuario(usuarioRequest));

	}

	@Override
	public boolean registrarUsuario(UsuarioBean usuarioBean) {
		System.out.println("entrooooooo");
		return usuarioAccess.registrarUsuario(usuarioConvertidor.beanToEntity(usuarioBean));

	}

	@Override
	public boolean actualizarUsuario(UsuarioBean usuarioBean) {
		return usuarioAccess.actualizarUsuario(usuarioConvertidor.beanToEntity(usuarioBean));

	}

	@Override
	public boolean eliminarUsuario(UsuarioRequest usuario) {

		return usuarioAccess.eliminarUsuario(usuario);

	}

	@Override
	public String recuperarPassword(UsuarioRequest usuarioRequest) {
		return String.valueOf(usuarioAccess.recuperarPassword(usuarioRequest));
	}

	@Override
	public boolean cambiarPassword(UsuarioRequest usuarioRequest) {
		return usuarioAccess.cambiarPassword(usuarioRequest);
	}

}
