package com.unifacil.servicioImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.unifacil.bean.SedeBean;
import com.unifacil.bean.UnidadBean;
import com.unifacil.bean.request.SedeRequest;
import com.unifacil.datos.SedeAccess;
import com.unifacil.servicio.SedeServicio;
import com.unifacil.util.convertidores.SedeConvertidor;
import com.unifacil.util.convertidores.UnidadConvertidor;

@Service
public class SedeServicioImpl implements SedeServicio {

	@Autowired
	private SedeAccess sedeAccess;

	@Autowired
	SedeConvertidor sedeConvertidor;

	@Autowired
	UnidadConvertidor unidadConvertidor;

	@Override
	public List<SedeBean> findAll(List<UnidadBean> listaUnidadBean) {
		return sedeConvertidor.toListBean(sedeAccess.listarSedes(unidadConvertidor.toListEntity(listaUnidadBean)));
	}
	
	@Override
	public List<SedeBean> buscarSede(SedeRequest sedeRequest){
		return sedeConvertidor.toListBean(sedeAccess.buscarSede(sedeRequest));
	}

	@Override
	public int registrarSede(SedeRequest sedeRequest) {
		return sedeAccess.registrarSede(sedeRequest);
	}

	@Override
	public int modificarSede(SedeRequest sedeRequest) {
		return sedeAccess.modificarSede(sedeRequest);
	}

	@Override
	public int eliminarSede(SedeRequest sedeRequest) {
		return sedeAccess.eliminarSede(sedeRequest);		
	}
}
