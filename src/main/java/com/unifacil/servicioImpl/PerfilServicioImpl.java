package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.PerfilBean;
import com.unifacil.datos.PerfilAccess;
import com.unifacil.entidad.Perfil;
import com.unifacil.servicio.PerfilServicio;
import com.unifacil.util.convertidores.PerfilConvertidor;

@Service
public class PerfilServicioImpl implements PerfilServicio{

	@Autowired
	private PerfilAccess perfilAccess;
	
	@Autowired
	private PerfilConvertidor perfilConvertidor;
	
	@Override
	public List<PerfilBean> listarPerfiles() {
		// TODO Auto-generated method stub
		return perfilConvertidor.toListBean(perfilAccess.listarPerfiles());
		
	}

}
