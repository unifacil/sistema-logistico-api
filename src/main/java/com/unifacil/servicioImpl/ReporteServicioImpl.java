package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.ReporteBean;
import com.unifacil.bean.request.ReporteRequest;
import com.unifacil.datos.ReporteAccess;
import com.unifacil.servicio.ReporteServicio;
import com.unifacil.util.convertidores.ReporteConvertidor;

@Service
public class ReporteServicioImpl implements ReporteServicio {

	@Autowired
	ReporteConvertidor reporteConvertidor;
	
	@Autowired
	ReporteAccess reporteAccess;
	
	@Override
	public List<ReporteBean> obtenerReporte(ReporteRequest request) {
		// TODO Auto-generated method stub
		return reporteConvertidor.toListBean(reporteAccess.obtenerReporte(request));
	}

}
