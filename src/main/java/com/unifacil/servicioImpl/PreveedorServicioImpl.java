package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.ProveedorBean;
import com.unifacil.bean.request.ProveedorRequest;
import com.unifacil.datos.ProveedorAccess;
import com.unifacil.servicio.ProveedorServicio;
import com.unifacil.util.convertidores.ProveedorConvertidor;

@Service
public class PreveedorServicioImpl implements ProveedorServicio {

	@Autowired
	ProveedorConvertidor proveedorConvertidor;
	@Autowired
	ProveedorAccess proveedorAccess;

	@Override
	public List<ProveedorBean> buscarProveedor(ProveedorRequest proveedorRequest) {
		return proveedorConvertidor.listaEntityToListBean(proveedorAccess.buscarProveedor(proveedorRequest));
	}

	@Override
	public boolean registrarProveedor(ProveedorRequest proveedorRequest) {
		return proveedorAccess.registrarProveedor(proveedorRequest);
	}

	@Override
	public boolean modificarProveedor(ProveedorRequest proveedorRequest) {
		return proveedorAccess.modificarProveedor(proveedorRequest);
	}

	@Override
	public boolean eliminarProveedor(ProveedorRequest proveedorRequest) {
		return proveedorAccess.eliminarProveedor(proveedorRequest);
	}

}
