package com.unifacil.servicioImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unifacil.bean.SubcategoriaBean;
import com.unifacil.bean.SubcategoriaRequest;
import com.unifacil.datos.SubcategoriaAccess;
import com.unifacil.servicio.SubcategoriaServicio;
import com.unifacil.util.convertidores.SubcategoriaConvertidor;

@Service
public class SubcategoriaServicioImpl implements SubcategoriaServicio{

	@Autowired
	SubcategoriaConvertidor subcategoriaConvertidor;
	
	@Autowired
	SubcategoriaAccess subcategoriaAccess;
	
	@Override
	public List<SubcategoriaBean> listarSubcategoria(SubcategoriaRequest request) {
		return subcategoriaConvertidor.toListBean(subcategoriaAccess.listarSubcategoria(request));
	}

}
