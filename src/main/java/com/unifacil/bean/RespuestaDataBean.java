package com.unifacil.bean;

public class RespuestaDataBean extends RespuestaBean{

	public RespuestaDataBean(){
		super();
	}
	
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
}
