package com.unifacil.bean;

import java.util.List;

public class UsuarioBean {

	private String id;

	private Long perfilId;

	private String perfilNombre;

	private String nombres;

	private String apellidos;

	private String nombreCompleto;

	private String correo;

	private String username;

	private String usuarioCreador;

	private String usuarioModificar;

	private String password;

	private String estado;

	private List<UbicacionBean> listaUbicaciones;

	private List<UnidadBean> listaUnidades;

	private List<SedeBean> listaSedes;

	public UsuarioBean() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getEstado() {
		return estado;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String isEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Long getPerfilId() {
		return perfilId;
	}

	public void setPerfilId(Long perfilId) {
		this.perfilId = perfilId;
	}

	public String getPerfilNombre() {
		return perfilNombre;
	}

	public void setPerfilNombre(String perfilNombre) {
		this.perfilNombre = perfilNombre;
	}

	public List<UbicacionBean> getListaUbicaciones() {
		return listaUbicaciones;
	}

	public void setListaUbicaciones(List<UbicacionBean> listaUbicaciones) {
		this.listaUbicaciones = listaUbicaciones;
	}

	public List<UnidadBean> getListaUnidades() {
		return listaUnidades;
	}

	public void setListaUnidades(List<UnidadBean> listaUnidades) {
		this.listaUnidades = listaUnidades;
	}

	public List<SedeBean> getListaSedes() {
		return listaSedes;
	}

	public void setListaSedes(List<SedeBean> listaSedes) {
		this.listaSedes = listaSedes;
	}

	public String getUsuarioCreador() {
		return usuarioCreador;
	}

	public void setUsuarioCreador(String usuarioCreador) {
		this.usuarioCreador = usuarioCreador;
	}

	public String getUsuarioModificar() {
		return usuarioModificar;
	}

	public void setUsuarioModificar(String usuarioModificar) {
		this.usuarioModificar = usuarioModificar;
	}

}
