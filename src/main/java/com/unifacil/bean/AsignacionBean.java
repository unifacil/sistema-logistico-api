package com.unifacil.bean;

import java.util.List;

public class AsignacionBean {

	private String id;
	
	private AgenteBean agente;
	
	private UnidadBean unidad;
	
	private SedeBean sede;
	
	private InventarioBean inventario;
	
	private String cantidad;
	
	private String fechaAsignacion;
	
	private String estado;
	
	private String idUsuarioRegistro;
	
	private String idUsuarioModificador;
	
	private String fechaRegistro;
	
	private String fechaModificacion;
	
	//Para el registrar
	
	private List<AgenteBean> listaAgentes;
	
	private List<SedeBean> listaSedes;
	
	private List<UnidadBean> listaUnidades;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaAsignacion() {
		return fechaAsignacion;
	}

	public void setFechaAsignacion(String fechaAsignacion) {
		this.fechaAsignacion = fechaAsignacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIdUsuarioRegistro() {
		return idUsuarioRegistro;
	}

	public void setIdUsuarioRegistro(String idUsuarioRegistro) {
		this.idUsuarioRegistro = idUsuarioRegistro;
	}

	public String getIdUsuarioModificador() {
		return idUsuarioModificador;
	}

	public void setIdUsuarioModificador(String idUsuarioModificador) {
		this.idUsuarioModificador = idUsuarioModificador;
	}

	public AgenteBean getAgente() {
		return agente;
	}

	public void setAgente(AgenteBean agente) {
		this.agente = agente;
	}

	public UnidadBean getUnidad() {
		return unidad;
	}

	public void setUnidad(UnidadBean unidad) {
		this.unidad = unidad;
	}

	public SedeBean getSede() {
		return sede;
	}

	public void setSede(SedeBean sede) {
		this.sede = sede;
	}

	public InventarioBean getInventario() {
		return inventario;
	}

	public void setInventario(InventarioBean inventario) {
		this.inventario = inventario;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public List<AgenteBean> getListaAgentes() {
		return listaAgentes;
	}

	public void setListaAgentes(List<AgenteBean> listaAgentes) {
		this.listaAgentes = listaAgentes;
	}

	public List<SedeBean> getListaSedes() {
		return listaSedes;
	}

	public void setListaSedes(List<SedeBean> listaSedes) {
		this.listaSedes = listaSedes;
	}

	public List<UnidadBean> getListaUnidades() {
		return listaUnidades;
	}

	public void setListaUnidades(List<UnidadBean> listaUnidades) {
		this.listaUnidades = listaUnidades;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AsignacionBean [id=");
		builder.append(id);
		builder.append(", agente=");
		builder.append(agente);
		builder.append(", unidad=");
		builder.append(unidad);
		builder.append(", sede=");
		builder.append(sede);
		builder.append(", inventario=");
		builder.append(inventario);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", fechaAsignacion=");
		builder.append(fechaAsignacion);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", idUsuarioRegistro=");
		builder.append(idUsuarioRegistro);
		builder.append(", idUsuarioModificador=");
		builder.append(idUsuarioModificador);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append(", listaAgentes=");
		builder.append(listaAgentes);
		builder.append(", listaSedes=");
		builder.append(listaSedes);
		builder.append(", listaUnidades=");
		builder.append(listaUnidades);
		builder.append("]");
		return builder.toString();
	}
	
}
