package com.unifacil.bean;

public class SubcategoriaBean {

	private String id;
	
	private String nombre;
	
	private CategoriaBean categoria;
	
	public SubcategoriaBean(){
		categoria = new CategoriaBean();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public CategoriaBean getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaBean categoria) {
		this.categoria = categoria;
	}
	
	
	
}
