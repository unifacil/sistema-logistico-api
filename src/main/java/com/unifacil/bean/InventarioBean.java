package com.unifacil.bean;

public class InventarioBean {

	private String id;
	
	private TipoProductoBean tipoProducto;
	
	private ProveedorBean proveedor;
	
	private UbicacionBean ubicacion;
	
	private String nombre;
	
	private String talla;
	
	private String marca;
	
	private String modelo;
	
	private String serie;
	
	private String resolucion;
	
	private String placa;
	
	private String hdd;
	
	private String ram;
	
	private String so;
	
	private String voltaje;
	
	private String codigo;
	
	private String color;
	
	private String tarj_prop;
	
	private String foto;
	
	private String stock;
	
	private String estado;
	
	private String usuarioRegistrador;
	
	private String fechaRegistro;
	
	private String usuarioModificador;
	
	private String fechaModificacion;
	
	// para el sp buscar inventario
	
	private String cantidad;
	
	private String fechaCompra;

	private String id_med_detalle;
	
	public InventarioBean(){
		tipoProducto = new TipoProductoBean();
		proveedor = new ProveedorBean();
		ubicacion = new UbicacionBean();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TipoProductoBean getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(TipoProductoBean tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public ProveedorBean getProveedor() {
		return proveedor;
	}

	public void setProveedor(ProveedorBean proveedor) {
		this.proveedor = proveedor;
	}

	public UbicacionBean getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(UbicacionBean ubicacion) {
		this.ubicacion = ubicacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getHdd() {
		return hdd;
	}

	public void setHdd(String hdd) {
		this.hdd = hdd;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getSo() {
		return so;
	}

	public void setSo(String so) {
		this.so = so;
	}

	public String getVoltaje() {
		return voltaje;
	}

	public void setVoltaje(String voltaje) {
		this.voltaje = voltaje;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTarj_prop() {
		return tarj_prop;
	}

	public void setTarj_prop(String tarj_prop) {
		this.tarj_prop = tarj_prop;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistrador() {
		return usuarioRegistrador;
	}

	public void setUsuarioRegistrador(String usuarioRegistrador) {
		this.usuarioRegistrador = usuarioRegistrador;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificador() {
		return usuarioModificador;
	}

	public void setUsuarioModificador(String usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getId_med_detalle() {
		return id_med_detalle;
	}

	public void setId_med_detalle(String id_med_detalle) {
		this.id_med_detalle = id_med_detalle;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InventarioBean [id=");
		builder.append(id);
		builder.append(", tipoProducto=");
		builder.append(tipoProducto);
		builder.append(", proveedor=");
		builder.append(proveedor);
		builder.append(", ubicacion=");
		builder.append(ubicacion);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", talla=");
		builder.append(talla);
		builder.append(", marca=");
		builder.append(marca);
		builder.append(", modelo=");
		builder.append(modelo);
		builder.append(", serie=");
		builder.append(serie);
		builder.append(", resolucion=");
		builder.append(resolucion);
		builder.append(", placa=");
		builder.append(placa);
		builder.append(", hdd=");
		builder.append(hdd);
		builder.append(", ram=");
		builder.append(ram);
		builder.append(", so=");
		builder.append(so);
		builder.append(", voltaje=");
		builder.append(voltaje);
		builder.append(", codigo=");
		builder.append(codigo);
		builder.append(", color=");
		builder.append(color);
		builder.append(", tarj_prop=");
		builder.append(tarj_prop);
		builder.append(", foto=");
		builder.append(foto);
		builder.append(", stock=");
		builder.append(stock);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", usuarioRegistrador=");
		builder.append(usuarioRegistrador);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", usuarioModificador=");
		builder.append(usuarioModificador);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", fechaCompra=");
		builder.append(fechaCompra);
		builder.append(", id_med_detalle=");
		builder.append(id_med_detalle);
		builder.append("]");
		return builder.toString();
	}
	
}
