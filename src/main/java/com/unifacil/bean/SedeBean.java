package com.unifacil.bean;

public class SedeBean {
	
	private String id;	
	private String nombre;
	private String direccion;
	private String estado;
	private String usuarioRegistro;
	private String fechaRegistro;
	private String usuarioModificacion;
	private String fechaModificacion;
	private UbicacionBean ubicacion; 
	private UnidadBean unidad;

	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}

	public String getNombre() 
	{
		return nombre;
	}
	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}	
	
	public String getDireccion() 
	{
		return direccion;
	}
	public void setDireccion(String direccion) 
	{
		this.direccion = direccion;
	}	
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public UbicacionBean getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(UbicacionBean ubicacion) {
		this.ubicacion = ubicacion;
	}
	public UnidadBean getUnidad() {
		return unidad;
	}
	public void setUnidad(UnidadBean unidad) {
		this.unidad = unidad;
	}
	@Override
	public String toString() {
		return "SedeBean [id=" + id + ", nombre=" + nombre + ", direccion=" + direccion + ", estado=" + estado
				+ ", usuarioRegistro=" + usuarioRegistro + ", fechaRegistro=" + fechaRegistro + ", usuarioModificacion="
				+ usuarioModificacion + ", fechaModificacion=" + fechaModificacion + ", ubicacion=" + ubicacion
				+ ", unidad=" + unidad + "]";
	}
	
}
