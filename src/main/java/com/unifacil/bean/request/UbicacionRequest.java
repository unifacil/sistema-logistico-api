package com.unifacil.bean.request;

public class UbicacionRequest {

	private int idUbicacion;
	
	private String direccion;
	
	private String responsable;
	
	private int usuario_perfil;
	
	public UbicacionRequest(){
		
	}  

	public int getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(int idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public int getUsuario_perfil() {
		return usuario_perfil;
	}

	public void setUsuario_perfil(int usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}

	@Override
	public String toString() {
		return "UbicacionRequest [idUbicacion=" + idUbicacion + ", direccion=" + direccion + ", responsable="
				+ responsable + ", usuario_perfil=" + usuario_perfil + "]";
	}
	
	
	
}
