package com.unifacil.bean.request;

import java.sql.Date;

public class AgenteRequest {

	private Integer id;

	private String nombres;

	private String apellidos;

	private String dni;

	private String carnet;

	private Date fecha_Ini;

	private Date fecha_Hasta;

	private String celular;

	private String direccion;

	private String licencia;

	public String Sedes;

	public int idUsuarioCreador;

	public int idUsuarioModificador;

	private Date fecha_ces_Ini;

	private Date fecha_ces_Hasta;

	public String Ubicacion;

	public String Unidad;
	
	private int usuario_perfil;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public Date getFecha_Ini() {
		return fecha_Ini;
	}

	public void setFecha_Ini(Date fecha_Ini) {
		this.fecha_Ini = fecha_Ini;
	}

	public Date getFecha_Hasta() {
		return fecha_Hasta;
	}

	public void setFecha_Hasta(Date fecha_Hasta) {
		this.fecha_Hasta = fecha_Hasta;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLicencia() {
		return licencia;
	}

	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	public String getSedes() {
		return Sedes;
	}

	public void setSedes(String sedes) {
		Sedes = sedes;
	}

	public int getIdUsuarioCreador() {
		return idUsuarioCreador;
	}

	public void setIdUsuarioCreador(int idUsuarioCreador) {
		this.idUsuarioCreador = idUsuarioCreador;
	}

	public int getIdUsuarioModificador() {
		return idUsuarioModificador;
	}

	public void setIdUsuarioModificador(int idUsuarioModificador) {
		this.idUsuarioModificador = idUsuarioModificador;
	}

	public Date getFecha_ces_Ini() {
		return fecha_ces_Ini;
	}

	public void setFecha_ces_Ini(Date fecha_ces_Ini) {
		this.fecha_ces_Ini = fecha_ces_Ini;
	}

	public Date getFecha_ces_Hasta() {
		return fecha_ces_Hasta;
	}

	public void setFecha_ces_Hasta(Date fecha_ces_Hasta) {
		this.fecha_ces_Hasta = fecha_ces_Hasta;
	}

	public String getUbicacion() {
		return Ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		Ubicacion = ubicacion;
	}

	public String getUnidad() {
		return Unidad;
	}

	public void setUnidad(String unidad) {
		Unidad = unidad;
	}

	@Override
	public String toString() {
		return "AgenteRequest [id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", dni=" + dni
				+ ", carnet=" + carnet + ", fecha_Ini=" + fecha_Ini + ", fecha_Hasta=" + fecha_Hasta + ", celular="
				+ celular + ", direccion=" + direccion + ", licencia=" + licencia + ", Sedes=" + Sedes
				+ ", idUsuarioCreador=" + idUsuarioCreador + ", idUsuarioModificador=" + idUsuarioModificador
				+ ", fecha_ces_Ini=" + fecha_ces_Ini + ", fecha_ces_Hasta=" + fecha_ces_Hasta + ", Ubicacion="
				+ Ubicacion + ", Unidad=" + Unidad + "]";
	}

	public int getUsuario_perfil() {
		return usuario_perfil;
	}

	public void setUsuario_perfil(int usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}
	
	

}
