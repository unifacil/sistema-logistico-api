package com.unifacil.bean.request;

public class UnidadRequest {
	
	private int idUbicacion;
	private int idUnidad;
	private String nombre;
	private String abreviatura;
	
	private int usuario_perfil;
	public UnidadRequest(){
		
	}

	public int getIdUbicacion() {
		return idUbicacion;
	}

	public void setIdUbicacion(int idUbicacion) {
		this.idUbicacion = idUbicacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAbreviatura() {
		return abreviatura;
	}

	public void setAbreviatura(String abreviatura) {
		this.abreviatura = abreviatura;
	}

	public int getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(int idUnidad) {
		this.idUnidad = idUnidad;
	}

	public int getUsuario_perfil() {
		return usuario_perfil;
	}

	public void setUsuario_perfil(int usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}
	
	
}
