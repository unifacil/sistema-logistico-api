package com.unifacil.bean.request;

public class TipoProductoRequest {
	
	private int idSubcategoria;
	
	public TipoProductoRequest(){
		
	}

	public int getIdSubcategoria() {
		return idSubcategoria;
	}

	public void setIdSubcategoria(int idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}

	
	
}
