package com.unifacil.bean.request;

import java.sql.Date;

import com.unifacil.entidad.Unidad;

public class SedeRequest {
	private Integer idUbicacion;
	private Integer idUnidad;
	private Integer id;	
	private String nombre;
	private String direccion;
	public Integer idUsuarioCreador;
	public Integer idUsuarioModificador;
	
	private int usuario_perfil;
	public Integer getIdUbicacion() {
		return idUbicacion;
	}
	public void setIdUbicacion(Integer idUbicacion) {
		this.idUbicacion = idUbicacion;
	}
	public Integer getIdUnidad() {
		return idUnidad;
	}
	public void setIdUnidad(Integer idUnidad) {
		this.idUnidad = idUnidad;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getIdUsuarioCreador() {
		return idUsuarioCreador;
	}
	public void setIdUsuarioCreador(Integer idUsuarioCreador) {
		this.idUsuarioCreador = idUsuarioCreador;
	}
	public Integer getIdUsuarioModificador() {
		return idUsuarioModificador;
	}
	public void setIdUsuarioModificador(Integer idUsuarioModificador) {
		this.idUsuarioModificador = idUsuarioModificador;
	}
	public int getUsuario_perfil() {
		return usuario_perfil;
	}
	public void setUsuario_perfil(int usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}		
	
	
}
