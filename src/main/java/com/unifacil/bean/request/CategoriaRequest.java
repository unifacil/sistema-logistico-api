package com.unifacil.bean.request;

public class CategoriaRequest {
	private int idCategoria;
	
	public CategoriaRequest(){
		
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	
	
	
}
