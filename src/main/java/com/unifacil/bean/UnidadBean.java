package com.unifacil.bean;

import java.util.Date;

public class UnidadBean {
	
	private String id;	
	private String nombrerazon;
	private String abreviatura;
	private String estado;
	private String usuarioRegistro;
	private String fechaRegistro;
	private String usuarioModificacion;
	private String fechaModificacion;	
	private UbicacionBean ubicacion; 
	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}

	public String getNombreRazon() 
	{
		return nombrerazon;
	}
	public void setNombreRazon(String nombrerazon) 
	{
		this.nombrerazon = nombrerazon;
	}		
	
	public String getAbreviatura() 
	{
		return abreviatura;
	}
	public void setAbreviatura(String abreviatura) 
	{
		this.abreviatura = abreviatura;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public String getNombrerazon() {
		return nombrerazon;
	}
	public void setNombrerazon(String nombrerazon) {
		this.nombrerazon = nombrerazon;
	}
	public UbicacionBean getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(UbicacionBean ubicacion) {
		this.ubicacion = ubicacion;
	}
	
}
