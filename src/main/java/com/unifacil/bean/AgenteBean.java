package com.unifacil.bean;

import java.util.List;

public class AgenteBean {
	
	private String id;	
	private String nombres;
	private String apellidos;
	private String dni;
	private String licencia;
	private String carnet;
	private String fechaInit;
	private String fechaCese;
	private String celular;
	private String direccion;
	private String estado;
	private String usuarioRegistro;
	private String fechaRegistro;
	private String usuarioModificacion;
	private String fechaModificacion;
	
	public List<UbicacionBean> listaUbicaciones;
	
	public List<UnidadBean> listaUnidades;
	
	public List<SedeBean> listaSedes;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getLicencia() {
		return licencia;
	}
	public void setLicencia(String licencia) {
		this.licencia = licencia;
	}

	public String getCarnet() {
		return carnet;
	}
	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getFechaInit() {
		return fechaInit;
	}
	public void setFechaInit(String fechaInit) {
		this.fechaInit = fechaInit;
	}
	
	public String getFechaCese() {
		return fechaCese;
	}
	public void setFechaCese(String fechaCese) {
		this.fechaCese = fechaCese;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}
	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
}
