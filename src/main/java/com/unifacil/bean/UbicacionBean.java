package com.unifacil.bean;

import java.util.Date;

public class UbicacionBean {

	private String id;
	private String nombre;
	private String direccion;
	private String responsable;
	private String estado;
	private String usuarioRegistro;
	private String fechaRegistro;
	private String usuarioModificacion;
	private String fechaModificacion;
	private String usuario_perfil;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getUsuario_perfil() {
		return usuario_perfil;
	}

	public void setUsuario_perfil(String usuario_perfil) {
		this.usuario_perfil = usuario_perfil;
	}

	@Override
	public String toString() {
		return "UbicacionBean [id=" + id + ", nombre=" + nombre + ", direccion=" + direccion + ", responsable="
				+ responsable + ", estado=" + estado + ", usuarioRegistro=" + usuarioRegistro + ", fechaRegistro="
				+ fechaRegistro + ", usuarioModificacion=" + usuarioModificacion + ", fechaModificacion="
				+ fechaModificacion + ", usuario_perfil=" + usuario_perfil + "]";
	}

}
