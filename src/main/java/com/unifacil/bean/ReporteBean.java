package com.unifacil.bean;

public class ReporteBean {

	private InventarioBean inventario;
	
	private AsignacionBean asignacion;
	
	private InventarioDetalleBean inventarioDetalle;

	public ReporteBean(){
		inventario = new InventarioBean();
		asignacion = new AsignacionBean();
		inventarioDetalle = new InventarioDetalleBean();
	}
	
	public AsignacionBean getAsignacion() {
		return asignacion;
	}

	public void setAsignacion(AsignacionBean asignacion) {
		this.asignacion = asignacion;
	}

	public InventarioDetalleBean getInventarioDetalle() {
		return inventarioDetalle;
	}

	public void setInventarioDetalle(InventarioDetalleBean inventarioDetalle) {
		this.inventarioDetalle = inventarioDetalle;
	}

	public InventarioBean getInventario() {
		return inventario;
	}

	public void setInventario(InventarioBean inventario) {
		this.inventario = inventario;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ReporteBean [inventario=");
		builder.append(inventario);
		builder.append(", asignacion=");
		builder.append(asignacion);
		builder.append(", inventarioDetalle=");
		builder.append(inventarioDetalle);
		builder.append("]");
		return builder.toString();
	}
	
}
