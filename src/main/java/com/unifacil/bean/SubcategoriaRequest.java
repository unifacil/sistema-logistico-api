package com.unifacil.bean;

public class SubcategoriaRequest {

	
	private int idCategoria;
	
	public SubcategoriaRequest(){
		
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}
	
	
}
