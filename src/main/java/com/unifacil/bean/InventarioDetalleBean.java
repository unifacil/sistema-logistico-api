package com.unifacil.bean;

public class InventarioDetalleBean {

	private String id;
	
	private InventarioBean inventario;
	
	private String cantidad;
	
	private String fechaCompra;
	
	private String estado;
	
	private String usuarioRegistrador;
	
	private String fechaRegistro;
	
	private String usuarioModificador;
	
	private String fechaModificacion;
	
	public InventarioDetalleBean(){
		inventario = new InventarioBean();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public InventarioBean getInventario() {
		return inventario;
	}

	public void setInventario(InventarioBean inventario) {
		this.inventario = inventario;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getFechaCompra() {
		return fechaCompra;
	}

	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuarioRegistrador() {
		return usuarioRegistrador;
	}

	public void setUsuarioRegistrador(String usuarioRegistrador) {
		this.usuarioRegistrador = usuarioRegistrador;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getUsuarioModificador() {
		return usuarioModificador;
	}

	public void setUsuarioModificador(String usuarioModificador) {
		this.usuarioModificador = usuarioModificador;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("InventarioDetalleBean [id=");
		builder.append(id);
		builder.append(", inventario=");
		builder.append(inventario);
		builder.append(", cantidad=");
		builder.append(cantidad);
		builder.append(", fechaCompra=");
		builder.append(fechaCompra);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", usuarioRegistrador=");
		builder.append(usuarioRegistrador);
		builder.append(", fechaRegistro=");
		builder.append(fechaRegistro);
		builder.append(", usuarioModificador=");
		builder.append(usuarioModificador);
		builder.append(", fechaModificacion=");
		builder.append(fechaModificacion);
		builder.append("]");
		return builder.toString();
	}
	
}
