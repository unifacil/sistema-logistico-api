package com.unifacil.bean;

public class TipoProductoBean {

	private String id;
	private SubcategoriaBean subcategoria;
	private String nombre;
	private String estado;
	
	public TipoProductoBean(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public SubcategoriaBean getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(SubcategoriaBean subcategoria) {
		this.subcategoria = subcategoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	
	
}
